This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Host

Staging: [Here](https://client-hgb-staging.firebaseapp.com/) <br/> Auto deploy when push to branch `dev`

Production: [Here](https://client-hgb.firebaseapp.com/) <br/> Auto deploy when push to branch `master`

## Available Scripts
`npm run dev`

Run development mode on port 5000

`npm run start`

Development Mode

`npm run build`

Create production build

`npm run test`

Run test

`npm run eject`

NO



