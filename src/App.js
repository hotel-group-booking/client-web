import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { ToastContainer } from 'react-toastify';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import { doTokenVerify } from './reducers/auth';
import { doSubcribe, doFetchNotifications } from './reducers/notification';
import { getSettings } from './reducers/application';
import { getUser } from './reducers/user';
import { AuthRoute } from 'components';
import {
  HomePage,
  RegistrationPage,
  LoginPage,
  MemberPage,
  ProfilePage,
  PartnerPage,
  HotelPage
} from './pages';
import { useUpdateLanguage } from './hooks';

const App = ({
  tokenVerify,
  user,
  settings,
  subscribe,
  fetchNotifications
}) => {
  useUpdateLanguage(settings.lng);
  useEffect(() => {
    if (localStorage.getItem('token')) {
      tokenVerify(localStorage.getItem('token'));
    }
  }, []);

  useEffect(() => {
    if (user) {
      subscribe(user._id);
      fetchNotifications(user._id);
    }
  }, [user]);

  return (
    // <BrowserRouter>
    //   <Switch>
    //     <Route exact path='/' component={HomePage} />
    //     <Route path='/login' component={LoginPage} />
    //     <Route exact path='/registration/:role' component={RegistrationPage} />
    //     <AuthRoute
    //       path='/partner'
    //       component={PartnerPage}
    //       redirectTo='/login'
    //     />
    //     <AuthRoute path='/member' component={UserPage} redirectTo='/login' />
    //     <Route path='/hotel' component={HotelPage} />
    //     <AuthRoute
    //       path='/profile'
    //       component={ProfilePage}
    //       redirectTo='/login'
    //     />
    //     <Route path='/test' component={RHomePage} />
    //     <Route component={() => <div>404</div>} />
    //   </Switch>
    // </BrowserRouter>
    <>
      <ToastContainer
        position='top-right'
        autoClose={3000}
        hideProgressBar={false}
        draggable
        pauseOnHover
      />
      <BrowserRouter>
        <Switch>
          <Route exact path='/' component={HomePage} />
          <Route path='/registration/:role' component={RegistrationPage} />
          <Route path='/login' component={LoginPage} />
          <AuthRoute
            path='/member'
            component={MemberPage}
            redirectTo='/login'
          />
          <AuthRoute
            path='/profile'
            component={ProfilePage}
            redirectTo='/login'
          />
          <AuthRoute
            path='/partner'
            component={PartnerPage}
            redirectTo='/login'
          />
          <Route
            path='/hotel'
            component={HotelPage}
          />
        </Switch>
      </BrowserRouter>
    </>
  );
};

const mapStateToProps = state => ({
  user: getUser(state),
  settings: getSettings(state)
});

const mapDispatchToProps = dispatch => ({
  tokenVerify: token => dispatch(doTokenVerify(token)),
  subscribe: room => dispatch(doSubcribe(room)),
  fetchNotifications: userId => dispatch(doFetchNotifications(userId))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(App);
