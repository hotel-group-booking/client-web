import { callAPI, generateAPI } from 'lib';

const login = callAPI('/api/auth/user-login', 'POST');
const tokenVerify = callAPI('/api/auth/user-token-verify', 'GET');
const userLogout = callAPI('/api/auth/user-logout', 'GET');

const auth = {
  login: () => {
    return generateAPI({
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      }
    })('auth', 'login');
  },
  logout: () => {
    return generateAPI({
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token')}`
      },
      responseType: 'boolean'
    })('auth', 'logout');
  }, 
  current: () => {
    return generateAPI({
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    })('auth', 'current');
  },
  tokenVerify: () => {
    return generateAPI({
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    })('auth', 'token-verify');
  }
};

export {
  login,
  tokenVerify,
  userLogout,
  auth
};