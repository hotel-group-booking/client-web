import { mergeAll } from 'ramda';
const placeAPI = window.google.maps.places;
const autocompleteService = new placeAPI.AutocompleteService();
const placesService = new placeAPI.PlacesService(document.createElement('div'));
const geocoder = new window.google.maps.Geocoder();

const createSessionToken = () => new placeAPI.AutocompleteSessionToken();

const getPlacePredictions = (options, term) => {
  const initTerm = term
    ? term
    : { types: [options.typeAddress], componentRestrictions: { country: 'vn' } };

  const promise = new Promise((resolve, reject) => {
    autocompleteService.getPlacePredictions(
      mergeAll([initTerm, options]),
      (predictions, status) => {
        if (status !== placeAPI.PlacesServiceStatus.OK) {
          reject(status);
        }
        resolve(predictions);
      }
    );
  });

  return promise;
};

const getDetails = ({ placeId, sessionToken }) => {
  const promise = new Promise((resolve, reject) => {
    placesService.getDetails({
      placeId,
      sessionToken
    }, (place, status) => {
      if (status !== placeAPI.PlacesServiceStatus.OK) {
        reject(status);
      }
      resolve(place);
    })
  });

  return promise;
};

const getDragPlace = latlng => {
  const promise = new Promise((resolve, reject) => {
    geocoder.geocode({ 'location': latlng }, (results, status) => {
      if (status === 'OK') {
        if (results[0]) {
          resolve(results[0])
        } else {
          reject(status);
        }
      }
    });
  });
  return promise;
}

export { createSessionToken, getPlacePredictions, getDetails, getDragPlace };
