import {  generateAPI } from 'lib';

const hotels = {
  get: id => {
    return id
      ? generateAPI()('hotels', id)
      : generateAPI()('hotels');
  },
  post: token => {
    return generateAPI({
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`
      }
    })('hotels');
  },
  put: id => {
    return generateAPI({
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    })('hotels', id);
  },
  delete: id => {
    return generateAPI({
      method: 'DELETE',
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`
      },
      responseType: 'boolean'
    })('hotels', id);
  },
  uploadPhotos: id => {
    return generateAPI({
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    })('hotels', id, 'upload-photos');
  },
  deletePhoto: (id, photoId) => {
    return generateAPI({
      method: 'DELETE',
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`
      },
      responseType: 'boolean'
    })('hotels', id, 'photo', photoId);
  }
};

export { hotels };
