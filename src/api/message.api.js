import io from 'socket.io-client';
import { API_URL } from '../config/environments'
import { generateAPI } from 'lib';

const connectMessage = room => {
  const socket = io(`${API_URL}/message`);

  return {
    join: ()=> {
      socket.emit('join', { room });
    },

    typing: (username, id) => {
      socket.emit('typing', { room, username, _id: id });
    },

    endTyping: (username, id) => {
      socket.emit('endTyping', { room, username, _id: id });
    },

    send: data => {
      socket.emit('send', {
        room,
        ...data
      });
    },

    onTyping: handle => {
      socket.on('typing', handle);
    },

    onEndTyping: handle => {
      socket.on('endTyping', handle);
    },

    onMessageHistory: handle => {
      socket.on('messageHistory', handle);
    },

    onStatus: handle => {
      socket.on('status', handle);
    },
    
    onMessage: handle => {
      socket.on('message', handle);
    },

    disconnect: () => {
      socket.disconnect();
    }
  };
};

const messages = {
  get: () => {
    return generateAPI({
      method: 'GET',
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    })('messages');
  },
  post: () => {
    return generateAPI({
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    })('messages');
  },
  emit: (room, event) => {
    return generateAPI({
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      }
    })('messages', 'emit', room, event);
  }
}

export { connectMessage, messages };
