import io from 'socket.io-client';
import { API_URL } from '../config/environments';
import { generateAPI } from '../lib';

const connectNotification = room => {
  const socket = io(`${API_URL}/notification`);

  return {
    join: () => {
      socket.emit('join', { room });
    },

    test: handle => {
      socket.on('hello', handle);
    },

    onOfferUpdated: handle => {
      socket.on('offerUpdated', handle);
    },

    onOfferAccepted: handle => {
      socket.on('offerAccepted', handle);
    },

    onOfferBlocked: handle => {
      socket.on('offerBlocked', handle);
    },

    onOfferNotification: handle => {
      socket.on('offer_notification', handle);
    },
    
    onRequestNotification: handle => {
      socket.on('request_notification', handle);
    },

    disconnect: () => {
      socket.disconnect();
    }
  };
};

const notifications = {
  get: () => {
    return generateAPI({
      method: 'GET',
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    })('notifications');
  },
  put: id => {
    return generateAPI({
      method: 'PUT',
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`,
        'Content-Type': 'application/json'
      }
    })('notifications', id);
  }
};

export {
  connectNotification,
  notifications
}