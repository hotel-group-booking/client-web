import { callAPI, generateAPI } from 'lib';

const createOffer = callAPI('/api/offer/create', 'POST');
const fetchOffer = callAPI('/api/offer/read', 'GET');
const updateOffer = callAPI('/api/offer/update', 'POST');

const offers = {
  get: id => {
    return id
      ? generateAPI({
          headers: {
            Authorization: `Bearer ${localStorage.getItem('token')}`
          }
        })('offers', id)
      : generateAPI({
          headers: {
            Authorization: `Bearer ${localStorage.getItem('token')}`
          }
        })('offers');
  },
  post: () => {
    return generateAPI({
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    })('offers');
  },
  put: id => {
    return generateAPI({
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    })('offers', id);
  },
  delete: id => {
    return generateAPI({
      method: 'DELETE',
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`
      },
      responseType: 'boolean'
    })('offers', id);
  },
  accept: id => {
    return generateAPI({
      method: 'PUT',
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`,
        'Content-Type': 'application/json',
      }
    })('offers', id, 'accept');
  }
};

export { createOffer, fetchOffer, updateOffer, offers };
