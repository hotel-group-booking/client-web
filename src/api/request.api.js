import { callAPI, generateAPI } from 'lib';

const fetchRequests = callAPI('/api/request/fetch-requests', 'GET');
const create = callAPI('/api/request/create', 'POST');
const detail = callAPI('/api/request/detail', 'GET');
const filterRequest = callAPI('/api/request/filter-request', 'POST');

const requests = {
  get: id => {
    return id 
      ? generateAPI()('requests', id)
      : generateAPI()('requests')
  },
  post: token => {
    return generateAPI({
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token || localStorage.getItem('token')}`
      }
    })('requests');
  },
  put: id => {
    return generateAPI({
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    })('requests', id);
  },
  delete: id => {
    return generateAPI({
      method: 'DELETE',
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`
      },
      responseType: 'boolean'
    })('requests', id);
  },
  search: () => {
    return generateAPI({
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    })('requests', 'search');
  },
  count: () => {
    return generateAPI({
      headers: {
        method: 'GET',
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    })('requests', 'count');
  }
};

export { fetchRequests, create, detail, filterRequest, requests };
