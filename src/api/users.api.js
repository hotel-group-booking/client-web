import { callAPI, generateAPI } from '../lib';

const currentUser = callAPI('/api/auth/current-user', 'GET');
const registration = callAPI('/api/user/registration', 'POST');
const userUpdate = callAPI('/api/user/update', 'POST');
const changePassword = callAPI('/api/user/change-password', 'POST');
const fetchDetail = callAPI('/api/user/fetch-detail', 'GET');
const userUpdateInfo = callAPI('/api/user/update-hotel-info', 'POST');
const uploadPhoto = callAPI('/api/user/upload-photo', 'POST');

const users = {
  get: id => {
    return id
      ? generateAPI()('users', id)
      : generateAPI()('users');
  },
  post: () => {
    return generateAPI({
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Authorization:`Bearer ${localStorage.getItem('token')}`
      }
    })('users');
  },
  put: id => {
    return generateAPI({
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    })('users', id);
  },
  delete: id => {
    return generateAPI({
      method: 'DELETE',
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`
      },
      responseType: 'boolean'
    })('users', id);
  },
  uploadAvatar: id => {
    return generateAPI({
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    })('users', id, 'upload-avatar');
  },
  changePassword: id => {
    return generateAPI({
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token')}`
      },
    })('users', id, 'change-password');
  }
};

export {
  currentUser,
  registration,
  userUpdate,
  changePassword,
  fetchDetail,
  userUpdateInfo,
  uploadPhoto,
  users
}
