import React, { useState, useEffect } from 'react';
import { getSettings } from 'reducers/application';
import { connect } from 'react-redux';
import { compose, reduce, isEmpty } from 'ramda';
import { withRouter } from 'react-router-dom';
import { doChangeLanguage } from 'reducers/application';
import { getNotifications } from 'reducers/notification';
import { useTranslation } from 'react-i18next';
import { LANGUAGES } from 'config/constants';
import { doLogout } from 'reducers/auth';
import { getUser } from 'reducers/user';
import { NotificationItem } from './components';
import { Badge } from 'antd';
import {
  Wrapper,
  NavigationItem,
  Icon,
  LeftSection,
  RightSection,
  NavigationWrapper,
  ToggleWrapper,
  ToggleItem,
  Avatar,
  Modal,
  ModalContent,
  LanguageItem,
  Button,
  Brand,
  ItemDropdown,
  Divider,
  NotiSection,
  NotiFooter,
  Drawer,
  EmptySection,
  EmptyNotifiIcon
} from './application-navigator.styles';

const Navigation = ({
  className,
  children,
  brandURL,
  user,
  settings,
  changeLanguage,
  logout,
  history,
  notifications
}) => {
  const [visible, setVisible] = useState(false);
  const [lngModalVisible, setLngModalVisible] = useState(false);
  const [collapsed, setCollapsed] = useState(false);
  const [notiVisible, setNotiVisible] = useState(false);
  const [drawerVisible, setDrawerVisible] = useState(false);
  const [t] = useTranslation('application');

  useEffect(() => {
    setCollapsed(window.innerWidth < 768 ? true : false);
    const handleResize = () => {
      if (window.innerWidth < 768) {
        setCollapsed(true);
      } else {
        setCollapsed(false);
      }
    };
    window.addEventListener('resize', handleResize);
    return () => {
      window.removeEventListener('resize', handleResize);
    };
  }, []);

  const handleOnClick = e => {
    setVisible(!visible);
  };

  const handleLanguageClick = e => {
    setLngModalVisible(!lngModalVisible);
  };

  const handleChangeLanguage = lng => e => {
    changeLanguage(lng);
  };

  const handleLogout = e => {
    logout();
    history.push('/login');
  };

  const handleRedirect = url => e => {
    history.push(url);
  };

  const handleDrawerClose = () => {
    setDrawerVisible(false);
  };

  const handleMenuClick = e => {
    setDrawerVisible(!drawerVisible);
  };

  const handleBrandClick = e => {
    history.push(`/${user.role}`);
  };

  const handleNotificationClick = e => {
    setNotiVisible(!notiVisible);
  };

  const countUnreadNotifications = notifications => {
    return reduce((acc, elem) => {
      return elem.isRead ? acc : acc + 1;
    }, 0)(notifications);
  };

  return (
    <Wrapper className={className}>
      <NavigationWrapper expanded={visible}>
        <LeftSection>
          {collapsed ? (
            <NavigationItem onClick={handleMenuClick}>
              <Icon type='menu-unfold' size='24px' />
            </NavigationItem>
          ) : (
            <NavigationItem>
              <Brand onClick={handleBrandClick}>Hotel Group Booking</Brand>
            </NavigationItem>
          )}
        </LeftSection>
        <RightSection>
          <NavigationItem onClick={handleNotificationClick}>
            <Badge count={countUnreadNotifications(notifications)}>
              <Icon type='notification' size='24px' />
            </Badge>
            <ItemDropdown visible={notiVisible}>
              {isEmpty(notifications.filter(noti => !noti.isRead)) ? (
                <EmptySection>
                  <span>{t('application:navigation.no noti')}</span>
                  <EmptyNotifiIcon size={36} />
                </EmptySection>
              ) : (
                <NotiSection>
                  {notifications
                    .filter(noti => !noti.isRead)
                    .splice(0, 5)
                    .map(noti => (
                      <NotificationItem
                        user={user}
                        notification={noti}
                        key={noti._id}
                      />
                    ))}
                </NotiSection>
              )}
              <Divider />
              <NotiFooter
                onClick={handleRedirect(`/${user.role}/notifications`)}
              >
                <span>{t('application:navigation.see full')}</span>
              </NotiFooter>
            </ItemDropdown>
          </NavigationItem>
          <NavigationItem>
            <Icon type='user' size='24px' />
          </NavigationItem>
          <NavigationItem onClick={handleOnClick}>
            <Icon type='menu' size='24px' />
          </NavigationItem>
        </RightSection>
      </NavigationWrapper>

      <ToggleWrapper visible={visible}>
        <ToggleItem onClick={handleRedirect('/profile')}>
          {user.photo ? (
            <Avatar src={user.photo.link} />
          ) : (
            <Avatar icon='user' />
          )}
          <span>{user && user.username}</span>
        </ToggleItem>
        <ToggleItem onClick={handleRedirect('/profile/settings')}>
          <Icon type='setting' size='24px' theme='filled' />
          <span>{t('application:navigation.account settings')}</span>
        </ToggleItem>
        <ToggleItem onClick={handleLanguageClick}>
          <Icon type='global' size='24px' />
          <span>
            {t('application:navigation.language')}:{' '}
            {t(`application:lng.${settings.lng}`)}
          </span>
        </ToggleItem>
        <ToggleItem onClick={handleLogout}>
          <Icon type='logout' size='24px' />
          <span>{t('application:navigation.logout')}</span>
        </ToggleItem>
      </ToggleWrapper>

      <Modal
        visible={lngModalVisible}
        title={t('application:navigation.select your language')}
        footer={[
          <Button key='1' onClick={() => setLngModalVisible(false)}>
            {t('application:buttons.close')}
          </Button>
        ]}
      >
        <ModalContent>
          {LANGUAGES.map((lng, i) => (
            <LanguageItem
              key={i}
              selected={lng === settings.lng}
              onClick={handleChangeLanguage(lng)}
            >
              {t(`application:lng.${lng}`)}
            </LanguageItem>
          ))}
        </ModalContent>
      </Modal>

      <Drawer
        visible={drawerVisible}
        onClose={handleDrawerClose}
        closable={false}
        placement='left'
      >
        {children}
      </Drawer>
    </Wrapper>
  );
};

const mapStateToProps = state => ({
  settings: getSettings(state),
  user: getUser(state),
  notifications: getNotifications(state)
});

const mapDispatchToProps = dispatch => ({
  changeLanguage: lng => dispatch(doChangeLanguage(lng)),
  logout: () => dispatch(doLogout())
});

export default compose(
  connect(
    mapStateToProps,
    mapDispatchToProps
  ),
  withRouter
)(Navigation);
