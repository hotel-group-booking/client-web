import styled from 'styled-components';
import {
  Icon as AntIcon,
  Avatar as AntAvatar,
  Modal as AntModal,
  Button as AntButton,
  Badge as AntBadge,
  Divider as AntDivider,
  Drawer as AntDrawer
} from 'antd';
import { ZapOff } from 'react-feather';

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  background-color: var(--white);
  box-shadow: var(--highlight);
  position: relative;
`;

const NavigationWrapper = styled.div`
  display: flex;
  flex-direction: row;
  padding: 0rem 4rem;
  border-bottom: 1px solid;
  border-bottom-color: ${props =>
    props.expanded ? 'var(--text-sub)' : 'transparent'};
  @media (max-width: 768px) {
    padding: 0.75rem 0.5rem;
  }
`;

const Icon = styled(AntIcon)`
  & > svg {
    height: ${props => (props.size ? props.size : '24px')};
    width: ${props => (props.size ? props.size : '24px')};
  }
`;

const NavigationItem = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  cursor: pointer;
  transition: 0.25s all;
  color: var(--text-em);
  position: relative;

  @media (min-width: 768px) {
    margin: 0.5rem;
  }

  :hover {
    color: var(--brand-color);
  }
`;

const LeftSection = styled.div`
  display: flex;
  align-items: center;
  justify-content: flex-start;
  flex-grow: 1;
  @media (max-width: 992px) {
    flex-grow: 1;
    justify-content: center;
  }
`;

const RightSection = styled.div`
  display: flex;
  align-items: center;
  margin-left: auto;
  flex-grow: 5;
  justify-content: flex-end;
  @media (max-width: 768px) {
    margin-left: 0;
    flex-grow: 3;
    justify-content: space-around;
  }
`;

const ToggleWrapper = styled.div`
  width: 100%;
  background-color: var(--white);
  display: flex;
  flex-direction: column;
  position: absolute;
  top: 100%;
  z-index: 1;
  transition: visibility 0s, height 0.25s, opacity 0.25s 0.1s;
  visibility: ${props => (props.visible ? 'visible' : 'hidden')};
  height: ${props => (props.visible ? '160px' : '0')};
  opacity: ${props => (props.visible ? '1' : '0')};
`;

const ToggleItem = styled.div`
  display: flex;
  align-items: center;
  padding: 0.5rem 1rem;
  font-family: var(--font-baloo);
  height: 40px;
  color: var(--text-em);
  cursor: pointer;
  transition: background-color 0.1s linear;
  span {
    margin-left: 0.5rem;
  }

  :hover {
    background-color: var(--text-sub);
  }
`;

const Avatar = styled(AntAvatar)`
  height: 26px;
  width: 26px;
  line-height: 26px;
  margin-left: 0 !important;
`;

const Modal = styled(AntModal)`
  top: 20px;
`;

const ModalContent = styled.div`
  display: flex;
  flex-direction: row;
`;

const LanguageItem = styled.div`
  font-family: var(--font-baloo);
  color: ${props => (props.selected ? 'var(--text-em)' : 'var(--text-sub)')};
  padding: 1rem 2rem;
  font-size: 1rem;
  cursor: pointer;
  &::after {
    /* content: ${props => (props.selected ? 'asdfas' : '')}; */
    content: ' \u2714';
    display: ${props => (props.selected ? 'inline' : 'none')};
  }
`;

const Button = styled(AntButton)``;

const Brand = styled.div`
  font-size: 2rem;
  font-weight: 500;
`;

const Badge = styled(AntBadge)``;

const ItemDropdown = styled.div`
  position: absolute;
  border-radius: .5rem;
  height: 100px;
  width: 340px;
  top: 110%;
  right: 0;
  background-color: white;
  box-shadow: var(--highlight);
  z-index: 1;
  transition: visibility 0s, height 0.25s, opacity 0.25s 0.1s;
  visibility: ${props => (props.visible ? 'visible' : 'hidden')};
  height: ${props => (props.visible ? 'fit-content' : '0')};
  opacity: ${props => (props.visible ? '1' : '0')};
  display: flex;
  flex-direction: column;

  @media (max-width: 768px) {
    left: 0;
  }
`;

const Divider = styled(AntDivider)`
  margin: 0;
`;

const NotiSection = styled.div`
  display: flex;
  flex-direction: column;
`;

const NotiFooter = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  font-size: 1rem;
  font-weight: 500;
  padding: .5rem;
  color: var(--text-em);

  :hover {
    background-color: var(--background-primary);
  }
`;

const Drawer = styled(AntDrawer).attrs(props => ({
  placement: 'right',
  width: 260
}))`
  .ant-drawer-body {
    padding: 0;
  }
  .ant-drawer-wrapper-body {
    background-color: var(--background-primary);
    overflow: hidden !important;
  }
`;

const EmptySection = styled.div`
  padding: 2.5rem .5rem 1.75rem;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  span {
    color: var(--text-sub);
    font-size: 1rem;
    font-family: var(--font-baloo);
  }
`;

const EmptyNotifiIcon = styled(ZapOff)`
  margin-top: .5rem;
  color: var(--text-sub);
`;

export {
  Wrapper,
  Icon,
  NavigationItem,
  LeftSection,
  RightSection,
  ToggleWrapper,
  NavigationWrapper,
  ToggleItem,
  Avatar,
  Modal,
  ModalContent,
  LanguageItem,
  Button,
  Brand,
  Badge,
  ItemDropdown,
  Divider,
  NotiSection,
  NotiFooter,
  Drawer,
  EmptySection,
  EmptyNotifiIcon
};
