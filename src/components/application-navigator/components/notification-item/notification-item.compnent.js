import React from 'react';
import { useTranslation } from 'react-i18next';
import moment from 'moment';
import { compose } from 'ramda';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { doUpdate } from 'reducers/notification';
import {
  Wrapper,
  DetailWrapper,
  IntroSection,
  Tag,
  Footer,
  Time
} from './notification-item.styles';

const NotificationItem = ({ notification, history, update, user, ...rest }) => {
  const [t] = useTranslation(['common', 'notifications']);

  const handleRedirect = url => e => {
    update(notification._id, {
      isRead: true
    });
    history.push(url);
  };

  const renderDetail = notification => {
    switch (notification.message) {
      case 'offer updated': {
        return (
          <DetailWrapper
            onClick={handleRedirect(
              `/${user.role}/offer/${notification.offer._id}`
            )}
          >
            <IntroSection>
              {`${notification.offer.partner.displayName} ${t(
                'notifications:updated offer'
              )} ${notification.offer.request.description || 'No Description'}`}
            </IntroSection>
            <Footer>
              <Tag type='primary'>
                {t(`notifications:${notification.message}`)}
              </Tag>
              <Time>
                {moment(notification.created).format('D/M/YYYY - LTS')}
              </Time>
            </Footer>
          </DetailWrapper>
        );
      }

      case 'offer canceled': {
        return (
          <DetailWrapper
            onClick={handleRedirect(
              `/${user.role}/offer/${notification.offer._id}`
            )}
          >
            <IntroSection>
              {`${notification.offer.partner.displayName} ${t(
                'notifications:canceled offer'
              )} ${notification.offer.request.description || 'No Description'}`}
            </IntroSection>
            <Footer>
              <Tag type='secondary'>
                {t(`notifications:${notification.message}`)}
              </Tag>
              <Time>
                {moment(notification.created).format('D/M/YYYY - LTS')}
              </Time>
            </Footer>
          </DetailWrapper>
        );
      }

      case 'new offer': {
        return (
          <DetailWrapper
            onClick={handleRedirect(
              `/${user.role}/offer/${notification.offer._id}`
            )}
          >
            <IntroSection>
              {`${notification.offer.partner.displayName} ${t(
                'notifications:send new offer'
              )} ${notification.offer.request.description || 'No Description'}`}
            </IntroSection>
            <Footer>
              <Tag>{t(`notifications:${notification.message}`)}</Tag>
              <Time>
                {moment(notification.created).format('D/M/YYYY - LTS')}
              </Time>
            </Footer>
          </DetailWrapper>
        );
      }

      case 'offer accepted': {
        return (
          <DetailWrapper
            onClick={handleRedirect(
              `/${user.role}/offer/${notification.offer._id}`
            )}
          >
            <IntroSection>
              {`${notification.offer.partner.displayName} ${t(
                'notifications:send new offer'
              )} ${notification.offer.request.description || 'No Description'}`}
            </IntroSection>
            <Footer>
              <Tag>{t(`notifications:${notification.message}`)}</Tag>
              <Time>
                {moment(notification.created).format('D/M/YYYY - LTS')}
              </Time>
            </Footer>
          </DetailWrapper>
        );
      }

      case 'request deactived': {
        return (
          <DetailWrapper
            onClick={handleRedirect(
              `/${user.role}/offer/${notification.offer._id}`
            )}
          >
            <IntroSection>
              {`${notification.request.member.displayName} ${t(
                'notifications:deactived '
              )} ${notification.request.description || 'No Description'}`}
            </IntroSection>
            <Footer>
              <Tag type='secondary'>
                {t(`notifications:${notification.message}`)}
              </Tag>
              <Time>
                {moment(notification.created).format('D/M/YYYY - LTS')}
              </Time>
            </Footer>
          </DetailWrapper>
        );
      }

      case 'request actived': {
        return (
          <DetailWrapper
            onClick={handleRedirect(
              `/${user.role}/offer/${notification.offer._id}`
            )}
          >
            <IntroSection>
              {`${notification.request.member.displayName} ${t(
                'notifications:deactived '
              )} ${notification.request.description || 'No Description'}`}
            </IntroSection>
            <Footer>
              <Tag type='primary'>
                {t(`notifications:${notification.message}`)}
              </Tag>
              <Time>
                {moment(notification.created).format('D/M/YYYY - LTS')}
              </Time>
            </Footer>
          </DetailWrapper>
        );
      }

      default:
        return null;
    }
  };

  return <Wrapper {...rest}>{renderDetail(notification)}</Wrapper>;
};

const mapDispatchToProps = dispatch => ({
  update: (id, body) => dispatch(doUpdate(id, body))
});

export default compose(
  connect(
    undefined,
    mapDispatchToProps
  ),
  withRouter
)(NotificationItem);
