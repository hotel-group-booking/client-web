import styled from 'styled-components';

const Wrapper = styled.div`
  padding: 1rem;
  transition: background-color .2s;
  border-bottom: .5px solid #eee;

  :hover {
    background-color: var(--background-primary);
  }
`;

const DetailWrapper = styled.div`
  display: flex;
  flex-direction: column;
`;

const IntroSection = styled.div`
  font-size: .9rem;
  font-weight: 500;
  color: var(--text-em);
`;

const Tag = styled.span`
  color: var(--white);
  padding: .1rem .5rem;
  font-size: .8rem;
  width: fit-content;
  margin-top: .5rem;
  border-radius: .2rem;
  background-color: ${props => {
    switch (props.type) {
      case 'primary':
        return '#005b6d';
      case 'secondary':
        return 'rgb(224, 56, 143)';
      default:
        return 'rgb(61, 164, 211)';
    }
  }};
  color: var(--white);
  margin-right: 0.5rem;
`;

const Time = styled.div`
  color: var(--text-sub);
  font-size: .8rem;
  margin-left: auto;
  text-align: center;
  margin-top: .5rem;
`;

const Footer = styled.div`
  display: flex;
  flex-direction: row;
`;

export {
  Wrapper,
  DetailWrapper,
  IntroSection,
  Tag,
  Time,
  Footer
};
