import React, { useEffect } from 'react';
import { Route, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import { message } from 'antd';
import PropTypes from 'prop-types';
import { getIsAuth } from '../../reducers/auth';

const AuthRoute = ({ component: Component, isAuth, redirectTo, ...rest }) => {
  useEffect(() => {
    if (!isAuth && !localStorage.getItem('isAuth')) {
      message.warn('You need to sign in to do that', 2);
    }
  }, []);
  return (
    <Route {...rest}
      render={props => (
        isAuth || localStorage.getItem('isAuth')
        ? <Component {...props}/>
        : <Redirect
            to={{
              pathname: redirectTo,
              state: { from: props.location }
            }}
          />
      )}
    />
  );
};

AuthRoute.propTypes = {
  component: PropTypes.any.isRequired,
  redirectTo: PropTypes.string.isRequired,
  path: PropTypes.string.isRequired,
};

const mapStateToProps = state => ({
  isAuth: getIsAuth(state)
});

export default connect(mapStateToProps)(AuthRoute);