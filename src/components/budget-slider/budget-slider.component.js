import React, { useReducer } from 'react';
import PropTypes from 'prop-types';
import { Select, Slider } from 'antd';
import { numberWithCommas } from 'lib';
import './budget-slider.styles.scss';

const initialState = {
  marks: { 0: '0 VND', 250: '250.000 VND', 500: '500.000 VND' },
  min: 0,
  step: 10,
  max: 500
}

const reducer = (state, action) => {
  switch (action.type) {
    case '0': {
      return initialState;
    }

    case '1': {
      return  {
        min: 500,
        max: 2000,
        step: 10,
        marks: { 500: '500.000 VND', 1000: '1.000.000 VND', 1500: '1.500.000 VND', 2000: '2.000.000 VND'}
      }
    }

    case '2': {
      return  {
        min: 2000,
        max: 5000,
        step: 100,
        marks: { 2000: '2.000.000 VND', 3500: '3.500.000 VND', 5000: '5.000.000 VND'}
      }
    }

    case '3': {
      return  {
        min: 5000,
        max: 20000,
        step: 500,
        marks: { 5000: '5.000.000 VND', 10000: '10.000.000 VND', 15000: '15.000.000 VND', 20000: '20.000.000 VND'}
      }
    }

    default: return state;
  }
};

const BudgetSlider = ({ 
  className = '',
  onSliderChange,
  onSelectorChange,
  defaultValue
}) => {

  const [sliderState, sliderDispatch] = useReducer(reducer, initialState);

  const sliderTipFormatter = value => {
    return `${numberWithCommas(value)}.000`;
  };

  const handleSelectorChange = value => {
    sliderDispatch({type: value});
    if (onSelectorChange) {
      onSelectorChange(value);
    }
  };

  const handleSliderChange = values => {
    onSliderChange(values);
  };

  return (
    <div className={`budget-slider-root ${className}`}>
      <Select 
        className='range-selector' 
        defaultValue='0'
        onChange={handleSelectorChange}
      >
        <Select.Option value='0'>0 - 500.000 VND</Select.Option>
        <Select.Option value='1'>500.000 - 2.000.000 VND</Select.Option>
        <Select.Option value='2'>2.000.000 - 5.000.000 VND</Select.Option>
        <Select.Option value='3'>5.000.000 - 20.000.000 VND</Select.Option>
      </Select>
      <Slider
        range
        {...sliderState}
        tipFormatter={sliderTipFormatter}
        onChange={handleSliderChange}
        defaultValue={defaultValue || [0, 0]}
      />
    </div>
  );
};

BudgetSlider.propTypes = {
  onSliderChange: PropTypes.func.isRequired,
  onSelectorChange: PropTypes.func,
  values: PropTypes.array,
}

export default BudgetSlider;
