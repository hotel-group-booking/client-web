import React from 'react';
import PropTypes from 'prop-types';
import { ThemeProvider } from 'styled-components';
import { Wrapper } from './button.styles';

const Button = ({
  children,
  type = 'default',
  htmlType = 'button',
  text = {},
  size = {},
  ...rest
}) => {
  const theme = {
    fontWeight: text.fontWeight || 500,
    fontSize: text.fontSize || '1rem',
    height: size.height || '48px',
    width: size.width || 'fit-content'
  };

  return (
    <ThemeProvider theme={theme}>
      <Wrapper {...rest} btnType={type} type={htmlType}>
        {children}
      </Wrapper>
    </ThemeProvider>
  );
};

Button.propTypes = {
  type: PropTypes.string,
  htmlType: PropTypes.string,
};

export default Button;
