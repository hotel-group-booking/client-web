import styled, { css } from 'styled-components';

const gradientStyles = css`
 background-image: linear-gradient(
    86.2deg,
    #ef7029 0%,
    #ff4242 49.24%,
    #f93eac 101.28%
  );
  color: var(--white);

  &:hover {
    background-image: linear-gradient(
      to right,
      #ef7029 0%,
      #f93eac 30%,
      #f93eac 100%
    );
  }
`;

const defaultStyles = css`
  color: black;
`;

const Wrapper = styled.button`
  display: inline-block;
  border: none;
  border-radius: 0.25rem;
  height: ${props => props.theme.height};
  line-height: ${props => props.theme.height};
  width: ${props => props.theme.width};
  text-align: center;
  font-size: ${props => props.theme.fontSize};
  font-weight: ${props => props.theme.fontWeight};
  color: ${props => props.theme.color};
  padding: 0 1.5rem;
  cursor: pointer;

  ${props => {
    switch (props.btnType) {
      case 'gradient': {
        return gradientStyles;
      }

      default:
        return defaultStyles;
    }
  }}
`;

export { Wrapper };
