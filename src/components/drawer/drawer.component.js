import React from 'react';
import { Wrapper } from './drawer.styles';

const Drawer = ({ children, ...rest }) => {
  return (
    <Wrapper 
      closable={false}
      placement='left'
      {...rest}
    >
      {children}
    </Wrapper>
  );
};

export default Drawer;