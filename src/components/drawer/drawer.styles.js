import styled from 'styled-components';
import { min } from 'ramda';
import { Drawer as AntDrawer } from 'antd';

const Wrapper = styled(AntDrawer).attrs(props => ({
  placement: 'right',
  width: `${min(window.innerWidth, 520)}px`
}))`
  .ant-drawer-body {
    padding: 0;
    height: 100%;
  };
  .ant-drawer-wrapper-body {
    background-color: var(--background-primary);
    overflow: hidden !important;
  };
`;

export {
  Wrapper,
}