import React, { useState } from 'react';
import PlacesAutocomplete, {
  getLatLng,
  geocodeByPlaceId
} from 'react-places-autocomplete';
import { ThemeProvider } from 'styled-components';
import {
  Wrapper,
  Input,
  Dropdown,
  Item,
  ItemText
} from './geocode-autocomplete.styles';

const GeocodeAutocomplete = ({
  className,
  onSelect,
  height,
  fontSize,
  placeholder,
  defaultValue = ''
}) => {
  const [value, setValue] = useState(defaultValue);

  const handleChange = address => {
    setValue(address);
  };

  const handleSelect = (address, placeId) => {
    setValue(address);
    geocodeByPlaceId(placeId)
      .then(results => getLatLng(results[0]))
      .then(({ lat, lng }) => {
        if (onSelect) {
          onSelect({ name: address, placeId, lat, lng });
        }
      });
  };

  return (
    <PlacesAutocomplete
      value={value}
      onChange={handleChange}
      onSelect={handleSelect}
      searchOptions={{
        types: ['geocode'],
        componentRestrictions: {
          country: 'vn'
        }
      }}
    >
      {({ getInputProps, suggestions, getSuggestionItemProps, loading }) => (
        <ThemeProvider
          theme={{
            height,
            fontSize
          }}
        >
          <Wrapper className={className}>
            <Input {...getInputProps()} placeholder={placeholder} />
            <Dropdown>
              {suggestions.map(suggestion => (
                <Item
                  {...getSuggestionItemProps(suggestion)}
                  active={suggestion.active}
                >
                  <ItemText>{suggestion.description}</ItemText>
                </Item>
              ))}
            </Dropdown>
          </Wrapper>
        </ThemeProvider>
      )}
    </PlacesAutocomplete>
  );
};

export default GeocodeAutocomplete;
