import styled from 'styled-components';
import { Input as AntInput } from 'antd';

const Wrapper = styled.div`
  position: relative;
`;

const Input = styled(AntInput)`
  font-size: ${props => props.theme.fontSize};
  height: ${props => `${props.theme.height} !important` };
`;

const Dropdown = styled.div`
  position: absolute;
  z-index: 10;
  width: 100%;
  left: 0;

`;

const Item = styled.div`
  background-color: ${props => props.active ? '#fafafa' : '#ffffff'};
  cursor: pointer;
  padding: .5rem;
`;

const ItemText = styled.span`
  font-size: .9rem;
  font-weight: 500;
`;

const Loading = styled.span`
  font-size: .9rem;
  padding: .5rem;
`;

export {
  Wrapper,
  Input,
  Dropdown,
  Item,
  ItemText,
  Loading
};
