import React from 'react';
import PropTypes from 'prop-types';
import { HashLink } from 'react-router-hash-link';
import { useAnchorPosition } from '../../hooks';

const Anchor = ({ children, to, offset = 0, ...rest }) => {
  const position = useAnchorPosition(to);

  const handleScroll = el => {
    if (offset) {
      const offsetPosition = position - offset;
      window.scrollTo({
        top: offsetPosition,
        behavior: 'smooth'
      });
    } else {
      el.scrollIntoView({ behavior: 'smooth', block: 'start' });
    }
  };

  return (
    <HashLink {...rest} to={to} scroll={handleScroll}>
      {children}
    </HashLink>
  );
};

Anchor.propTypes = {
  to: PropTypes.string.isRequired,
  offset: PropTypes.number
};

export default Anchor;
