import React from 'react';
import { PushSpinner } from 'react-spinners-kit';
import { Wrapper } from './loading-list.styles';

const LoadingList = ({ loading = true, ...rest }) => {
  return (
    <Wrapper {...rest} loading={loading}>
      <PushSpinner color='#c2c6da'/>
    </Wrapper>
  );
};

export default LoadingList;
