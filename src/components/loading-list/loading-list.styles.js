import styled from 'styled-components';

const Wrapper = styled.div`
  display: ${props => props.loading ? 'flex' : 'none'};
  justify-content: center;
  align-items: center;
`;

export {
  Wrapper
};