import React, { useState } from 'react';
import PlacesAutocomplete from 'react-places-autocomplete';
import { ThemeProvider } from 'styled-components';
import {
  Wrapper,
  Input,
  Dropdown,
  Item,
  ItemText
} from './location-autocomplete.styles';

const CityAutocomplete = ({
  className,
  onSelect,
  height,
  fontSize,
  placeholder = '',
  defaultValue = ''
}) => {
  const [value, setValue] = useState(defaultValue);

  const handleChange = address => {
    setValue(address);
  };

  const handleSelect = (address, placeId) => {
    setValue(address);
      
    if (onSelect) {
      onSelect({ name: address, placeId });
    }
  };

  return (
    <PlacesAutocomplete
      value={value}
      onChange={handleChange}
      onSelect={handleSelect}
      searchOptions={{
        types: ['(cities)'],
        componentRestrictions: {
          country: 'vn'
        }
      }}
    >
      {({ getInputProps, suggestions, getSuggestionItemProps, loading }) => (
        <ThemeProvider
          theme={{
            height,
            fontSize
          }}
        >
          <Wrapper className={className}>
            <Input {...getInputProps()} placeholder={placeholder} />
            <Dropdown>
              {suggestions.map(suggestion => (
                <Item
                  {...getSuggestionItemProps(suggestion)}
                  active={suggestion.active}
                >
                  <ItemText>{suggestion.description}</ItemText>
                </Item>
              ))}
            </Dropdown>
          </Wrapper>
        </ThemeProvider>
      )}
    </PlacesAutocomplete>
  );
};

export default CityAutocomplete;
