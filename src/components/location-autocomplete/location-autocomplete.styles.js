import styled from 'styled-components';
import { Input as AntInput } from 'antd';

const Wrapper = styled.div`
  position: relative;
`;

const Input = styled(AntInput)`
  font-size: ${props => props.theme.fontSize || '.9rem'};
  height: ${props => `${props.theme.height} !important` };
`;

const Dropdown = styled.div`
  position: absolute;
  left: 0;
  top: 32px;
  right: 0;
  background-color: var(--white);
  padding: .25rem 0;
  z-index: 1;
`;

const Item = styled.div`
  background-color: ${props => props.active ? '#fafafa' : '#ffffff'};
  cursor: pointer;
  padding: .5rem;
`;

const ItemText = styled.span`
  font-size: .9rem;
  font-weight: 500;
`;

const Loading = styled.span`
  font-size: .9rem;
  padding: .5rem;
`;

export {
  Wrapper,
  Input,
  Dropdown,
  Item,
  ItemText,
  Loading
};
