import React from 'react';
import PropTypes from 'prop-types';
import { ThemeProvider } from 'styled-components';
import { Icon } from './logo.styles';

const Logo = ({className, size, color, ...rest}) => {
  const theme = {
    size: size || 36,
    color: color || '#000'
  };

  return (
    <ThemeProvider theme={theme}>
      <Icon className={className} {...rest}/>
    </ThemeProvider>
  );
};

Logo.propTypes = {
  className: PropTypes.string,
  size: PropTypes.number,
  color: PropTypes.string
};

export default Logo;

