import styled from 'styled-components';
import { ReactComponent as LogoIcon } from './icon.svg';


const Icon = styled(LogoIcon)`
  width: ${props => `${props.theme.size}px`};
  height: ${props => `${props.theme.size}px`};
  fill: ${props => props.theme.color};
`;


export {
  Icon
}