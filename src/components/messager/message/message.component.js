import React from 'react';
import PropTypes from 'prop-types';
import { Avatar } from 'antd';
import { ThemeProvider } from 'styled-components';
import { Wrapper, Content, AvatarSection } from './message.styles';

const Message = ({ className, message, own, user, ...rest }) => {
  return (
    <ThemeProvider theme={{ own }}>
      <Wrapper className={className} {...rest}>
        <AvatarSection>
          {user.photo ? (
            <Avatar src={user.photo.link} />
          ) : (
            <Avatar icon='user' />
          )}
        </AvatarSection>
        <Content>{message.data}</Content>
      </Wrapper>
    </ThemeProvider>
  );
};

Message.propTypes = {
  className: PropTypes.string,
  message: PropTypes.object.isRequired
};

export default Message;
