import styled from 'styled-components';

const Wrapper = styled.div`
  display: flex;
  width: 70%;
  margin-bottom: 1.75rem;
  min-width: 250px;
  margin-left: ${props => (props.theme.own ? 'auto' : '0')};
`;
const Content = styled.div`
  padding: 0.7rem;
  width: 100%;
  background-color: ${props => (props.theme.own ? '#746BAE' : '#f1f1f7')};
  border-radius: 0.4rem;
  color: ${props => (props.theme.own ? 'var(--white)' : 'var(--text-em)')};
`;

const AvatarSection = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: flex-end;
  order: ${props => (props.theme.own ? '1' : '0')};
  margin: ${props => (props.theme.own ? '0 0 0 .5rem' : '0 .5rem 0 0')};
`;

export { Wrapper, Content, AvatarSection };
