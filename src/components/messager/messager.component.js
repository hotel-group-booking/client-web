import React, { useEffect, useState } from 'react';
import { Star, Bookmark, Paperclip, Smile } from 'react-feather';
import { join, isEmpty, compose, map } from 'ramda';
import { connect } from 'react-redux';
import Message from './message/message.component';
import {
  getMessages,
  getTyping,
  doSend,
  doSubcribe,
  doEmit
} from 'reducers/message';
import TypingDots from './typing-dots/typing-dots.component';
import { useTranslation } from 'react-i18next';
// import { connectMessage } from 'api';
import {
  Wrapper,
  StatusBar,
  MessageSection,
  InputGroup,
  Status,
  ButtonGroup,
  Input,
  InputButtonGroup,
  IconButton,
  SendButton
} from './messager.styles';

const Messager = ({
  className,
  room,
  messages,
  typing,
  user,
  subscribe,
  sendMessage,
  emit
}) => {
  // const [messageService, setMessageService] = useState();
  const [input, setInput] = useState('');
  const [t] = useTranslation(['messager']);
  const ref = React.createRef();
  useEffect(() => {
    const offset = document.getElementById('end').offsetTop;
    ref.current.scrollTop = offset;
  }, [messages]);

  useEffect(() => {
    subscribe(room);
  }, [room]);


  const handleInputChange = e => {
    if (!input) {
      // messageService.typing(user.displayName, user._id);
      emit(room, 'typing', {
        username: user.displayName,
        id: user._id
      })
    }
    if (!e.target.value) {
      emit(room, 'endTyping', {
        username: user.displayName,
        id: user._id
      });
    }
    setInput(e.target.value);
  };

  const handleSend = e => {
    emit(room, 'endTyping', {
      username: user.displayName,
      id: user._id
    });
    sendMessage({
      data: input,
      room
    });
    setInput('');

  };

  const typingUsers = typing => {
    return compose(
      join(', '),
      map(user => user.username)
    )(typing);
  };

  return (
    <Wrapper className={className}>
      <StatusBar>
        <Status>
          {!isEmpty(typing) && (
            <>
              <span>{typingUsers(typing)}</span>
              <TypingDots/>
            </>
          )}
        </Status>

        <ButtonGroup>
          <IconButton>
            <Star />
          </IconButton>
          <IconButton>
            <Bookmark />
          </IconButton>
        </ButtonGroup>
      </StatusBar>

      <MessageSection ref={ref}>
        {messages.map(message => (
          <Message
            message={message}
            key={message._id}
            own={user && message.owner === user._id}
            user={user}
          />
        ))}
        <span id='end' />
      </MessageSection>

      <InputGroup>
        <InputButtonGroup>
          <IconButton>
            <Paperclip size={20} />
          </IconButton>
          <IconButton>
            <Smile size={20} />
          </IconButton>
        </InputButtonGroup>
        <Input
          placeholder={t('message:type your message')}
          onChange={handleInputChange}
          value={input}
        />
        <SendButton onClick={handleSend}>Send</SendButton>
      </InputGroup>
    </Wrapper>
  );
};

const mapStateToProps = state => ({
  messages: getMessages(state),
  typing: getTyping(state)
});

const mapDispatchToProps = dispatch => ({
  // addMessage: msg => dispatch(doAddMessage(msg)),
  // loadHistory: messages => dispatch(doLoadHistory(messages)),
  // addTyping: user => dispatch(doAddTyping(user)),
  // removeTyping: user => dispatch(doRemoveTyping(user))
  subscribe: room => dispatch(doSubcribe(room)),
  sendMessage: message => dispatch(doSend(message)),
  emit: (room, event, data) => dispatch(doEmit(room, event, data))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Messager);
