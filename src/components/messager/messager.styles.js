import styled from 'styled-components';
import { Button } from 'components';

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  border: 1px solid var(--brand-color);
  border-radius: 0.25rem;
`;

const StatusBar = styled.div`
  display: flex;
  justify-content: space-between;
  border-bottom: 1px solid #f1f1fa;
  padding: 0.5rem 1rem;
`;

const Status = styled.span`
  color: var(--text-sub);
  font-size: 1rem;
  display: flex;
`;

const ButtonGroup = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-around;
  width: 70px;
`;

const IconButton = styled.span`
  display: flex;
  align-items: center;
  cursor: pointer;
  svg {
    color: var(--text-sub);
    transition: color 0.2s;
  }
  &:hover svg {
    color: var(--text-title);
  }
`;

const MessageSection = styled.div`
  display: flex;
  flex-direction: column;
  padding: 0.5rem 1rem;
  border-bottom: 1px solid #f1f1fa;
  height: 400px;
  overflow-y: auto;
  &::-webkit-scrollbar {
    width: 4px;
  };
  &::-webkit-scrollbar-thumb {
    background: var(--text-em);
    border-radius: 2px;
  };
`;

const InputGroup = styled.div`
  display: flex;
  flex-direction: row;
  padding: 0.5rem 1rem;
`;

const Input = styled.input`
  height: 2.5rem;
  font-size: 1rem;
  border: none;
  outline: none;
  flex-grow: 1;
  color: var(--text-em);
  &::placeholder {
    color: var(--text-sub);
  }
`;

const InputButtonGroup = styled.div`
  display: flex;
  justify-content: space-around;
  width: 70px;
  margin-right: 1rem;
`;

const SendButton = styled(Button).attrs(props => ({
  type: 'gradient'
}))`
  height: 40px;
  line-height: 40px;
  border-radius: 20px;
  margin-left: 1rem;
`;

export {
  Wrapper,
  StatusBar,
  Status,
  MessageSection,
  InputGroup,
  Input,
  ButtonGroup,
  InputButtonGroup,
  IconButton,
  SendButton,
};
