import React from 'react';
import { Wrapper, Dot } from './typing-dots.styles';

const TypingDots = () => {
  return (
    <Wrapper>
      <Dot/>
      <Dot/>
      <Dot/>
    </Wrapper>
  );
};

export default TypingDots;
