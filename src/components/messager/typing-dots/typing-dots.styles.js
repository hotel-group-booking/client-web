import styled, { keyframes } from 'styled-components';

const wave = keyframes`
  0%, 60%, 100% {
		transform: initial;
	}

	30% {
		transform: translateY(-8px);
	}
`;

const Wrapper = styled.div`
  position: relative;
  width: 40px;
  margin-left: auto;
  margin-right: auto;
  display: flex;
  align-items: flex-end;
  padding-bottom: 7px;
  justify-content: center;
`;

const Dot = styled.span`
  display: inline-block;
  width: 4px;
  height: 4px;
  border-radius: 50%;
  margin-right: 2px;
  background-color: var(--text-sub);
  animation: ${wave} 1.0s linear infinite;

  &:nth-child(2) {
    animation-delay: -.8s;
  }

  &:nth-child(3) {
    animation-delay: -0.7s;
  }
`;

export {
  Dot,
  Wrapper
}

