import React, { useState } from 'react';
import { range } from 'ramda';
import { useTranslation } from 'react-i18next';
import { ROOM_TYPES, GROUP_TYPES } from 'config/constants';
import { LocationAutocomplete } from 'components';
import { validator } from 'config/validator';
import {
  Wrapper,
  Form,
  Label,
  Row,
  Col,
  FormItem,
  RangePicker,
  InputNumber,
  Select,
  Option,
  Input,
  Slider,
  Footer,
  SubmitButton
} from './request-form.styles';

const RequestForm = ({
  onSubmit,
  form: { getFieldDecorator, validateFields, setFieldsValue },
  ...rest
}) => {
  const [location, setLocation] = useState();

  const [t] = useTranslation(['request-form', 'validator']);

  const handleSubmit = e => {
    e.preventDefault();
    validateFields((err, values) => {
      if (!err) {
        const request = {
          budgetFrom: values.budgetFrom,
          budgetTo: values.budgetTo,
          roomType: values.roomType,
          groupType: values.groupType,
          rooms: values.rooms,
          starRating: values.starRating,
          checkin: values.time[0].toISOString(),
          checkout: values.time[1].toISOString(),
          destination: {
            id: location.placeId,
            name: location.name
          },
          description: values.description || `${values.rooms} ${t('request-form:mat.rooms')} ${t('request-form:mat.at')} ${location.name}`
        };
        onSubmit(request);
      }
    })
  }

  const handleLocationSelect = location => {
    setLocation(location);
  };

  const handleSliderChange = values => {
    setFieldsValue({
      budgetFrom: values[0],
      budgetTo: values[1]
    });
  };

  const rules = validator(t);
  const checkin = () =>
    getFieldDecorator('time', {
      rules: [rules.required]
    })(
      <RangePicker
        format='D/M/YYYY'
        placeholder={[
          t('request-form:placeholders.checkin'),
          t('request-form:placeholders.checkout')
        ]}
      />
    );

  const rooms = () =>
    getFieldDecorator('rooms', {
      rules: [rules.required]
    })(
      <InputNumber
        min={1}
        max={100}
        placeholder={t('request-form:placeholders.rooms')}
      />
    );

  const starRating = () =>
    getFieldDecorator('starRating', {
      rules: [rules.required]
    })(
      <Select placeholder={t('request-form:placeholders.star rating')}>
        {range(1, 6).map(star => (
          <Option key={star} value={star}>
            <span>
              {star}{' '}
              {star > 1
                ? t('request-form:count.stars')
                : t('request-form:count.star')}
            </span>
          </Option>
        ))}
      </Select>
    );

  const roomType = () =>
    getFieldDecorator('roomType', {
      rules: [rules.required]
    })(
      <Select placeholder={t('request-form:placeholders.room type')}>
        {ROOM_TYPES.map(type => (
          <Option key={type.value} value={type.value}>
            {type.name}
          </Option>
        ))}
      </Select>
    );

  const groupType = () =>
    getFieldDecorator('groupType')(
      <Select placeholder={t('request-form:placeholders.group type')}>
        {GROUP_TYPES.map(type => (
          <Option key={type} value={type}>
            {type}
          </Option>
        ))}
      </Select>
    );

  const budgetFrom = () =>
    getFieldDecorator('budgetFrom')(
      <Input
        addonAfter='000 VND'
        placeholder={t('request-form:placeholders.budget from')}
      />
    );

  const budgetTo = () =>
    getFieldDecorator('budgetTo', {
      rules: [rules.required]
    })(
      <Input
        addonAfter='000 VND'
        placeholder={t('request-form:placeholders.budget to')}
      />
    );

  const description = () =>
    getFieldDecorator('description')(
      <Input placeholder={t('request-form:placeholders.description')} />
    );

  return (
    <Wrapper {...rest}>
      <Form onSubmit={handleSubmit}>
        <Row>
          <Col xs={24} mb='0.5rem'>
            <Label>{t('request-form:labels.destination')}</Label>
          </Col>
          <Col xs={24}>
            <LocationAutocomplete
              onSelect={handleLocationSelect}
              placeholder={t('request-form:placeholders.destination')}
            />
          </Col>
        </Row>

        <Row gutter={24}>
          <Col xs={24} lg={12}>
            <FormItem label={t('request-form:labels.time')}>
              {checkin()}
            </FormItem>
          </Col>
          <Col xs={12} lg={6}>
            <FormItem label={t('request-form:labels.rooms')}>
              {rooms()}
            </FormItem>
          </Col>
          <Col xs={12} lg={6}>
            <FormItem label={t('request-form:labels.star rating')}>
              {starRating()}
            </FormItem>
          </Col>
          <Col xs={12} lg={6}>
            <FormItem label={t('request-form:labels.room type')}>
              {roomType()}
            </FormItem>
          </Col>
          <Col xs={12} lg={6}>
            <FormItem label={t('request-form:labels.group type')}>
              {groupType()}
            </FormItem>
          </Col>

          <Col xs={24}>
            <FormItem label={t('request-form:labels.description')}>
              {description()}
            </FormItem>
          </Col>
        </Row>

        <Row>
          <Col xs={24} lg={6}>
            <FormItem
              label={t('request-form:labels.nightly budget')}
              colon={false}
              required
              mb='0'
            >
              {budgetFrom()}
            </FormItem>
            <Form.Item>{budgetTo()}</Form.Item>
          </Col>
          <Col xs={24} lg={18}>
            <Slider onSliderChange={handleSliderChange} />
          </Col>
        </Row>

        <Footer>
          <SubmitButton type='gradient' htmlType='submit'>
            {t('request-form:buttons.create request')}
          </SubmitButton>
        </Footer>
      </Form>
    </Wrapper>
  );
};

export default Form.create()(RequestForm);
