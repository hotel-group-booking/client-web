import styled from 'styled-components';
import {
  Form as AntForm,
  Row as AntRow,
  Col as AntCol,
  Input as AntInput,
  InputNumber as AntInputNumber,
  Select as AntSelect,
  DatePicker
} from 'antd';
import { BudgetSlider, Button } from 'components';

const Wrapper = styled.div``;

const Row = styled(AntRow).attrs(props => ({
  gutter: 24
}))``;

const Col = styled(AntCol)`
  margin-bottom: ${props => props.mb};
`;

const Input = styled(AntInput)`
  width: 100%;
`;

const Form = styled(AntForm)``;

const FormItem = styled(AntForm.Item)`
  margin-bottom: ${props => props.mb};
  label {
    font-size: 1rem;
    color: var(--text-em);
    font-weight: 500;
  }
`;

const Label = styled.span`
  font-size: 1rem;
  color: var(--text-em);
  font-weight: 500;

  ::before {
    display: inline-block;
    margin-right: 4px;
    color: #f5222d;
    font-size: 14px;
    font-family: SimSun, sans-serif;
    line-height: 1;
    content: '*';
  }
`;

const RangePicker = styled(DatePicker.RangePicker)`
  width: 100%;
`;

const InputNumber = styled(AntInputNumber)`
  width: 100%;
`;

const Select = styled(AntSelect)`
  width: 100%;
`;

const Option = styled(AntSelect.Option)``;

const Slider = styled(BudgetSlider)`
  margin-bottom: 0.5rem;

  @media (min-width: 992px) {
    margin-top: 46px;
    padding: 0 4rem;
  }
`;

const Footer = styled.div`
  display: flex;
  justify-content: center;
  margin-top: 1.5rem;
`;

const SubmitButton = styled(Button)`
  font-family: var(--font-baloo);
  font-size: 1rem;
  text-transform: uppercase;
  margin-top: .5rem;
`;

export {
  Wrapper,
  Row,
  Col,
  Input,
  Form,
  FormItem,
  Label,
  RangePicker,
  InputNumber,
  Select,
  Option,
  Slider,
  Footer,
  SubmitButton
};
