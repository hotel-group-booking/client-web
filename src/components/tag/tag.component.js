import React from 'react';
import { Wrapper } from './tag.styles';

const Tag = ({children, ...rest}) => {
  return (
    <Wrapper {...rest}>
      {children}
    </Wrapper>
  );
};

export default Tag;
