import styled from 'styled-components';

const Wrapper = styled.span`
  background-color: ${props => {
    switch (props.type) {
      case 'primary':
        return '#005b6d';
      case 'secondary':
        return 'rgb(224, 56, 143)';
      default:
        return 'rgb(61, 164, 211)';
    }
  }};
  color: var(--white);
  font-size: ${props => (props.large ? '1rem' : '.75rem')};
  padding: ${props => (props.large ? '.25rem 1rem' : '0 .25rem')};
  border-radius: ${props => (props.large ? '.3rem' : '.2rem')};
  margin-right: 0.5rem;
  margin-bottom: .25rem;
`;

export { Wrapper };
