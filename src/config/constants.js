const ROLE_PARTNER = 'partner';
const ROLE_USER = 'user';
const ROLE_MEMBER = 'member';
const GROUP_TYPES = [
  "Association",
  "Birthday party",
  "Business",
  "Bus tour",
  "Charity event",
  "Class reunion",
  "Convention",
  "Family reunion",
  "Holiday party",
  "Religious",
  "School trip",
  "Sport team",
  "Wedding",
];
const ROOM_TYPES = [
  { value: 1, name: 'Single Bed' },
  { value: 2, name: 'Double Bed' },
  { value: 3, name: 'Luxury' },
  { value: 4, name: 'Superior' },
];

const LANGUAGES = [
  'vi',
  'en'
];

export {
  ROLE_PARTNER,
  ROLE_USER,
  GROUP_TYPES,
  ROOM_TYPES,
  ROLE_MEMBER,
  LANGUAGES
};