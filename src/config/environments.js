const API_URL = process.env.NODE_ENV === 'development' ? 'http://localhost:8124' : 'https://hgb-test.herokuapp.com';

export { API_URL };