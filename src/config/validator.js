const validator = translation => {
  const t = translation;

  return {
    required: {
      required: true,
      message: t('validator:required')
    },
    
    minLength: {
      min: 6,
      message: t('validator:length')
    },

    maxLength: {
      max: 24,
      message: t('validator:length')
    },

    minValue: (min) => ({
      min,
      message: t('validator:minValue')
    }),

    confirm: (confirmValue) => ({
      validator: (rule, value, callback) => {
        if (value && value !== confirmValue) {
          callback(t('validator:password confirm wrong'));
        }
        callback();
      }
    }),

    year: tranKey => ({
      validator: (rule, value, callback) => {
        const maxYear = new Date().getFullYear();
        if (value && (value < 1900 || value > maxYear)) {
          callback(t(tranKey));
        }
        callback();
      }
    }),

    budget: (budgetFrom)=> ({
      validator: (rule, value, callback) => {
        if (budgetFrom === undefined || value === undefined) {
          callback(t('validator:budget empty'));
        } 
        if (value < budgetFrom) {
          callback(t('validator:budget invalid'));
        }
        callback();
      }
    })
  };
};

export {
  validator
}