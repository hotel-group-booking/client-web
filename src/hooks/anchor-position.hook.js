import { useState, useEffect } from 'react';
import { drop } from 'ramda';

const useAnchorPosition = id => {
  const [position, setPosition] = useState(0);
  useEffect(() => {
    const ele = document.getElementById(drop(1)(id));
    if (ele) {
      window.scrollTo({
        top: 0
      });
      setPosition(
        document.getElementById(drop(1)(id)).getBoundingClientRect().top
      );
    }
  }, [id]);
  return position;
};

export {
  useAnchorPosition
};