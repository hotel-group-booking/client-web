import { useEffect } from 'react';

const useAuthAction = callback => isAuth => {
  useEffect(() => {
    if (localStorage.getItem('isAuth')) {
      callback();
    }
  }, [isAuth])
};

export {
  useAuthAction
};