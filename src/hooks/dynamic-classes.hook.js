import { useState, useEffect } from 'react';
import { append, join } from 'ramda';

const useDynamicClasses = dynamicClass => (className, toggle) => {
  const [classes, setClasses] = useState([className]);

  useEffect(() => {
    setClasses(
      append(toggle ? dynamicClass : '', [className])
    );
  }, [className, toggle]);
  return join(' ', classes);
};

export {
  useDynamicClasses
};