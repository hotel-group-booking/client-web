import { useEffect } from 'react';
import { useTranslation } from 'react-i18next';


const useUpdateLanguage = (lng) => {
  const { i18n } = useTranslation();
  useEffect(() => {
    i18n.changeLanguage(lng);
  }, [lng]);
};

export {
  useUpdateLanguage
};