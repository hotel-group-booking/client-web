import { useEffect } from 'react';
import { includes } from 'ramda';
import { message } from 'antd';
import { useTranslation } from 'react-i18next';

const useRoleGuard = config => user => {
  const [t] = useTranslation();

  useEffect(() => {
    if (
      user &&
      !includes(user.role || localStorage.getItem('role'), config.allows)
    ) {
      message.error(t('role retricted'), 2);
      config.history.push(config.redirectTo);
    }
  }, [user]);
};

export { useRoleGuard };
