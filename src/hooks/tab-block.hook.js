import { useReducer, useEffect } from 'react';

const useTabBlock = (tab) => {
  const initialState = {
    '1': false,
    '2': false,
    '3': false,
  };

  const reducer = (state, action) => {
    switch (action.type) {
      case 'STEP_1': {
        return {
          '1': true,
          '2': false,
          '3': false,
        };
      }
  
      case 'STEP_2': {
        return {
          '1': true,
          '2': true,
          '3': false,
        };
      }
  
      case 'STEP_3': {
        return {
          '1': true,
          '2': true,
          '3': true
        };
      }
  
      default: return state;
    }
  };
  const [state, dispatch] = useReducer(reducer, initialState);

  useEffect(() => {
    const step = parseInt(tab) - 1;
    dispatch({ type: `STEP_${step}`});
  }, [tab])

  return state;
};

export { useTabBlock };