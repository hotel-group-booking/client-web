import { merge, compose, dropLast, reduce, keys, join } from 'ramda';
import { API_URL } from '../config/environments';
import jimp from 'jimp';
import moment from 'moment';

const callAPI = (endpoint = '/', method = 'GET', init) => {
  const initHeaders = init ? init : { 'Content-Type': 'application/json' };

  return ({ headers = {}, query = {} } = {}) => async body => {
    if (localStorage.getItem('token')) {
      initHeaders['Authorization'] = `Bearer ${localStorage.getItem('token')}`;
    }
    const options = {
      method,
      headers: merge(initHeaders, headers)
    };
    if (body) {
      options['body'] = JSON.stringify(body);
    }
    try {
      const queryString = compose(
        dropLast(1),
        reduce((queryString, key) => {
          const value = Array.isArray(query[key])
            ? reduce((acc, elem) => acc + `${key}=${elem}&`, '', query[key])
            : `${key}=${query[key]}&`;

          return queryString + value;
        }, '?'),
        keys
      )(query);
      const response = await fetch(
        `${API_URL}${endpoint}${queryString}`,
        options
      );
      const json = await response.json();
      return {
        status: response.status,
        ...json
      };
    } catch (err) {
      throw err;
    }
  };
};

const numberWithCommas = numb => {
  return numb.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.');
};

const parseQuery = query => {
  return compose(
    dropLast(1),
    reduce((queryString, key) => {
      const value = Array.isArray(query[key])
        ? `${key}=${JSON.stringify(query[key])}&`
        : `${key}=${query[key]}&`;

      return queryString + value;
    }, '?'),
    keys
  )(query);
};

const generateAPI = ({ headers, responseType, method = 'GET' } = {}) => (
  ...args
) => async ({ body, query = {} } = {}) => {
  const options = {
    method
  };

  if (headers) {
    options.headers = headers;
  }

  if (body) {
    options.body = JSON.stringify(body);
  }

  const response = await fetch(
    `${API_URL}/api/v2/${join('/', args)}${parseQuery(query)}`,
    options
  );

  if (!response.ok) {
    if (response.status === 401) {
      const message = await response.text();
      throw new Error(message);
    }
    const json = await response.json();
    throw new Error(json.message);
  }

  switch (responseType) {
    case 'text':
      return await response.text();
    case 'boolean':
      return response.ok;
    default:
      return await response.json();
  }
};

const base64Reader = height => file => {
  const reader = new FileReader();

  return new Promise((resolve, reject) => {
    reader.onerror = () => {
      reject(new DOMError('File parsing problem'));
    };

    reader.onload = () => {
      const { result } = reader;

      const buffer = Buffer.from(result);

      jimp
        .read(buffer)
        .then(img => {
          return img.resize(jimp.AUTO, height).quality(90);
        })
        .then(resizedImg => {
          return resizedImg.getBase64Async(jimp.MIME_JPEG);
        })
        .then(result => {
          resolve(result);
        });
      // resolve(reader.result);
    };
    reader.readAsArrayBuffer(file);
  });
};

const budgetFormat = (budgetFrom, budgetTo) => {
  const from = Number(budgetFrom * 1000).toLocaleString('vi-VN', {
    style: 'currency',
    currency: 'VND'
  });
  const to = Number(budgetTo * 1000).toLocaleString('vi-VN', {
    style: 'currency',
    currency: 'VND'
  });
  return `${from} - ${to}`;
};

const currencyFormat = value => {
  return Number(value * 1000).toLocaleString('vi-VN', {
    style: 'currency',
    currency: 'VND'
  });
};

const getDuration = (checkin, checkout) => {
  const cout = moment(checkout);
  const cin = moment(checkin);

  return moment.duration(cout.diff(cin)).asDays();
};

export {
  callAPI,
  numberWithCommas,
  parseQuery,
  generateAPI,
  base64Reader,
  budgetFormat,
  currencyFormat,
  getDuration
};
