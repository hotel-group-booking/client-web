import React from 'react';
import Selector from './selector/selector.component';
import {
  Wrapper,
  Section,
  Col,
  Content,
  Title,
  Description,
  Text,
  Decorator,
  CircleDecorator,
  StrikeDecorator,
} from './body.styles';
import { useTranslation } from 'react-i18next';

const Body = props => {
  const [t] = useTranslation('home-page');

  return (
    <Wrapper {...props}>
      <Section>
        <Col xs={24} lg={12} id='introduction'>
          <Content>
            <Title>{t('home-page:contents.1.title')}</Title>
            <Description>{t('home-page:contents.1.description')}</Description>
            <Text>{t('home-page:contents.1.text')}</Text>
          </Content>
        </Col>
        <Col xs={0} lg={12}>
          <Decorator />
        </Col>
      </Section>

      <Section>
        <Col xs={0} lg={10}>
          <CircleDecorator/>
        </Col>
        <Col xs={24} lg={14}>
          <Content start>
            <Title>{t('home-page:contents.2.title')}</Title>
            <Description>{t('home-page:contents.2.description')}</Description>
            <Text>{t('home-page:contents.2.text')}</Text>
          </Content>
        </Col>
      </Section>

      <Section>
        <Col xs={24} lg={12}>
          <Content>
            <Title>{t('home-page:contents.1.title')}</Title>
            <Description>{t('home-page:contents.1.description')}</Description>
            <Text>{t('home-page:contents.1.text')}</Text>
          </Content>
        </Col>
        <Col xs={0} lg={12}>
          <StrikeDecorator/>
        </Col>
      </Section>

      <Section id='signup'>
       <Selector/>
      </Section>
    </Wrapper>
  );
};

export default Body;
