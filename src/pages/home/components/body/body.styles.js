import styled from 'styled-components';
import img1 from './assets/abstract-1.png';
import img2 from './assets/abstract-2.png';
import { Row as AntRow, Col as AntCol } from 'antd';

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  /* max-width: 100vw; */
`;

const Section = styled(AntRow).attrs(props => ({
  gutter: 0
}))`
  padding: 2rem 1rem;
`;

const Col = styled(AntCol)`
  @media (min-width: 992px) {
    display: flex;
    flex-direction: column;
    justify-content: center;
    height: 500px;
  }
`;

const Content = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  z-index: 2;

  @media (min-width: 992px) {
    align-items: ${props => props.start ? 'flex-start' : 'flex-end'};
  }
`;

const Title = styled.div`
  width: 100%;
  text-align: center;
  font-size: 2rem;
  color: rgb(218, 242, 254);
  font-family: var(--font-baloo);

  @media (min-width: 992px) {
    font-size: 3rem;
    text-align: left;
    max-width: 550px;
  }
`;

const Description = styled.div`
  width: 100%;
  text-align: center;
  color: var(--text-title);
  font-size: 1.25rem;
  font-family: var(--font-baloo);
  padding: 0 2rem;
  max-width: 400px;

  @media (min-width: 992px) {
    font-size: 2rem;
    text-align: left;
    max-width: 550px;
    padding: 0;
  }
`;

const Text = styled.div`
  width: 100%;
  text-align: center;
  font-family: var(--font-baloo);
  font-size: 1rem;
  padding: 0.5rem 2rem;
  max-width: 400px;

  @media (min-width: 992px) {
    font-size: 1.25rem;
    text-align: left;
    max-width: 550px;
    padding: 0.5rem 0;
  }
`;

const Decorator = styled.div`
  width: 100%;
  height: 100%;
  background-image: url(${img1});
  background-position: left center;
`;

const CircleDecorator = styled.div`
  position: relative;
  width: 100%;
  height: 100%;

  ::before {
    content: '';
    position: absolute;
    background-color: #166986;
    width: 850px;
    height: 850px;
    left: -330px;
    bottom: -150px;
    border-radius: 50%;
    
    @media (max-width: 1500px) {
      height: 770px;
      width: 770px;
    }
  }
`;

const StrikeDecorator = styled.div`
  width: 100%;
  height: 100%;
  background-image: url(${img2});
  background-position: left center;
`;


export {
  Wrapper,
  Section,
  Col,
  Content,
  Title,
  Description,
  Text,
  Decorator,
  CircleDecorator,
  StrikeDecorator
};
