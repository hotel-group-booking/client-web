import React, { useReducer } from 'react';
import { useTranslation } from 'react-i18next';
import { withRouter } from 'react-router-dom';
import {
  Wrapper,
  Row,
  Col,
  Section,
  MemberIcon,
  PartnerIcon,
  Description,
  Title,
  Button
} from './selector.styles';

const initialState = {
  left: 12,
  right: 12,
};

const reducer = (state, action) => {
  switch(action.type) {
    case 'LEFT_ENTER': {
      return {
        left: 16,
        right: 8
      };
    }

    case 'LEFT_LEAVE': {
      return initialState;
    }

    case 'RIGHT_ENTER': {
      return {
        left: 8,
        right: 16
      };
    }

    case 'RIGHT_LEAVE': {
      return initialState;
    }

    default: return state;
  }
};

const Selector = ({history, ...rest}) => {
  const [state, dispatch] = useReducer(reducer, initialState);
  const [t] = useTranslation('home-page');

  const handleMouseEnter = position => _ => {
    dispatch({ type: `${position}_ENTER` });
  };

  const handleMouseLeave = position => _ => {
    dispatch({ type: `${position}_LEAVE` });
  };

  const handleRedirect = url => e => {
    history.push(url);
  };

  return (
    <Wrapper {...rest}>
      <Row>
        <Col xs={24} lg={state.left}>
          <Section 
            onMouseEnter={handleMouseEnter('LEFT')}
            onMouseLeave={handleMouseLeave('LEFT')}
          >
            <MemberIcon />
            <Title>{t('home-page:registration.member.title')}</Title>
            <Description>
              {t('home-page:registration.member.description')}
            </Description>
            <Button 
              type='gradient'
              onClick={handleRedirect('/registration/member')}
            >
              {t('home-page:registration.member.button')}
            </Button>
          </Section>
        </Col>
        <Col xs={24} lg={state.right}>
          <Section
            onMouseEnter={handleMouseEnter('RIGHT')}
            onMouseLeave={handleMouseLeave('RIGHT')}
          >
            <PartnerIcon />
            <Title>{t('home-page:registration.partner.title')}</Title>
            <Description>
              {t('home-page:registration.partner.description')}
            </Description>
            <Button 
              type='gradient' 
              onClick={handleRedirect('/registration/partner')}
            >
              {t('home-page:registration.partner.button')}
            </Button>
          </Section>
        </Col>
      </Row>
    </Wrapper>
  );
};

export default withRouter(Selector);
