import styled from 'styled-components';
import { Row as AntRow, Col as AntCol, Icon } from 'antd';
import { Users } from 'react-feather';
import { Button as SharedButton } from 'components';

const Wrapper = styled.div``;

const Row = styled(AntRow).attrs(props => ({
  gutter: 24
}))`
  @media (min-width: 992px) {
    padding: 0 2rem;
  }
`;

const Col = styled(AntCol)`
  transition: width 0.5s;
`;

const Section = styled.div`
  height: 300px;
  background-color: var(--white);
  border-radius: 0.5rem;
  box-shadow: var(--highlight);
  margin: 0.5rem 0;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;

  @media (min-width: 992px) {
    height: 480px;
  }
`;

const MemberIcon = styled(Users).attrs(props => ({
  size: 48
}))`
  color: var(--text-title);
`;

const PartnerIcon = styled(Icon).attrs(props => ({
  type: 'book'
}))`
  svg {
    width: 48px;
    height: 48px;
    color: var(--text-title);
  }
`;

const Title = styled.div`
  font-size: 2rem;
  font-weight: 700;
  color: var(--text-title);
  text-align: center;
`;

const Description = styled.div`
  font-size: 1.2rem;
  font-weight: 500;
  color: var(--text-title);
  text-align: center;
`;

const Button = styled(SharedButton)`
  margin-top: 0.5rem;
`;

export {
  Wrapper,
  Row,
  Col,
  Section,
  MemberIcon,
  PartnerIcon,
  Title,
  Description,
  Button
};
