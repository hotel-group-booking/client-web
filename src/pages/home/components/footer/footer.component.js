import React from 'react';
import { Wrapper, Row, Col, Header, Item, CopyrightSection } from './footer.styles';
import { useTranslation } from 'react-i18next';

const Footer = props => {
  const [t] = useTranslation('home-page');

  return (
    <Wrapper>
      <Row>
        <Col xs={12} lg={6}>
          <Header>{t('home-page:footer.1.header')}</Header>
          <Item>{t('home-page:footer.1.items.1')}</Item>
          <Item>{t('home-page:footer.1.items.2')}</Item>
          <Item>{t('home-page:footer.1.items.3')}</Item>
          <Item>{t('home-page:footer.1.items.4')}</Item>
        </Col>
        <Col xs={12} lg={6}>
          <Header>{t('home-page:footer.2.header')}</Header>
          <Item>{t('home-page:footer.2.items.1')}</Item>
          <Item>{t('home-page:footer.2.items.2')}</Item>
          <Item>{t('home-page:footer.2.items.3')}</Item>
          <Item>{t('home-page:footer.2.items.4')}</Item>
        </Col>
        <Col xs={12} lg={6}>
          <Header>{t('home-page:footer.3.header')}</Header>
          <Item>{t('home-page:footer.3.items.1')}</Item>
          <Item>{t('home-page:footer.3.items.2')}</Item>
        </Col>
        <Col xs={12} lg={6}>
          <Header>{t('home-page:footer.4.header')}</Header>
          <Item>{t('home-page:footer.4.items.1')}</Item>
          <Item>{t('home-page:footer.4.items.2')}</Item>
        </Col>
      </Row>
      <CopyrightSection>
        &copy; 2019 Hotel Group Booking. All right reserved.
      </CopyrightSection>
    </Wrapper>
  );
};

export default Footer;
