import styled from 'styled-components';
import { Row as AntRow, Col as AntCol } from 'antd';

const Wrapper = styled.div`
  width: 100%;
  background-color: var(--text-title);
  padding: 3rem 0 1rem 0;
`;

const Row = styled(AntRow).attrs(props => ({
  gutter: 0
}))`
  color: var(--white);
`;

const Col = styled(AntCol)`
  display: flex;
  flex-direction: column;
  align-items: center;
  padding: 1rem .5rem;
`;

const Header = styled.div`
  text-align: center;
  font-size: 1rem;
  margin-bottom: 1rem;
  font-weight: 500;
  text-transform: uppercase;
`;

const Item = styled.div`
  text-align: center;
  font-size: .9rem;
  color: var(--text-sub);
  border-bottom: 1px solid transparent;
  cursor: pointer;

  :hover {
    color: var(--white);
    border-bottom-color: var(--white);
  }
`;

const CopyrightSection = styled.div`
  color: var(--text-sub);
  text-align: center;
  padding: 2rem 4rem 0 4rem;

  @media (min-width: 992px) {
    text-align: right;
    font-size: 1rem;
  }
`;

export {
  Wrapper,
  Row,
  Col,
  Header,
  Item,
  CopyrightSection
};
