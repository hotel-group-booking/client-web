import React from 'react';
import Navbar from './navbar/navbar.component';
import Intro from './intro/intro.component';
import { Wrapper } from './header.styles';

const Header = (props) => {
  return (
    <Wrapper {...props}>
      <Intro/>
      <Navbar/>
    </Wrapper>
  );
};

export default Header;
