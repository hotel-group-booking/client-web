import styled from 'styled-components';

// Breakpoint: >= 768px (md)
const Wrapper = styled.div`
  height: 100%;
  width: 100%;
  display: flex;
  flex-direction: column;

  @media (min-width: 768px) {
  }
`;

export {
  Wrapper
}