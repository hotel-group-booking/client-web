import React, { useState } from 'react';
import { compose } from 'ramda';
import { withRouter } from 'react-router-dom';
import { useTranslation } from 'react-i18next';
import { Facebook, Instagram } from 'react-feather';
import { connect } from 'react-redux';
import { getSettings, doChangeLanguage } from 'reducers/application';
import { doSaveRequest } from 'reducers/request';
import { RequestForm } from 'components';
import { Modal } from 'antd';
import {
  Wrapper,
  ContentWrapper,
  MessageSection,
  Message,
  Title,
  Button,
  ActionsGroupWrapper,
  LanguageSelector,
  LanguagueItem,
  Line,
  SocialMedia,
  FormSection,
  Btn,
  ModalContent,
  ModalBody,
  ModalHeader
} from './intro.styles';

const Intro = ({ history, settings, changeLanguage, saveRequest, ...rest }) => {
  const [mode, setMode] = useState(false);
  const [visible, setVisible] = useState(false);
  const [t] = useTranslation('home-page');

  const handleChangeLanguage = lng => e => {
    changeLanguage(lng);
  };

  const handleChangeMode = e => {
    setMode(true);
  };

  const handleSubmit = request => {
    saveRequest(request);
    setVisible(true);
  };

  const handleCancel = e => {
    saveRequest(undefined);
    setVisible(false);
  };

  const handleRedirect = url => e => {
    history.push(url);
  };

  return (
    <Wrapper {...rest}>
      <ContentWrapper>
        {mode ? (
          <FormSection>
            <RequestForm onSubmit={handleSubmit} />
          </FormSection>
        ) : (
          <MessageSection>
            <Title>Hotel Group Booking</Title>
            <Message>{t('home-page:header message')}</Message>
            <Button type='gradient' onClick={handleChangeMode}>
              <span to='#signup' offset={80}>
                {t('home-page:intro button label')}
              </span>
            </Button>
          </MessageSection>
        )}
      </ContentWrapper>
      <ActionsGroupWrapper>
        <LanguageSelector>
          <LanguagueItem
            active={settings.lng === 'en'}
            onClick={handleChangeLanguage('en')}
          >
            EN
          </LanguagueItem>
          <LanguagueItem
            active={settings.lng === 'vi'}
            onClick={handleChangeLanguage('vi')}
          >
            VI
          </LanguagueItem>
          <Line />
        </LanguageSelector>
        <SocialMedia>
          <Line />
          <Facebook />
          <Instagram />
        </SocialMedia>
      </ActionsGroupWrapper>
      <Modal
        visible={visible}
        closable={false}
        footer={[
          <Btn onClick={handleCancel} key='cancel'>
            {t('cancel')}
          </Btn>
        ]}
      >
        <ModalContent>
          <ModalHeader>{t('home-page:finish create request')}</ModalHeader>
          <ModalBody>
            <Button
              onClick={handleRedirect('/registration/member')}
              mr='.5rem'
              type='gradient'
            >
              <span>{t('home-page:sign up')}</span>
            </Button>

            <Button onClick={handleRedirect('/login')} type='gradient'>
              <span>{t('home-page:sign in')}</span>
            </Button>
          </ModalBody>
        </ModalContent>
      </Modal>
    </Wrapper>
  );
};

const mapStateToProps = state => ({
  settings: getSettings(state)
});

const mapDispatchToProps = dispatch => ({
  changeLanguage: lng => dispatch(doChangeLanguage(lng)),
  saveRequest: request => dispatch(doSaveRequest(request))
});

export default compose(
  connect(
    mapStateToProps,
    mapDispatchToProps
  ),
  withRouter
)(Intro);
