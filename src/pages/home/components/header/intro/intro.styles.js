import styled, { keyframes } from 'styled-components';
import background from './assets/background.jpg';
import { Button as SharedButton, HashLink } from 'components';
import { Divider, Button as AntButton } from 'antd';

const shilft = keyframes`
  0% {
    transform: translateY(75px);
  }
  50% {
    opacity: .8;
  }
  100% {
    transform: translateY(0);
    opacity: 1;
  }
`;

const Wrapper = styled.div`
  flex-grow: 1;
  display: none;
  padding: 3rem 6rem;
  background-image: url(${background});
  background-size: cover;
  background-position: center;
  background-attachment: fixed;
  background-repeat: no-repeat;
  position: relative;

  ::after {
    content: '';
    opacity: 0.6;
    background-color: #000;
    top: 0;
    left: 0;
    bottom: 0;
    right: 0;
    position: absolute;
    z-index: 1;
  }

  @media (min-width: 992px) {
    display: flex;
  }
`;

// Content Styles
const ContentWrapper = styled.div`
  z-index: 2;
  display: flex;
  flex-direction: row;
`;

const MessageSection = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
`;

const Title = styled.span`
  font-size: 3rem;
  color: var(--white);
  font-family: var(--font-baloo);
`;

const Message = styled.span`
  font-size: 1.5rem;
  color: var(--white);
  font-family: var(--font-baloo);
  width: 300px;
`;

const Button = styled(SharedButton)`
  font-family: var(--font-baloo);
  font-size: 1.2rem;
  margin-top: 1.5rem;
  padding: 0;
  margin-right: ${props => props.mr};

  span {
    padding: 0 1.5rem;
    color: var(--white);
  }
`;

// ActionsGroup
const ActionsGroupWrapper = styled.div`
  margin-left: auto;
  z-index: 2;
  display: flex;
  flex-direction: column;
`;

const LanguageSelector = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
`;

const LanguagueItem = styled.span`
  font-family: var(--font-baloo);
  color: ${props => (props.active ? 'var(--teal)' : 'var(--white)')};
  font-size: 1rem;
  text-align: center;
  margin-bottom: 0.5rem;
  cursor: pointer;
`;

const Line = styled(Divider).attrs(props => ({
  type: 'vertical'
}))`
  height: 4rem;
`;

const SocialMedia = styled.div`
  margin-top: auto;
  display: flex;
  flex-direction: column;
  align-items: center;

  svg {
    margin-top: 0.8rem;
    color: var(--white);
    cursor: pointer;
  }
`;

const Link = styled(HashLink)`
  color: var(--white);
  display: inline-block;
  width: 100%;
  height: 100%;
  padding: 0 1.5rem;

  :hover {
    border: none;
    color: var(--white);
  }
`;

const FormSection = styled.div`
  display: flex;
  background-color: var(--white);
  border-radius: .5rem;
  padding: 2rem;
  max-width: 700px;
  animation: ${shilft} .5s;
`;

const Btn = styled(AntButton)``;

const ModalContent = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
`;

const ModalHeader = styled.span`
  font-size: 1.4rem;
  font-family: var(--font-baloo);
`;

const ModalBody = styled.div`
  display: flex;
  justify-content: center;
`;

export {
  Wrapper,
  ContentWrapper,
  MessageSection,
  Title,
  Message,
  Button,
  ActionsGroupWrapper,
  LanguageSelector,
  LanguagueItem,
  Line,
  SocialMedia,
  Link,
  FormSection,
  Btn,
  ModalContent,
  ModalBody,
  ModalHeader
};
