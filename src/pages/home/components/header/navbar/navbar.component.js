import React, { useState } from 'react';
import { connect } from 'react-redux';
import { compose } from 'ramda';
import { getUser, getIsFetchingData } from 'reducers/user';
import { withRouter } from 'react-router-dom';
import { Affix } from 'antd';
import { User } from 'react-feather';
import {
  Wrapper,
  AppIcon,
  Navigator,
  MenuIcon,
  MenuDrawer,
  MenuDrawerInner,
  DrawerHeader,
  DrawerItemsGroup,
  DrawerItem,
  UserActions,
  Label,
  Dropdown,
  DropdownItem,
  MenuSection,
  UserGroup,
  Brand,
  LinkSection,
  Link,
  SignInButton,
  DropdownLink,
  Username
} from './navbar.styles';
import { useTranslation } from 'react-i18next';

const Navbar = ({ history, user, isFetchingData, ...rest }) => {
  const [menuVisible, setMenuVisible] = useState(false);
  const [userVisible, setUserVisible] = useState(false);
  const [t] = useTranslation('home-page');

  const handleRedirect = link => e => {
    history.push(link);
  };

  const handleMenuClick = e => {
    setMenuVisible(!menuVisible);
  };

  const handleUserClick = e => {
    setUserVisible(!userVisible);
  };

  const handleDrawerItemClick = e => {
    setMenuVisible(false);
  };

  const handleDropdownItemClick = e => {
    setUserVisible(false);
  };

  return (
    <Affix>
      <Wrapper {...rest}>
        <Navigator>
          <MenuIcon size={28} onClick={handleMenuClick} />
          <MenuSection>
            <Brand onClick={handleRedirect('/')}>Hotel Group Booking</Brand>
            <LinkSection>
              <Link to='#introduction' offset={80}>
                {t('home-page:introduction')}
              </Link>
              <Link to='#body' offset={80}>
                {t('home-page:about us')}
              </Link>
              <Link to='#contact' offset={80}>
                {t('home-page:contact')}
              </Link>
            </LinkSection>
          </MenuSection>
        </Navigator>

        <AppIcon size={36} onClick={handleRedirect('/')} />

        <UserActions>
          <Label expanded={userVisible} onClick={handleUserClick}>
            <User size={24} />
          </Label>
          <Dropdown open={userVisible}>
            {user ? (
              <DropdownItem onClick={handleRedirect(`/${user.role}`)}>
                <Username size={14}>{user.displayName}</Username>
              </DropdownItem>
            ) : (
              <>
                <DropdownItem onClick={handleDropdownItemClick}>
                  <DropdownLink to='#signup' offset={48}>
                    {t('home-page:sign up')}
                  </DropdownLink>
                </DropdownItem>
                <DropdownItem onClick={handleRedirect('/login')}>
                  {t('home-page:sign in')}
                </DropdownItem>
              </>
            )}
          </Dropdown>
        </UserActions>

        <UserGroup>
          {user ? (
            <Username onClick={handleRedirect(`/${user.role}`)}>
              {user.displayName}
            </Username>
          ) : (
            <SignInButton
              type='gradient'
              size={{ height: '40px' }}
              onClick={handleRedirect('/login')}
            >
              {t('home-page:sign in')}
            </SignInButton>
          )}
        </UserGroup>

        <MenuDrawer visible={menuVisible} onClose={() => setMenuVisible(false)}>
          <MenuDrawerInner>
            <DrawerHeader onClick={handleRedirect}>
              Hotel Group Booking
            </DrawerHeader>
            <DrawerItemsGroup>
              <DrawerItem onClick={handleDrawerItemClick} emp>
                <span>{t('home-page:make request')}</span>
              </DrawerItem>
              <DrawerItem onClick={handleDrawerItemClick}>
                <Link to='#introduction' offset={48} drawer>
                  {t('home-page:introduction')}
                </Link>
              </DrawerItem>
              <DrawerItem onClick={handleDrawerItemClick}>
                <Link to='#body' offset={48} drawer>
                  {t('home-page:about us')}
                </Link>
              </DrawerItem>
              <DrawerItem onClick={handleDrawerItemClick}>
                <Link to='#contact' offset={48} drawer>
                  {t('home-page:contact')}
                </Link>
              </DrawerItem>
            </DrawerItemsGroup>
          </MenuDrawerInner>
        </MenuDrawer>
      </Wrapper>
    </Affix>
  );
};

const mapStateToProps = state => ({
  user: getUser(state),
  isFetchingData: getIsFetchingData(state)
});

export default compose(
  connect(mapStateToProps),
  withRouter
)(Navbar);
