import styled from 'styled-components';
import { Logo, Drawer, HashLink, Button } from 'components';
import { Menu } from 'react-feather';

const Wrapper = styled.div`
  display: flex;
  flex-direction: row;
  height: 3rem;
  align-items: center;
  padding: 0 1rem;
  background-color: var(--white);
  box-shadow: var(--highlight);
  justify-content: space-between;

  @media (min-width: 992px) {
    justify-content: flex-start;
    padding: 0 3rem;
    height: 5rem;
  }
`;

const AppIcon = styled(Logo)`
  fill: var(--brand-color);
  cursor: pointer;
  transition: fill 0.2s;
  :hover {
    fill: var(--text-title);
  }

  @media (min-width: 992px) {
    display: none;
  }
`;

const Navigator = styled.div`
  position: relative;
`;

const MenuIcon = styled(Menu)`
  color: var(--brand-color);
  cursor: pointer;
  transition: fill 0.2s;

  :hover {
    color: var(--text-title);
  }

  @media (min-width: 992px) {
    display: none;
  }
`;

const MenuSection = styled.div`
  display: none;

  @media (min-width: 992px) {
    display: flex;
    flex-direction: row;
  }
`;

const Brand = styled.span`
  font-size: 2rem;
  color: var(--text-title);
  font-family: var(--font-baloo);
  cursor: pointer;
  transition: color 0.2s;

  :hover {
    color: var(--teal);
  }
`;

const LinkSection = styled.div`
  display: flex;
  align-items: center;
  margin-left: 3rem;
`;

const Link = styled(HashLink).attrs(props => ({
  offset: 80
}))`
  font-size: ${props => (props.drawer ? '1rem' : '1.2rem')};
  color: var(--text-title);
  font-family: var(--font-baloo);
  margin-right: 1rem;
  transition: color 0.2s;

  ${props => props.drawer && 
    `
      display: inline-block;
      width: 100%;
      height: 100%;
      line-height: 40px;
    `
  };

  :hover {
    color: var(--teal);
    border-bottom: ${props =>
      props.drawer ? 'none' : '1px solid var(--text-title)'};
  }
`;

// Drawer Styles
const MenuDrawer = styled(Drawer).attrs(props => ({
  placement: 'left'
}))``;

const MenuDrawerInner = styled.div`
  height: 100%;
  display: flex;
  flex-direction: column;
  background-color: var(--white);
`;

const DrawerHeader = styled.span`
  font-size: 1.2rem;
  color: var(--brand-color);
  font-family: var(--font-baloo);
  text-align: center;
  padding: 0.7rem;
  height: 3rem;
  border-bottom: 1px solid var(--brand-color);
  cursor: pointer;

  :hover {
    color: var(--text-title);
  }
`;

const DrawerItemsGroup = styled.div`
  padding: 0.5rem 1rem;
  display: flex;
  flex-direction: column;
`;

const DrawerItem = styled.div`
  width: 100%;
  display: flex;
  flex-direction: row;
  justify-content: flex-start;
  align-items: center;
  padding: 0 1rem;
  height: ${props => (props.emp ? '3rem' : '2.5rem')};
  border-radius: ${props => (props.emp ? '1.5rem' : '1.25rem')};
  font-family: var(--font-baloo);
  cursor: pointer;
  transition: background-color 0.2s;

  span {
    font-size: ${props => (props.emp ? '1.2rem' : '1rem')};
    color: var(--text-em);
  }

  :hover {
    background-color: var(--text-sub);
  }
`;

// User Styles
const UserActions = styled.div`
  display: flex;
  flex-direction: row;
  position: relative;

  @media (min-width: 992px) {
    display: none;
  }
`;

const Label = styled.div`
  font-size: 1rem;
  display: flex;
  align-items: center;
  cursor: pointer;

  span {
    margin-right: 0.25rem;
    color: var(--brand-color);
    font-weight: 600;
    font-size: 1rem;
    transition: color 0.2s;
  }

  svg {
    color: var(--brand-color);
    transition: color 0.2s;
  }

  :hover {
    span {
      color: var(--text-em);
    }

    svg {
      color: var(--text-em);
    }
  }
`;

const Dropdown = styled.div`
  position: absolute;
  top: 45px;
  left: -90px;
  width: 120px;
  background-color: var(--white);
  box-shadow: var(--highlight);
  border-radius: 0.5rem;
  padding: 0.5rem;
  flex-direction: column;
  display: ${props => (props.open ? 'flex' : 'none')};
  z-index: 10;
`;

const DropdownItem = styled.span`
  font-size: 1rem;
  color: var(--text-em);
  font-weight: 500;
  padding: 0.25rem 0.5rem;
  border-radius: 0.25rem;
  cursor: pointer;
  text-align: center;

  :hover {
    background-color: var(--text-sub);
  }
`;

const UserGroup = styled.div`
  display: none;

  @media (min-width: 992px) {
    display: flex;
    margin-left: auto;
  }
`;

const SignInButton = styled(Button)`
  font-family: var(--font-baloo);
`;

const DropdownLink = styled(HashLink)`
  font-size: 1rem;
  color: var(--text-em);
  font-weight: 500;
  padding: 0.25rem .5rem;
  border-radius: .25rem;
  text-align: center;
  display: block;
  width: 100%;
  height: 100%;
  cursor: pointer;
  
  :hover {
    background-color: var(--text-sub);
    color: var(--text-em);
  }
`;

const Username = styled.div`
  font-size: ${props => props.size ? `${props.size}px` : '1.2rem'};
  color: var(--text-title);
  font-family: var(--font-baloo);
  cursor: pointer;

  :hover {
    color: var(--teal);
  }
`;

export {
  Wrapper,
  AppIcon,
  Navigator,
  MenuIcon,
  MenuSection,
  MenuDrawer,
  MenuDrawerInner,
  DrawerHeader,
  DrawerItemsGroup,
  DrawerItem,
  UserActions,
  Label,
  Dropdown,
  DropdownItem,
  DropdownLink,
  UserGroup,
  Brand,
  LinkSection,
  Link,
  SignInButton,
  Username
};
