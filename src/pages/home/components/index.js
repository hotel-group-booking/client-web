export { default as Header } from './header/header.component';
export { default as Body } from './body/body.component';
export { default as Footer } from './footer/footer.component';