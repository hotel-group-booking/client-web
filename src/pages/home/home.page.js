import React, { useEffect } from 'react';
import { drop } from 'ramda';
import { Header, Body, Footer } from './components';
import {
  Wrapper,
  HeaderSection,
  BodySection,
  FooterSection
} from './home.styles';

const HomePage = ({ history }) => {

  useEffect(() => {
    if (history.location.hash) {
      window.scrollTo({
        top: 0
      });
      const element = document.getElementById(drop(1, history.location.hash));
      if (element) {
        const position = element.getBoundingClientRect().top;
        const offsetPosition = position - 80;
        window.scrollTo({
          top: offsetPosition,
          behavior: 'smooth'
        });
      }
    }
  }, []);

  return (
    <Wrapper>
      <HeaderSection>
        <Header />
      </HeaderSection>
      <BodySection id='body'>
        <Body/>
      </BodySection>
      <FooterSection id='contact'>
        <Footer/>
      </FooterSection>
    </Wrapper>
  );
};

export default HomePage;
