import styled from 'styled-components';

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  background-color: var(--background-primary);
`;

const HeaderSection = styled.div`
  height: 3rem;
  
  @media (min-width: 992px) {
    height: 900px;
  }
`;

const BodySection = styled.div`
  flex-grow: 1;
`;

const FooterSection = styled.div``;


export {
  Wrapper,
  HeaderSection,
  BodySection,
  FooterSection
};
