import React, { useEffect } from 'react';
import { isEmpty } from 'ramda';
import { connect } from 'react-redux';
import { useTranslation } from 'react-i18next';
import { getHotel, doFetchHotel } from 'reducers/hotel';
import Map from './map/map.component';
import {
  Wrapper,
  HotelInformationSection,
  Label,
  ContentContainer,
  Item,
  ItemLabel,
  Content,
  SlideshowSection,
  PhotoContainer,
  Carousel,
  Row,
  Col,
  AdditionSection,
  Card,
  Title,
  CardContent
} from './hotel-detail.styles';

const HotelDetail = ({ history, match, hotel, fetchHotel, ...rest }) => {
  const [t] = useTranslation('hotel-page');

  useEffect(() => {
    fetchHotel(match.params.id);
  }, []);

  console.log(hotel);

  return hotel ? (
    <Wrapper>
      <HotelInformationSection>
        <Label>{hotel.name}</Label>
        <ContentContainer mt='0.5rem'>
          <Item>
            <ItemLabel>{t('hotel-page:hotel-detail.labels.address')}</ItemLabel>
            <Content>{hotel.address.name}</Content>
          </Item>
          <Item>
            <ItemLabel>
              {t('hotel-page:hotel-detail.labels.checkin time')}
            </ItemLabel>
            <Content>
              {hotel.checkinTime} - {hotel.checkoutTime}
            </Content>
          </Item>
        </ContentContainer>
      </HotelInformationSection>
      <Row gutter={16} mt='1rem'>
        <Col xs={24} lg={16}>
          <SlideshowSection>
            <Carousel autoplay>
              {hotel.images.map(image => (
                <PhotoContainer>
                  <img src={image.link} alt='' />
                </PhotoContainer>
              ))}
            </Carousel>
          </SlideshowSection>
        </Col>
        <Col xs={24} lg={8}>
          <HotelInformationSection>
            <ContentContainer>
              <Item>
                <ItemLabel>
                  {t('hotel-page:hotel-detail.labels.rooms')}
                </ItemLabel>
                <Content>{`${hotel.totalRoom} ${t(
                  'hotel-page:count.rooms'
                )}`}</Content>
              </Item>
              <Item>
                <ItemLabel>
                  {t('hotel-page:hotel-detail.labels.star rating')}
                </ItemLabel>
                <Content>
                  {`${hotel.starRating} ${
                    hotel.starRating > 1
                      ? t('hotel-page:count.stars')
                      : t('hotel-page:count.star')
                  }`}
                </Content>
              </Item>
            </ContentContainer>
          </HotelInformationSection>
        </Col>
      </Row>
      <HotelInformationSection mt='1rem'>
        <Label>{t('hotel-page:hotel-detail.labels.map')}</Label>
        <ContentContainer mt='0.5rem'>
          <Map
            lat={hotel.address.lat}
            lng={hotel.address.lng}
            name={hotel.name}
          />
        </ContentContainer>
      </HotelInformationSection>
      {!isEmpty(hotel.extraInfo) && (
        <HotelInformationSection mt='1rem'>
          <Label>{t('hotel-page:hotel-detail.labels.other information')}</Label>
          <AdditionSection>
            <Row gutter={16}>
              {hotel.extraInfo.map((info, i) => (
                <Col key={i} xs={8}>
                  <Card>
                    <Title>{info.title}</Title>
                    <CardContent>{info.content}</CardContent>
                  </Card>
                </Col>
              ))}
            </Row>
          </AdditionSection>
        </HotelInformationSection>
      )}
    </Wrapper>
  ) : null;
};

const mapStateToProps = state => ({
  hotel: getHotel(state)
});

const mapDispatchToProps = dispatch => ({
  fetchHotel: id => dispatch(doFetchHotel(id))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(HotelDetail);
