import styled from 'styled-components';
import { Carousel as AntCarousel, Row as AntRow, Col as AntCol } from 'antd';

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  margin: 0.25rem;
`;

const HotelInformationSection = styled.div`
  display: flex;
  flex-direction: column;
  margin-top: ${props => props.mt};
`;

const Label = styled.div`
  font-size: 1.25rem;
  color: var(--text-title);
  font-weight: 500;
`;

const ContentContainer = styled.div`
  background-color: var(--white);
  padding: 1rem;
  display: flex;
  flex-direction: column;
  box-shadow: var(--highlight);
  border-radius: 0.5rem;
  margin-top: ${props => props.mt};

  @media (max-width: 992px) {
    margin-top: ${props => (props.mt ? props.mt : '1rem')};
  }
`;

const Item = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  padding: 0.5rem 0;
`;

const ItemLabel = styled.span`
  font-size: 1rem;
  font-weight: 500;
  color: var(--text-em);
  white-space: nowrap;
  margin-right: 1rem;

  ::after {
    content: ':';
    font-size: 1rem;
    color: var(--text-em);
  }
`;

const Content = styled.span`
  font-size: 1rem;
  color: var(--text-em);
  word-break: break-word;
`;

const SlideshowSection = styled.div`
  display: flex;
  flex-direction: column;
  background-color: #fff;
  box-shadow: var(--highlight);
  border-radius: 0.5rem;
  padding: 1rem;
`;

const Carousel = styled(AntCarousel)`
  .ant-carousel .slick-slide {
    text-align: center;
    line-height: 160px;
    overflow: hidden;
  }
  .ant-carousel .slick-slide {
    height: 500px;
  }
  .ant-carousel .slick-slide {
    align-self: center;
    height: 500px;
    h3 {
      color: #fff;
    }
  }
`;

const PhotoContainer = styled.div`
  height: 500px;
  display: flex !important;
  justify-content: center;
  background: #364d79;
  align-items: center;
  > img {
    max-width: 100%;
    max-height: 100%;
  }
`;

const Row = styled(AntRow)`
  margin-top: ${props => props.mt};
`;

const Col = styled(AntCol)``;

const AdditionSection = styled.div`
`;

const Card = styled.div`
  display: flex;
  flex-direction: column;
  height: 180px;
  background-color: var(--white);
  box-shadow: var(--highlight);
  border-radius: .5rem;
  padding: 1rem;
  margin-top: .5rem;
`;

const Title = styled.div`
  padding: .5rem;
  font-size: 1rem;
  color: var(--text-em);
  font-weight: 500;
  border-bottom: 1px solid var(--text-sub);
  white-space: nowrap; 
  overflow: hidden;
  text-overflow: ellipsis;
`;

const CardContent = styled.div`
  font-size: 14px;
  padding-top: 1rem;
`;

export {
  Wrapper,
  HotelInformationSection,
  Label,
  ContentContainer,
  Item,
  ItemLabel,
  Content,
  SlideshowSection,
  Carousel,
  PhotoContainer,
  Row,
  Col,
  AdditionSection,
  Card,
  Title,
  CardContent
};
