import React from 'react';
import GoogleMapReact from 'google-map-react';
import { Wrapper, Marker, Tooltip } from './map.styles';

const MapMarker = ({ name }) => {
  return (
    <Tooltip title={name} placement='top'>
      <Marker/>
    </Tooltip>
  );
};

const Map = ({lat, lng, name, ...rest}) => {
  return (
    <Wrapper>
       <GoogleMapReact
        center={{ lat: lat ? lat : 10, lng: lng ? lng : 10 }}
        defaultZoom={15}
      >
        <MapMarker
          lat={lat ? lat : 10}
          lng={lng ? lng : 10}
          name={name}
        />
      </GoogleMapReact>

    </Wrapper>
  )
};

export default Map;

