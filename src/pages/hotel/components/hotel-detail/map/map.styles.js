import styled from 'styled-components';
import { Tooltip as AntTooltip } from 'antd';
import img from './assets/marker.png';

const Wrapper = styled.div`
  width: 100%;
  height: 40vh;
`;

const Marker = styled.div`
  width: 20px;
  height: 40px;
  position: relative;
  font-size: 2rem;
  color: black;
  &::before {
    position: absolute;
    content: '';
    top: 0;
    bottom: 0;
    left: 0;
    right: 0;
    background-image: url(${img});
    background-repeat: no-repeat;
    background-size: 20px 40px;
  }
`;

const Tooltip = styled(AntTooltip)``;


export { Wrapper, Marker, Tooltip };
