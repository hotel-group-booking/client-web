import React from 'react';
import { compose } from 'ramda';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { getUser } from 'reducers/user';
import { Wrapper, Brand } from './navbar.styles';

const Navbar = ({history, name, user, ...rest}) => {

  const handleRedirect = e => {
    if (user) {
      history.push(`/${user.role}`);
    } else {
      history.push('/');
    }

  };

  return (
    <Wrapper {...rest}>
      <Brand onClick={handleRedirect}>
        Hotel Group Booking
      </Brand>
    </Wrapper>
  );
};

const mapStateToProps = state => ({
  user: getUser(state)
});

export default compose(
  connect(mapStateToProps),
  withRouter
)(Navbar);
