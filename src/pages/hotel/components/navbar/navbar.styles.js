import styled from 'styled-components';

const Wrapper = styled.div`
  background-color: var(--white);
  display: flex;
  align-items: center;
  font-size: 1rem;
  padding: .5rem 3rem;
  justify-content: space-between;
  box-shadow: var(--highlight);
  @media (max-width: 900px) {
    justify-content: center;
    flex-direction: column;
  }
`;

const Brand = styled.div`
  font-size: 2rem;
  color: var(--text-title);
  font-weight: 500;
  cursor: pointer;
  &:hover {
    color: var(--brand-color);
  }
`;

export { Wrapper, Brand };
