import React from 'react';
import { connect } from 'react-redux';
import { Switch, Route } from 'react-router-dom';
import { useUpdateLanguage } from 'hooks';
import { getSettings } from 'reducers/application';
import { Navbar, HotelDetail } from './components';
import { Wrapper, Content, ContentInner, Section } from './hotel.styles';

const HotelPage = ({ history, settings, ...rest }) => {
  useUpdateLanguage(settings.lng);

  return (
    <Wrapper {...rest}>
      <Navbar />
      <Content>
        <ContentInner>
          <Section xs={24} lg={24}>
            <Switch>
              <Route
                path='/hotel/:id'
                component={HotelDetail}
              />
            </Switch>
          </Section>
        </ContentInner>
      </Content>
    </Wrapper>
  );
};

const mapStateToProps = state => ({
  settings: getSettings(state)
});

export default connect(mapStateToProps)(HotelPage);
