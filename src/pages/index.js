export { default as HomePage } from './home/home.page';
export { default as RegistrationPage } from './registration/registration.page';
export { default as LoginPage } from './login/login.page';
export { default as MemberPage } from './member/member.page';
export { default as ProfilePage } from './profile/profile.page';
export { default as PartnerPage } from './partner/partner.page';
export { default as HotelPage } from './hotel/hotel.page';