import styled from 'styled-components';
import { Divider } from 'antd';

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  padding: .5rem 1rem;
`;

const LanguageItem = styled.div`
  font-size: 1rem;
  font-family: var(--font-baloo);
  color: ${props => props.active ? 'var(--teal)' : 'var(--text-em)'};
  cursor: pointer;
`;

const Line = styled(Divider).attrs(props => ({
  type: 'vertical'
}))`
  height: 3rem;
  background-color: var(--text-em);
`;

export {
  Wrapper,
  LanguageItem,
  Line
};

