import React from 'react';
import { connect } from 'react-redux';
import { getSettings, doChangeLanguage } from 'reducers/application';
import { Wrapper, LanguageItem, Line } from './language-selector.styles';

const LanguageSelector = ({settings, changeLanguage, ...rest}) => {

  const handleChangeLanguage = lng => e => {
    changeLanguage(lng);
  };

  return (
    <Wrapper {...rest}>
      <LanguageItem 
        active={settings.lng === 'en'}
        onClick={handleChangeLanguage('en')}
      >
        EN
      </LanguageItem>
      <LanguageItem 
        active={settings.lng === 'vi'}
        onClick={handleChangeLanguage('vi')}
      >
        VI
      </LanguageItem>
      <Line/>
    </Wrapper>
  );
};

const mapStateToProps = state => ({
  settings: getSettings(state)
});

const mapDispatchToProps = dispatch => ({
  changeLanguage: lng => dispatch(doChangeLanguage(lng))
});

export default connect(mapStateToProps, mapDispatchToProps)(LanguageSelector);
