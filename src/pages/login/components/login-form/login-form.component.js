import React from 'react';
import { useTranslation } from 'react-i18next';
import { compose } from 'ramda';
import { withRouter } from 'react-router-dom';
import { validator } from 'config/validator';
import { Icon } from 'antd';
import {
  Wrapper,
  Form,
  FormTitle,
  Input,
  FormItem,
  Line,
  CreateAccountButton,
  ForgetPasswordButton,
  SubmitButton,
  FormFooter,
  FormEffect
} from './login-form.styles';

const LoginForm = ({
  history,
  form: { getFieldDecorator, validateFields },
  onSubmit,
  ...rest
}) => {
  const [t] = useTranslation(['login-page', 'validator']);

  const handleSubmit = e => {
    e.preventDefault();
    validateFields((err, values) => {
      if (!err) {
        onSubmit(values);
      }
    });
  };

  const handleRedirect = (url, hash) => e => {
    history.push({
      pathname: url,
      hash
    });
  };

  const rules = validator(t);
  const username = () =>
    getFieldDecorator('username', {
      rules: [rules.required]
    })(
      <Input
        prefix={<Icon type='user' />}
        placeholder={t('login-page:username placeholder')}
      />
    );

  const password = () =>
    getFieldDecorator('password', {
      rules: [rules.required]
    })(
      <Input
        prefix={<Icon type='lock' />}
        type='password'
        placeholder={t('login-page:password placeholder')}
      />
    );

  return (
    <Wrapper>
      <Form {...rest} onSubmit={handleSubmit} colon={false}>
        <FormEffect/>
        <FormTitle>{t('login-page:login form title')}</FormTitle>
        <FormItem label={t('login-page:username')} required={false}>
          {username()}
        </FormItem>
        <FormItem label={t('login-page:password')} required={false}>
          {password()}
        </FormItem>

        <ForgetPasswordButton onClick={handleRedirect('/')}>
          <span>{t('login-page:forget password')}</span>
        </ForgetPasswordButton>

        <Line>{t('login-page:line text')}</Line>

        <CreateAccountButton onClick={handleRedirect('/', '#signup')}>
          <span>{t('login-page:create new account')}</span>
        </CreateAccountButton>

        <FormFooter>
          <SubmitButton
            size={{ height: '40px', width: '160px' }}
            type='gradient'
            htmlType='submit'
          >
            {t('login-page:login')}
          </SubmitButton>
        </FormFooter>
      </Form>
    </Wrapper>
  );
};

export default compose(
  withRouter,
  Form.create()
)(LoginForm);
