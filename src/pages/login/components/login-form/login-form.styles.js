import styled, { keyframes } from 'styled-components';
import { Form as AntForm, Input as AntInput, Divider } from 'antd';
import { Button } from 'components';

const shilft = keyframes`
  0% {
    transform: translateY(75px);
  }
  50% {
    opacity: .8;
  }
  100% {
    transform: translateY(0);
    opacity: 1;
  }
`;

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  width: 100%;
  justify-content: center;
  align-items: center;
  animation: ${shilft} .5s;
`;

const Form = styled(AntForm)`
  width: 100%;
  background-color: var(--white);
  border-radius: .5rem;
  box-shadow: rgba(0, 0, 0, 0.1) 0px 9px 35px 10px;
  max-width: 470px;
  padding: 2.5rem 1rem;
  position: relative;

  @media (min-width: 500px) {
    padding: 2.5rem 4rem;
  }
`;

const FormEffect = styled.div`
  position: absolute;
  bottom: 0;
  left: 0;
  right: 0;
  ::before {
    content: '';
    height: 300px;
    bottom: -10px;
    left: 25px;
    right: 25px;
    border-radius: 0.5rem;
    background-color: #aeeaff;
    position: absolute;
    z-index: -1;
  };
  ::after {
    content: '';
    height: 300px;
    bottom: -20px;
    left: 40px;
    right: 40px;
    border-radius: 0.5rem;
    background-color: #98e1fa;
    opacity: 0.4;
    position: absolute;
    z-index: -2;
  }
`;

const FormTitle = styled.div`
  font-size: 2rem;
  font-family: var(--font-baloo);
  color: var(--text-title);
  text-align: center;
  margin-bottom: 1rem;
`;

const FormItem = styled(AntForm.Item)`
  label {
    font-size: 1.25rem;
    font-family: var(--font-baloo);
    color: var(--text-em);
  }
`;

const Input = styled(AntInput)`
  width: 100%;
  height: 3rem;
  color: var(--text-em);

  svg {
    width: 20px;
    height: 20px;
  }

  .ant-input {
    padding-left: 40px !important;
  }
`;

const Line = styled(Divider)`
  margin: 8px 0 !important;
  padding: 0 1.5rem;
  span {
    padding: 0 12px;
    color: var(--text-sub);
    text-transform: uppercase;
  }
`;

const ForgetPasswordButton = styled.div`
  display: flex;
  justify-content: flex-end;

  span {
    cursor: pointer;
    font-size: .9rem;
    font-family: var(--font-baloo);
    :hover {
      color: var(--text-em);
    }
  }
`;

const CreateAccountButton = styled.div`
 
  display: flex;
  justify-content: center;

  span {
    cursor: pointer;
    font-size: 1rem;
    font-family: var(--font-baloo);
    :hover {
      color: var(--text-em);
    }
  }
`;

const FormFooter = styled.div`
  display: flex;
  justify-content: center;
`;

const SubmitButton = styled(Button)`
  margin-top: 1rem;
  border-radius: 20px;
  outline: none;
`;

export {
  Wrapper,
  Form,
  FormTitle,
  FormItem,
  Input,
  Line,
  CreateAccountButton,
  ForgetPasswordButton,
  FormFooter,
  SubmitButton,
  FormEffect
};
