import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { doLogin, getIsLogging, getResult, doClearResult } from 'reducers/auth';
import { getUser } from 'reducers/user';
import { getSavedRequest } from 'reducers/request';
import { LanguageSelector, LoginForm } from './components';
import { useTranslation } from 'react-i18next';
import { toast } from 'react-toastify';
import {
  Wrapper,
  LanguageSelectorSection,
  Content,
  Header,
  Brand,
  LoginWrapper,
  Decorator,
  Description
} from './login.styles';

const LoginPage = ({
  history,
  user,
  isLogging,
  result,
  login,
  clearResult,
  savedRequest,
  ...rest
}) => {
  const [t] = useTranslation('login-page');

  useEffect(() => {
    clearResult();
  }, []);

  useEffect(() => {
    if (user) {
      history.push(`/${user.role}`);
    }
  }, [user]);

  useEffect(() => {
    if (result) {
      if (result.type === 'success') {
        toast(t('login-page:login result.success'), {
          type: toast.TYPE.SUCCESS
        });
      }

      if (result.type === 'error') {
        toast(t(`login-page:login result.${result.message}`), {
          type: toast.TYPE.ERROR
        });
      }
    }
  }, [result]);

  const handleSubmit = ({ username, password }) => {
    login(username, password, savedRequest);
  };

  const handleRedirect = url => e => {
    history.push(url);
  };

  return (
    <Wrapper {...rest}>
      <LanguageSelectorSection>
        <LanguageSelector />
      </LanguageSelectorSection>
      <Content>
        <Header xs={24} lg={12}>
          <Brand onClick={handleRedirect('/')}>Hotel Group Booking</Brand>
          <Description>{t('login-page:header description')}</Description>
        </Header>
        <LoginWrapper xs={24} lg={12}>
          <LoginForm onSubmit={handleSubmit} />
        </LoginWrapper>
      </Content>
      <Decorator />
    </Wrapper>
  );
};

const mapStateToProps = state => ({
  isLogging: getIsLogging(state),
  user: getUser(state),
  result: getResult(state),
  savedRequest: getSavedRequest(state)
});

const mapDispatchToProps = dispatch => ({
  login: (username, password, savedRequest) =>
    dispatch(doLogin(username, password, savedRequest)),
  clearResult: () => dispatch(doClearResult())
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(LoginPage);
