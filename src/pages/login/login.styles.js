import styled from 'styled-components';
import bgdot from '../../assets/dot-pattern.png';
import { Row, Col as AntCol } from 'antd';

const Wrapper = styled.div`
  background-image: url(${bgdot});
  background-repeat: repeat;
  min-height: 100vh;
  position: relative;
  padding-top: 7vh;

  @media (min-width: 992px) {
    padding-top: 0;
  }
`;

const LanguageSelectorSection = styled.div`
  position: absolute;
  top: 0;
  right: 0;
  z-index: 10;
`;

const Content = styled(Row).attrs(props => ({
  gutter: 0
}))`
  z-index: 2;
  @media (min-width: 992px) {
    padding-top: 200px;
  }
`;

const Header = styled(AntCol)`
  display: flex;
  flex-direction: column;
  padding: 2rem 1rem;
  justify-content: center;
  align-items: center;
`;

const Brand = styled.div`
  font-size: 1.75rem;
  font-family: var(--font-baloo);
  color: var(--text-title);
  text-align: center;
  cursor: pointer;

  :hover {
    color: var(--brand-color);
  }

  @media (min-width: 992px) {
    font-size: 3rem;
  }
`;

const Description = styled.div`
  font-size: 2rem;
  color: var(--text-em);
  font-family: var(--font-baloo);
  text-align: center;
  max-width: 380px;
  margin-top: 2rem;
  display: none;
  
  @media (min-width: 992px) {
    display: block;
  }
`;

const LoginWrapper = styled(AntCol)`
  display: flex;
  justify-content: center;
  justify-items: center;
  padding: 2rem 0.5rem;
`;

const Decorator = styled.div`
  width: 1500px;
  height: 1500px;
  position: fixed;
  bottom: -800px;
  right: -650px;
  background-image: linear-gradient(
    120deg,
    var(--brand-color) 20%,
    #66a6ff 100%
  );
  border-radius: 50%;
`;

export {
  Wrapper,
  LanguageSelectorSection,
  Content,
  Header,
  Brand,
  LoginWrapper,
  Decorator,
  Description
};
