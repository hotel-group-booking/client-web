import React, { useState, useEffect } from 'react';
import Sorter from './sorter/sorter.component';
import EmptyList from './empty-list/empty-list.component';
import RequestItem from './request-item/request-item.component';
import RequestDrawer from './request-drawer/request-drawer.component';
import { isEmpty, sort } from 'ramda';
import { connect } from 'react-redux';
import {
  getList,
  getIsFetchingRequests,
  doFetchRequests,
  doClearResult,
  doUpdate,
  getIsUpdating,
  doSelect,
  getSelected
} from 'reducers/request';
import { getUser } from 'reducers/user';
import { LoadingList } from 'components';
import { Wrapper, LoadingSection } from './inactive-request-list.styles';

const sorter = {
  lastest: (a, b) => {
    const timeA = new Date(a.created).getTime();
    const timeB = new Date(b.created).getTime();
    return timeA - timeB;
  },
  newest: (a, b) => {
    const timeA = new Date(a.created).getTime();
    const timeB = new Date(b.created).getTime();
    return timeB - timeA;
  },
  offers: (a, b) => {
    return b.offerCount - a.offerCount;
  },
  location: (a, b) => {
    return a.destination.name.localeCompare(b.destination.name);
  }
};

const RequestList = ({
  clearResult,
  fetchRequests,
  isFetchingRequest,
  list,
  user,
  select,
  selected,
  update,
  ...rest
}) => {
  const [requests, setRequests] = useState(
    list.values.filter(req => !req.active)
  );
  const [visible, setVisible] = useState(false);
  const [key, setKey] = useState('newest');

  useEffect(() => {
    return () => {
      clearResult();
    };
  }, []);

  useEffect(() => {
    if (user) {
      fetchRequests(0, user._id);
    }
  }, [user]);

  useEffect(() => {
    setRequests(sort(sorter[key])(list.values.filter(req => !req.active)));
  }, [key, list]);

  const handleSorterChange = key => {
    setKey(key);
  };

  const handleRequestSelect = request => {
    select(request);
    setVisible(true);
  };

  const handleUpdate = (id, data) => {
    update(id, data, user._id);
  };

  return (
    <Wrapper {...rest}>
      <Sorter onChange={handleSorterChange} sortKey={key} />
      {isEmpty(requests) && !isFetchingRequest ? (
        <EmptyList />
      ) : (
        requests.map(request => (
          <RequestItem
            key={request._id}
            onSelect={handleRequestSelect}
            request={request}
          />
        ))
      )}
      <LoadingSection>
        <LoadingList loading={isFetchingRequest} />
      </LoadingSection>

      <RequestDrawer
        visible={visible}
        onClose={() => setVisible(false)}
        request={selected}
        update={handleUpdate}
      />
    </Wrapper>
  );
};

const mapStateToProps = state => ({
  user: getUser(state),
  list: getList(state),
  isFetchingRequest: getIsFetchingRequests(state),
  isUpdating: getIsUpdating(state),
  selected: getSelected(state)
});

const mapDispatchToProps = dispatch => ({
  fetchRequests: (page, id) => dispatch(doFetchRequests(page, id)),
  clearResult: () => dispatch(doClearResult()),
  update: (id, data, userId) => dispatch(doUpdate(id, data, userId)),
  select: request => dispatch(doSelect(request))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(RequestList);
