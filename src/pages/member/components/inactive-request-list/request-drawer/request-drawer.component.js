import React, { useState } from 'react';
import { Bookmark, Edit } from 'react-feather';
import { useTranslation } from 'react-i18next';
import moment from 'moment';
import { ROOM_TYPES } from 'config/constants';
import OfferList from './offer-list/offer-list.component';
import {
  Wrapper,
  DrawerContent,
  Header,
  Tag,
  Input,
  HeaderEditToggle,
  HeaderStatus,
  Body,
  Item,
  Title,
  Text
} from './request-drawer.styles';

const RequestDrawer = ({ onClose, visible, request, update }) => {
  const [isHeaderEdit, setIsHeaderEdit] = useState(false);
  const [description, setDescription] = useState('');

  const [t] = useTranslation('member-page');

  const handleDescriptionChange = e => {
    setDescription(e.target.value);
  };

  const handleHeaderEditToggle = e => {
    if (description) {
      update(request._id, {
        description
      });
      setDescription('');
    }
    setIsHeaderEdit(!isHeaderEdit);
  };

  const handleToggleRequest = e => {
    update(request._id, {
      active: !request.active
    });
  };

  const budgetFormat = (budgetFrom, budgetTo) => {
    const from = Number(budgetFrom * 1000).toLocaleString('vi-VN', {
      style: 'currency',
      currency: 'VND'
    });
    const to = Number(budgetTo * 1000).toLocaleString('vi-VN', {
      style: 'currency',
      currency: 'VND'
    });
    return `${from} - ${to}`;
  };

  return (
    <Wrapper
      onClose={onClose}
      visible={visible}
      title={t('member-page:request information')}
    >
      {request && (
        <DrawerContent>
          <Header>
            <Tag large>{request.destination.name}</Tag>
            {isHeaderEdit ? (
              <Input
                value={description}
                onChange={handleDescriptionChange}
                placeholder={request.description}
              />
            ) : (
              <div>{request.description}</div>
            )}
            <HeaderEditToggle>
              <Bookmark />
              <Edit onClick={handleHeaderEditToggle} />
            </HeaderEditToggle>
            <HeaderStatus onClick={handleToggleRequest}>
              {request.active ? (
                <Tag type='primary'>{t('member-page:active')}</Tag>
              ) : (
                <Tag type='secondary'>{t('member-page:inactive')}</Tag>
              )}
            </HeaderStatus>
          </Header>
          <Body>
            <Item>
              <Title>{t('member-page:item.checkin')}</Title>
              <Text>{moment(request.checkin).format('D/M/YYYY')}</Text>
            </Item>
            <Item>
              <Title>{t('member-page:item.checkout')}</Title>
              <Text>{moment(request.checkout).format('D/M/YYYY')}</Text>
            </Item>
            <Item>
              <Title>{t('member-page:item.nightly budget')}</Title>
              <Text>{budgetFormat(request.budgetFrom, request.budgetTo)}</Text>
            </Item>
            <Item>
              <Title>{t('member-page:item.rooms')}</Title>
              <Text>{request.rooms}</Text>
            </Item>
            <Item>
              <Title>{t('member-page:item.star rating')}</Title>
              <Text>{request.starRating}</Text>
            </Item>
            <Item>
              <Title>{t('member-page:item.room type')}</Title>
              <Text>
                {
                  ROOM_TYPES.find(
                    type => type.value === (request.roomType || 1)
                  ).name
                }
              </Text>
            </Item>
            <Item>
              <Title>{t('member-page:item.group type')}</Title>
              <Text>{request.groupType}</Text>
            </Item>
          </Body>
          <OfferList request={request}/>
        </DrawerContent>
      )}
    </Wrapper>
  );
};

export default RequestDrawer;
