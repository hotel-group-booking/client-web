import React, { useState } from 'react';
import {
  Wrapper,
  Dropdown,
  DropdownItem,
  CheckIcon,
  Chevron
} from './sorter.styles';
import { useTranslation } from 'react-i18next';

const Sorter = ({ className, onChange, sortKey = 'lastest' }) => {
  const [visible, setVisible] = useState(false);
  const [t] = useTranslation('member-page');

  const handleClick = e => {
    setVisible(!visible);
  };

  const handleSorterChange = key => e => {
    onChange(key);
  };

  return (
    <Wrapper className={className} onClick={handleClick}>
      <span>{t(`member-page:sorter.${sortKey}`)}</span> <Chevron />
      <Dropdown visible={visible}>
        <DropdownItem
          onClick={handleSorterChange('lastest')}
        >
          {sortKey === 'lastest' && <CheckIcon />}
          <span>{t('member-page:sorter.lastest')}</span>
        </DropdownItem>
        <DropdownItem
          onClick={handleSorterChange('newest')}
        >
          {sortKey === 'newest' && <CheckIcon />}
          <span>{t('member-page:sorter.newest')}</span>
        </DropdownItem>
        <DropdownItem
          onClick={handleSorterChange('offers')}
        >
          {sortKey === 'offers' && <CheckIcon />}
          <span>{t('member-page:sorter.offers')}</span>
        </DropdownItem>
        <DropdownItem
          onClick={handleSorterChange('location')}
        >
          {sortKey === 'location' && <CheckIcon />}
          <span>{t('member-page:sorter.location')}</span>
        </DropdownItem>
      </Dropdown>
    </Wrapper>
  );
};

export default Sorter;
