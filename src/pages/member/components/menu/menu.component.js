import React, { useEffect } from 'react';
import { compose } from 'ramda';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { getUser } from 'reducers/user';
import { getSize, doFetchCount, getCount } from 'reducers/request';
import {
  Wrapper,
  Header,
  Avatar,
  UserSection,
  Username,
  Body,
  FeatureButton,
  ItemGroup,
  Item,
  ColorBlock,
  Label,
  Badge,
  StandaloneHeader,
  HeaderLabel,
  UserGroup,
  Name
} from './menu.styles';
import { useTranslation } from 'react-i18next';

const Menu = ({
  history,
  user,
  standalone = false,
  onItemClick,
  size,
  fetchCount,
  count,
  ...rest
}) => {
  const [t] = useTranslation('application');
  useEffect(() => {
    if (user) {
      fetchCount(user._id);
    }
  }, [user]);

  const handleRedirect = url => e => {
    history.push(url);
    if (onItemClick) {
      onItemClick();
    }
  };

  return (
    <Wrapper {...rest}>
      {standalone ? (
        <StandaloneHeader>
          <HeaderLabel>My Account</HeaderLabel>
          <UserGroup>
            {user.photo ? (
              <Avatar src={user.photo.link} size={36} />
            ) : (
              <Avatar icon={user} size={36} />
            )}
            <Name>{user.displayName}</Name>
          </UserGroup>
        </StandaloneHeader>
      ) : (
        <Header>
          <UserSection>
            {user.photo ? (
              <Avatar src={user.photo.link} size={96} />
            ) : (
              <Avatar icon={user} size={96} />
            )}
            <Username>{user.displayName}</Username>
          </UserSection>
        </Header>
      )}
      <Body>
        <FeatureButton onClick={handleRedirect('/member/create-request')}>
          {t('application:menu.new request')}
        </FeatureButton>
        {/* <FeatureButton onClick={handleRedirect('/member/find-deal')}>
          {t('application:menu.find deal')}
        </FeatureButton> */}
        <ItemGroup>
          <Item onClick={handleRedirect('/member/requests')}>
            <ColorBlock color='#005b6d' />
            <Label>{t('application:menu.all request')}</Label>
            <Badge count={count.activeSize} bg='#005b6d' />
          </Item>
          {/* <Item onClick={handleRedirect('/member/new-requests')}>
            <ColorBlock color='#1c3f62' />
            <Label>{t('application:menu.recently created request')}</Label>
            <Badge count={0} bg='#1c3f62' />
          </Item> */}
          <Item onClick={handleRedirect('/member/inactive-request')}>
            <ColorBlock color='#000f28' />
            <Label>{t('application:menu.inactive requests')}</Label>
            <Badge count={count.inactiveSize} bg='#000f28' />
          </Item>
          {/* <Item>
            <ColorBlock color='#c65a1b' />
            <Label>{t('application:menu.marked requests')}</Label>
            <Badge count={0} bg='#c65a1b' />
          </Item>
          <Item>
            <ColorBlock color='#7f1414' />
            <Label>{t('application:menu.following offers')}</Label>
            <Badge count={0} bg='#7f1414' />
          </Item> */}
          <Item onClick={handleRedirect('/member/timeout-request')}>
            <ColorBlock color='#7f1414' />
            <Label>{t('application:menu.timeout requests')}</Label>
            <Badge count={count.timeoutSize} bg='#7f1414' />
          </Item>
        </ItemGroup>
      </Body>
    </Wrapper>
  );
};

const mapStatToProps = state => ({
  user: getUser(state),
  size: getSize(state),
  count: getCount(state)
});

const mapDispatchToProps = dispatch => ({
  fetchCount: id => dispatch(doFetchCount(id))
});

export default compose(
  connect(mapStatToProps, mapDispatchToProps),
  withRouter
)(Menu);
