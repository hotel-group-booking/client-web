import styled from 'styled-components';
import { Avatar as AntAvatar, Badge as AntBadge } from 'antd';

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
`;

const Header = styled.div`
  display: flex;
  flex-direction: column;
  padding: 2rem 1rem 1rem 1rem;
`;

const UserSection = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  padding-bottom: 1rem;
  border-bottom: 1px solid var(--text-title);
`;

const Avatar = styled(AntAvatar)``;

const Username = styled.div`
  font-size: 1.5rem;
  font-weight: 500;
  color: var(--text-em);
  margin-top: 0.5rem;
`;

const Body = styled.div`
  display: flex;
  flex-direction: column;
  padding: 0 1rem;
`;

const FeatureButton = styled.div`
  background-color: var(--text-em);
  display: flex;
  justify-content: center;
  align-items: center;
  color: var(--white);
  font-size: 1rem;
  padding: 0.5rem;
  border-radius: 0.25rem;
  margin-top: 1rem;
  cursor: pointer;
`;

const ItemGroup = styled.div`
  display: flex;
  flex-direction: column;
  margin-top: 2rem;
`;

const Item = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: flex-start;
  align-items: center;
`;

const ColorBlock = styled.div`
  width: 16px;
  height: 16px;
  background-color: ${props => props.color};
  border-radius: 0.2rem;
`;

const Label = styled.div`
  font-size: 0.9rem;
  color: #669199;
  margin-left: 0.5rem;
  padding: 0.3rem;
  cursor: pointer;

  :hover {
    color: var(--text-em);
  }
`;

const Badge = styled(AntBadge).attrs(props => ({
  showZero: true
}))`
  margin-left: auto;
  min-width: 40px;
  .ant-badge-count {
    background-color: ${props => props.bg};
  }
`;

const StandaloneHeader = styled.div`
  display: flex;
  flex-direction: column;
  padding: 0 1rem;
`;

const HeaderLabel = styled.div`
  display: flex;
  justify-content: flex-start;
  align-items: center;
  padding: 0.5rem 0;
  font-size: 1rem;
  color: var(--text-title);
  font-weight: 500;
  border-bottom: 1px solid var(--text-title);
`;

const UserGroup = styled.div`
  display: flex;
  flex-direction: row;
  padding: .5rem;
  align-items: center;
`;

const Name = styled.div`
  font-size: 1rem;
  margin-left: 1rem;
  color: var(--text-title);
  font-weight: 500;
`;

export {
  Wrapper,
  Header,
  UserSection,
  AntAvatar,
  Avatar,
  Username,
  Body,
  FeatureButton,
  ItemGroup,
  Item,
  ColorBlock,
  Label,
  Badge,
  StandaloneHeader,
  HeaderLabel,
  UserGroup,
  Name
};
