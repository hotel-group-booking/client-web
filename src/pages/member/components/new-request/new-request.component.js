import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { getUser } from 'reducers/user';
import {
  doCreateRequest,
  doClearResult,
  getIsCreatingRequest,
  getResult
} from 'reducers/request';
import { RequestForm } from 'components';
import { toast } from 'react-toastify';
import { Wrapper } from './new-request.styles';
import { useTranslation } from 'react-i18next';

const NewRequest = ({
  history,
  user,
  result,
  isCreatingRequest,
  createRequest,
  clearResult,
  ...rest
}) => {

  const [t] = useTranslation('member-page');

  useEffect(() => {
    return () => {
      clearResult();
    }
  }, []);

  

  useEffect(() => {
    if (result && result.type === 'success') {
      toast.success(t('member-page:results.request created'), {
        autoClose: 1000,
        hideProgressBar: true
      });
      setTimeout(() => {
        history.push(`/member/requests`);
      });
    } 
    if (result && result.type === 'error') {
      toast.error(t('member-page:results.request error'), {
        autoClose: 1000,
        hideProgressBar: true
      });
    }
  }, [result]);

  const handleRequestSubmit = request => {
    createRequest(request, user);
  };

  return (
    <Wrapper {...rest}>
      <RequestForm onSubmit={handleRequestSubmit} />
    </Wrapper>
  );
};

const mapStateToProps = state => ({
  user: getUser(state),
  result: getResult(state),
  isCreatingRequest: getIsCreatingRequest(state)
});

const mapDispatchToProps = dispatch => ({
  createRequest: (request, user) =>
    dispatch(doCreateRequest({ ...request, member: user._id})),
  clearResult: () => dispatch(doClearResult())
});

export default connect(mapStateToProps, mapDispatchToProps)(NewRequest);
