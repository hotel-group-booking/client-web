import styled from 'styled-components';

const Wrapper = styled.div`
  background-color: var(--white);
  border-radius: .5rem;
  margin: .1rem;
  padding: 1rem;
  box-shadow: var(--highlight);

  @media (min-width: 992px) {
    padding: 1rem 2rem;
  }
`;

export { Wrapper };
