import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { getUser } from 'reducers/user';
import {
  doFetchDetail,
  getDetail,
  getIsFetchingDetail,
  getIsAcceptingOffer,
  doAcceptOffer,
  doClearResult
} from 'reducers/offer';
import { useTranslation } from 'react-i18next';
import moment from 'moment';
import { currencyFormat, getDuration } from 'lib';
import { Messager } from 'components';
import { Skeleton } from 'antd';
import {
  Wrapper,
  OfferContainer,
  HeaderSection,
  HeaderLabel,
  ButtonsContainer,
  Button,
  Body,
  Divider,
  Item,
  Label,
  Content,
  Row,
  Col,
  UserContainer,
  DiscussionContainer
} from './offer-detail.styles';

const OfferDetail = ({
  history,
  user,
  offer,
  isFetchingDetail,
  isAcceptingOffer,
  fetchDetail,
  clearResult,
  match,
  acceptOffer,
  ...rest
}) => {
  const [t] = useTranslation('member-page');

  useEffect(() => {
    fetchDetail(match.params.id);

    return () => {
      clearResult();
    };
  }, []);

  const handleOfferAccept = e => {
    acceptOffer(offer._id, !offer.accepted);
  };

  const handleRedirect = url => e => {
    history.push(url);
  };

  return (
    <Wrapper {...rest}>
      <OfferContainer>
        <HeaderSection>
          <HeaderLabel>
            {t('member-page:offer-detail.labels.offer information')}
          </HeaderLabel>

          {offer && (
            <ButtonsContainer>
              <Button
                type={offer.accepted ? 'danger' : 'primary'}
                ghost
                onClick={handleOfferAccept}
                loading={isAcceptingOffer}
              >
                {offer.accepted
                  ? t('member-page:offer-detail.buttons.cancel')
                  : t('member-page:offer-detail.buttons.accept')}
              </Button>
            </ButtonsContainer>
          )}
        </HeaderSection>

        <Skeleton loading={isFetchingDetail} paragraph={{ rows: 6 }}>
          {offer && (
            <Body>
              <Item>
                <Label>
                  {t('member-page:offer-detail.labels.nightly budget')}
                </Label>
                <Content>{currencyFormat(offer.budget)}</Content>
              </Item>

              <Item>
                <Label>{t('member-page:offer-detail.labels.checkin')}</Label>
                <Content>{moment(offer.checkin).format('D/M/YYYY')}</Content>
              </Item>
              <Item>
                <Label>{t('member-page:offer-detail.labels.checkout')}</Label>
                <Content>{moment(offer.checkout).format('D/M/YYYY')}</Content>
              </Item>

              <Item>
                <Label>{t('member-page:offer-detail.labels.rooms')}</Label>
                <Content>
                  {`${offer.rooms} ${
                    offer.rooms > 1
                      ? t('member-page:count.rooms')
                      : t('member-page:count.room')
                  }`}
                </Content>
              </Item>

              <Item>
                <Label>{t('member-page:offer-detail.labels.created at')}</Label>
                <Content>
                  {moment(offer.created).format('D/M/YYYY - LTS')}
                </Content>
              </Item>

              <Divider dashed />

              <Item>
                <Label>{t('member-page:offer-detail.labels.total')}</Label>
                <Content>
                  {currencyFormat(
                    offer.budget *
                      offer.rooms *
                      getDuration(offer.checkin, offer.checkout)
                  )}
                </Content>
              </Item>
            </Body>
          )}
        </Skeleton>
      </OfferContainer>

      <Row gutter={24}>
        <Col xs={24}>
          <UserContainer>
            <HeaderSection>
              <HeaderLabel>
                {t('member-page:offer-detail.labels.hotel information')}
              </HeaderLabel>

              <ButtonsContainer>
                <Button
                  type='primary'
                  ghost
                  onClick={handleRedirect(
                    `/hotel/${offer && offer.partner.hotel._id}`
                  )}
                >
                  {t('member-page:offer-detail.buttons.hotel redirect')}
                </Button>
              </ButtonsContainer>
            </HeaderSection>

            <Skeleton loading={isFetchingDetail}>
              {offer && (
                <Body>
                  <Item>
                    <Label>
                      {t('member-page:offer-detail.labels.hotel name')}
                    </Label>
                    <Content>{offer.partner.hotel.name}</Content>
                  </Item>
                  <Item>
                    <Label>
                      {t('member-page:offer-detail.labels.address')}
                    </Label>
                    <Content>{offer.partner.hotel.address.name}</Content>
                  </Item>
                  <Item>
                    <Label>
                      {t('member-page:offer-detail.labels.star rating')}
                    </Label>
                    <Content>{`${offer.partner.hotel.starRating} ${
                      offer.partner.hotel.starRating > 1
                        ? t('member-page:count.stars')
                        : t('member-page:count.star')
                    }`}</Content>
                  </Item>
                </Body>
              )}
            </Skeleton>
          </UserContainer>
        </Col>
      </Row>

      {offer && (
        <DiscussionContainer>
          <HeaderSection>
            <HeaderLabel>
              {t('member-page:offer-detail.labels.discussion')}
            </HeaderLabel>
          </HeaderSection>
          <Body nopadding>
            <Messager room={offer._id} user={user} />
          </Body>
        </DiscussionContainer>
      )}
    </Wrapper>
  );
};

const mapStateToProps = state => ({
  user: getUser(state),
  offer: getDetail(state),
  isFetchingDetail: getIsFetchingDetail(state),
  isAcceptingOffer: getIsAcceptingOffer(state)
});

const mapDispatchToProps = dispatch => ({
  fetchDetail: id => dispatch(doFetchDetail(id)),
  clearResult: () => dispatch(doClearResult()),
  acceptOffer: (id, accepted) => dispatch(doAcceptOffer(id, accepted))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(OfferDetail);
