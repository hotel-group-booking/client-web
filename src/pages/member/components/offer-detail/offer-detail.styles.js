import styled from 'styled-components';
import {
  Button as AntButton,
  Divider as AntDivider,
  Row as AntRow,
  Col as AntCol
} from 'antd';

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  padding: 0.75rem;
`;

const OfferContainer = styled.div`
  box-shadow: var(--highlight);
  background-color: var(--white);
  padding: 0.5rem;
  border-radius: 0.5rem;
  margin-bottom: 1rem;
`;

const HeaderSection = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  padding: 0.5rem 0.25rem;
  align-items: center;
  border-bottom: 1px solid var(--text-em);
`;

const HeaderLabel = styled.div`
  color: var(--text-title);
  font-size: 1.25rem;
  font-weight: 500;
  line-height: 32px;
`;

const ButtonsContainer = styled.div`
  display: flex;
  flex-direction: row;
`;

const Button = styled(AntButton)`
  margin-right: 0.25rem;
`;

const Body = styled.div`
  display: flex;
  flex-direction: column;
  margin-top: 0.5rem;
  padding: ${props => props.nopadding ? '0' : '0 0.5rem'};
`;

const Item = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  padding: 0.5rem 0;
`;

const Label = styled.span`
  font-size: 1rem;
  font-weight: 500;
  color: var(--text-em);
  white-space: nowrap;
  margin-right: 1rem;

  ::after {
    content: ':';
    font-size: 1rem;
    color: var(--text-em);
  }
`;

const Content = styled.span`
  font-size: 1rem;
  color: var(--text-em);
  word-break: break-word;
`;

const Divider = styled(AntDivider)`
  background-color: var(--text-title);
  margin: 1rem 0 0.5rem 0;
`;

const HotelContainer = styled.div`
  box-shadow: var(--highlight);
  background-color: var(--white);
  padding: 0.5rem;
  border-radius: 0.5rem;
`;

const UserContainer = styled.div`
  box-shadow: var(--highlight);
  background-color: var(--white);
  padding: 0.5rem;
  border-radius: 0.5rem;
`;

const Row = styled(AntRow)`
  @media (min-width: 992px) {
    margin-bottom: 1rem;
  }
`;

const Col = styled(AntCol)`
  margin-bottom: 1rem;
  @media (min-width: 992px) {
    margin-bottom: 0;
  }
`;

const DiscussionContainer = styled.div`
  background-color: var(--white);
  border-radius: 0.5rem;
  padding: 1rem;
  box-shadow: var(--highlight);
  display: flex;
  flex-direction: column;
`;

export {
  Wrapper,
  OfferContainer,
  HeaderSection,
  HeaderLabel,
  ButtonsContainer,
  Button,
  Body,
  Item,
  Label,
  Content,
  Divider,
  HotelContainer,
  UserContainer,
  Row,
  Col,
  DiscussionContainer
};
