import React, { useEffect } from 'react';
import { compose, isEmpty } from 'ramda';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { useTranslation } from 'react-i18next';
import { LoadingList } from 'components';
import { doFetchOffers, getIsFetchingOffers, getOffers } from 'reducers/offer';
import { currencyFormat } from 'lib';
import {
  Wrapper,
  LoadingSection,
  LoadingText,
  Item,
  Description,
  CreatorSection,
  HotelLink,
  TagsSection,
  Tag,
  EmptySection,
  FrownIcon
} from './offer-list.styles';

const OfferList = ({
  history,
  request,
  offers,
  fetchOffers,
  isFetchingOffers,
  ...rest
}) => {
  const [t] = useTranslation('member-page');

  useEffect(() => {
    fetchOffers(0, request._id);
  }, [request]);

  const handleRedirect = url => e => {
    history.push(url);
  };

  return (
    <Wrapper {...rest}>
      {isEmpty(offers.values) && !isFetchingOffers ? 
        <EmptySection>
          <FrownIcon size={48}/>
          <span>{t('member-page:item.offer empty')}</span>
        </EmptySection>
        : offers.values.map(offer => (
        <Item key={offer._id}>
          <Description onClick={handleRedirect(`/member/offer/${offer._id}`)}>
            {`${offer.rooms} ${t('member-page:count.rooms')} - ${currencyFormat(offer.budget)}`}
          </Description>
          <CreatorSection>
            <div>{t('member-page:item.from')}</div>
            <HotelLink
              onClick={handleRedirect(`/hotel/${offer.partner.hotel._id}`)}
            >
              {offer.partner.hotel.name}
            </HotelLink>
          </CreatorSection>
          <TagsSection>
            <Tag type='primary'>
              {Number(offer.budget * 1000).toLocaleString('vi-VN', {
                style: 'currency',
                currency: 'VND'
              })}
            </Tag>
            <Tag>{`${offer.rooms} ${t('member-page:item.rooms')}`}</Tag>
            <Tag type='secondary'>
              {offer.active
                ? t('member-page:item.active')
                : t('member-page:item.inactive')}
            </Tag>
          </TagsSection>
        </Item>
      ))}
      <LoadingSection loading={isFetchingOffers}>
        <LoadingList/>
        <LoadingText>{t('member-page:item.offer loading')}</LoadingText>
      </LoadingSection>
    </Wrapper>
  );
};

const mapStateToProps = state => ({
  offers: getOffers(state),
  isFetchingOffers: getIsFetchingOffers(state)
});

const mapDispatchToProps = dispatch => ({
  fetchOffers: (page, requestId) => dispatch(doFetchOffers(page, requestId))
});

export default compose(
  connect(
    mapStateToProps,
    mapDispatchToProps
  ),
  withRouter
)(OfferList);
