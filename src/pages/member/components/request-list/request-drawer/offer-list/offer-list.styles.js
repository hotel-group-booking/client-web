import styled from 'styled-components';
import { Frown } from 'react-feather';

const Wrapper = styled.div`
  margin-top: 1px;
  overflow-y: auto;
  flex-grow: 1;
`;

const LoadingSection = styled.div`
  height: 120px;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  visibility: ${props => props.loading ? 'visible' : 'hidden'};
`;

const LoadingText = styled.div`
  font-size: 1rem;
  color: var(--text-sub);
  font-family: var(--font-baloo);
  margin-top: 1rem;
`;

const Item = styled.div`
  min-height: 110px;
  padding: .5rem;
  border-left: 5px solid;
  border-left-color: #e0388f;
  display: flex;
  flex-direction: column;
  transition: background-color .2s;

  :nth-of-type(odd) {
    border-left-color: var(--text-em);
    background-color: #3279790f;
  }

  :hover {
    background-color: #e4eaec;
  }
`;

const Description = styled.div`
  font-size: 1rem;
  color: var(--text-em);
  font-weight: 600;
  cursor: pointer;
  :hover {
    text-decoration: underline
  }
`;

const CreatorSection = styled.div`
  display: flex;
  flex-direction: row;
  font-size: .85rem;
  color: var(--text-sub);
`;

const HotelLink = styled.div`
  color: var(--gray);
  margin-left: .2rem;
  cursor: pointer;
  :hover {
    color: var(--gray-dark);
    text-decoration: underline;
  }
`;

const TagsSection = styled.div`
  display: flex;
  flex-direction: row;
  margin-top: .5rem;
`;

const Tag = styled.span`
  background-color: ${props => {
    switch (props.type) {
      case 'primary':
        return '#005b6d';
      case 'secondary':
        return 'rgb(224, 56, 143)';
      default:
        return 'rgb(61, 164, 211)';
    }
  }};
  color: var(--white);
  font-size: ${props => props.large ? '1rem' : '.85rem'};
  padding: ${props => props.large ? '.25rem 1rem' : '0 .3rem'};
  border-radius: ${props => props.large ? '.3rem' : '.2rem'};
  margin-right: 0.5rem;
`;

const EmptySection = styled.div`
  height: 180px;
  background-color: #3279790f;
  padding: 0 5rem;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  border-radius: 0.5rem;

  span {
    font-size: 1rem;
    color: var(--text-sub);
    font-weight: 500;
    text-align: center;
    margin-top: .5rem;
  }
`;

const FrownIcon = styled(Frown)`
  color: var(--text-sub);
`;

export { 
  Wrapper,
  LoadingSection,
  LoadingText,
  Item,
  Description,
  CreatorSection,
  HotelLink,
  TagsSection,
  Tag,
  EmptySection,
  FrownIcon
};
