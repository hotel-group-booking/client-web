import styled from 'styled-components';
import { min } from 'ramda';
import {
  Drawer as AntDrawer,
  Input as AntInput,
  Row as AntRow,
  Col as AntCol
} from 'antd';

const Wrapper = styled(AntDrawer).attrs(props => ({
  placement: 'right',
  width: `${min(window.innerWidth, 520)}px`
}))`
  .ant-drawer-body {
    padding: 0;
  }
  .ant-drawer-wrapper-body {
    background-color: var(--background-primary);
    overflow: hidden !important;
  }
`;

const Tag = styled.span`
  background-color: ${props => {
    switch (props.type) {
      case 'primary':
        return '#005b6d';
      case 'secondary':
        return 'rgb(224, 56, 143)';
      default:
        return 'rgb(61, 164, 211)';
    }
  }};
  color: var(--white);
  font-size: ${props => (props.large ? '1rem' : '.75rem')};
  padding: ${props => (props.large ? '.25rem 1rem' : '0 .25rem')};
  border-radius: ${props => (props.large ? '.3rem' : '.2rem')};
  margin-right: 0.5rem;
`;

const DrawerContent = styled.div`
  display: flex;
  flex-direction: column;
  height: 94vh;
`;

const Header = styled.div`
  background-color: var(--text-em);
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  color: var(--white);
  padding: 2rem 1rem;
  margin-bottom: 0.5rem;
  position: relative;
  div {
    margin-top: 1.25rem;
    font-family: var(--font-baloo);
    font-size: 1rem;
    text-align: center;
  }
`;

const HeaderStatus = styled.span`
  position: absolute;
  top: 0.25rem;
  left: 0.5rem;
  font-size: 1rem;
  span {
    cursor: pointer;
    font-size: 1rem;
    padding: 0 0.75rem;
    transition: background-color 0.2s;
    :hover {
      background-color: var(--danger);
    }
  }
`;

const HeaderEditToggle = styled.span`
  position: absolute;
  cursor: pointer;
  display: flex;
  flex-direction: column;
  top: 0.25rem;
  right: 0.5rem;
  svg {
    margin-bottom: 0.2rem;
  }
`;

const Buttons = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: flex-end;
  padding: 0.5rem 1.5rem;
`;

const Button = styled.button`
  padding: 0.25rem 1rem;
  border: none;
  border-radius: 0.25rem;
  cursor: pointer;
  color: var(--white);
  font-weight: 500;
  transition: background-color 0.2s;
  margin-left: 0.4rem;
  background-color: ${props =>
    props.danger ? 'var(--danger)' : 'var(--primary)'};

  :hover {
    background-color: ${props =>
      props.danger ? 'rgb(224, 56, 143)' : 'rgb(61, 164, 211)'};
  }
`;

const Input = styled(AntInput)`
  margin-top: 0.5rem;
`;

const Row = styled(AntRow).attrs(props => ({
  gutter: 24
}))``;

const Col = styled(AntCol)``;

const Body = styled.div`
  display: flex;
  flex-direction: column;
  color: var(--text-em);
  padding: 0.5rem 1.5rem;
  border-top: 1px dotted var(--text-em);
  border-bottom: 1px dotted var(--text-em);
`;

const Item = styled.div`
  display: flex;
  flex-direction: row;
  color: var(--text-em);
  font-size: 1rem;
  margin: 0.2rem;
`;

const Title = styled.span`
  font-weight: 500;
`;

const Text = styled.span`
  margin-left: auto;
`;

export {
  Wrapper,
  DrawerContent,
  Header,
  Buttons,
  Button,
  HeaderEditToggle,
  HeaderStatus,
  Input,
  Row,
  Col,
  Tag,
  Body,
  Item,
  Title,
  Text
};
