import React from 'react';
import { useTranslation } from 'react-i18next';
import { Wrapper, ActivityIcon, Tip } from './empty-list.styles';  

const EmptyList = (props) => {
  const [t] = useTranslation('member-page');
  return (
    <Wrapper {...props}>
      <ActivityIcon/>
      <span>{t('member-page:request empty')}</span>
      <Tip>{t('member-page:tips.request empty')}</Tip>
    </Wrapper>
  );
};

export default EmptyList;
