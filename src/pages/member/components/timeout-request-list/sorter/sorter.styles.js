import styled from 'styled-components';
import { Check, ChevronDown } from 'react-feather';

const Wrapper = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  background-color: #e4f3f6;
  color: var(--text-em);
  padding: 0.3rem 0.75rem;
  margin-bottom: 0.5rem;
  margin-left: 0.25rem;
  width: fit-content;
  border-radius: 0.25rem;
  cursor: pointer;
  transition: background-color 0.2s;
  font-weight: 500;
  position: relative;

  svg {
    margin-left: 0.2rem;
    margin-top: 2px;
  }

  :hover {
    background-color: var(--brand-color);
    color: var(--white);
  }
`;

const Dropdown = styled.div`
  position: absolute;
  display: flex;
  flex-direction: column;
  background-color: var(--white);
  color: var(--text-title);
  padding: 0.75rem 0.1rem;
  box-shadow: var(--highlight);
  border-radius: 0.25rem;
  top: 2.2rem;
  left: 0;
  width: 120px;
  z-index: 1;
  transition: visibility 0s, height 0.25s, opacity 0.25s 0.1s;
  visibility: ${props => (props.visible ? 'visible' : 'hidden')};
  height: ${props => (props.visible ? '170px' : '0')};
  opacity: ${props => (props.visible ? '1' : '0')};
`;

const CheckIcon = styled(Check).attrs(props => ({
  size: 16
}))`
  position: absolute;
  left: 12px;
`;

const Chevron = styled(ChevronDown).attrs(props => ({
  size: 16
}))`
  transform: ${props => props.expanded ? 'rotate(-180deg)' : 'rotate(0deg)'};
`;

const DropdownItem = styled.div`
  padding: 0.5rem 0.25rem;
  position: relative;
  span {
    margin-left: 36px;
  }

  :hover {
    background-color: #efefef;
  }
`;

export { Wrapper, Dropdown, DropdownItem, Chevron, CheckIcon };
