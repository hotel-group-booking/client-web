import React from 'react';
import { connect } from 'react-redux';
import { getUser } from 'reducers/user';
import { ApplicationNavigator } from 'components';
import {
  Menu,
  RequestList,
  NewRequest,
  OfferDetail,
  InactiveReqeustList,
  TimeoutRequestList
} from './components';
import { Switch, Route } from 'react-router-dom';
import { Wrapper, MainSection, Row, Col } from './member.styles';

const MemberPage = ({ history, user, ...rest }) => {
  return (
    <>
      {user && (
        <Wrapper {...rest}>
          <ApplicationNavigator>
            <Menu />
          </ApplicationNavigator>

          <MainSection>
            <Row>
              <Col xs={0} md={6} lg={5}>
                <Menu standalone />
              </Col>
              <Col xs={24} md={18} lg={19}>
                <Switch>
                  <Route exact path='/member' component={RequestList} />
                  <Route path='/member/requests' component={RequestList} />
                  <Route path='/member/create-request' component={NewRequest} />
                  <Route path='/member/offer/:id' component={OfferDetail} />
                  <Route
                    path='/member/inactive-request'
                    component={InactiveReqeustList}
                  />
                  <Route
                    path='/member/timeout-request'
                    component={TimeoutRequestList}
                  />
                </Switch>
              </Col>
            </Row>
          </MainSection>
        </Wrapper>
      )}
    </>
  );
};

const mapStateToProps = state => ({
  user: getUser(state)
});

export default connect(mapStateToProps)(MemberPage);
