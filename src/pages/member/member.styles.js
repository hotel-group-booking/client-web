import styled from 'styled-components';
import { Row as AntRow, Col as AntCol } from 'antd';

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
`;

const MainSection = styled.div`
  padding: 1rem;
  width: 100%;
  margin: 0 auto;
  max-width: 1100px;

  @media (min-width: 768px) {
    margin-top: 2rem;
  }
`;

const Row = styled(AntRow).attrs(props => ({
  gutter: 0
}))`

`;

const Col = styled(AntCol)``;

export { Wrapper, MainSection, Col, Row };