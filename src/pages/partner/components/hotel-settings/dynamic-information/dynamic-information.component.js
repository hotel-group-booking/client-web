import React, { useState, useEffect } from 'react';
import { useTranslation } from 'react-i18next';
import { clone, remove } from 'ramda';
import {
  Wrapper,
  Button,
  ListSection,
  Row,
  Col,
  InputGroup,
  Input,
  TextArea
} from './dynamic-information.styles';

const DynamicInformation = ({ infos, onChange, ...rest }) => {
  const [informations, setInformations] = useState(infos);
  const [t] = useTranslation('partner-page');

  useEffect(() => {
    onChange(informations);
  }, [informations]);

  const handleAddInformation = e => {
    setInformations([
      ...informations,
      { title: '', content: '' }
    ]);
  };

  const handleTitleChange = index => e => {
    const newInfos = clone(informations);
    newInfos[index].title = e.target.value;
    setInformations(newInfos);
  };

  const handleContentChange = index => e => {
    const newInfos = clone(informations);
    newInfos[index].content = e.target.value;
    setInformations(newInfos);
  };

  const handleDeleteInfo = index => e => {
    setInformations(remove(index, 1, informations));
  };

  return (
    <Wrapper>
      <Button ghost icon='plus' onClick={handleAddInformation}>
        {t('partner-page:hotel-settings.buttons.add information')}
      </Button>

      <ListSection>
        {informations.map((info, index) => 
           <InputGroup key={index}>
           <Row>
             <Col xs={22}>
               <Input
                 placeholder={t(
                   'partner-page:hotel-settings.placeholders.content title'
                 )}
                 value={info.title}
                 onChange={handleTitleChange(index)}
               />
             </Col>
             <Col xs={2} right>
               <Button ghost type='danger' icon='delete' onClick={handleDeleteInfo(index)}/>
             </Col>
           </Row>
           <TextArea
             rows={4}
             placeholder={t('partner-page:hotel-settings.placeholders.content')}
             value={info.content}
             onChange={handleContentChange(index)}
           />
         </InputGroup>
        )}
      </ListSection>
    </Wrapper>
  );
};

export default DynamicInformation;
