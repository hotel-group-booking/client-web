import styled from 'styled-components';
import {
  Button as AntButton,
  Row as AntRow,
  Col as AntCol,
  Input as AntInput
} from 'antd';

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  justify-content: flex-start;
`;

const Button = styled(AntButton)`
  color: var(--text-title);
  border-color: var(--text-sub);
  outline: none;

  :focus {
    border-color: var(--text-sub);
    color: var(--text-title);
  }

  :hover {
    color: #40a9ff;
    border-color: #40a9ff;
  }
`;

const ListSection = styled.div`
  margin-top: 0.5rem;
  display: flex;
  flex-direction: column;
  width: 100%;
`;

const Row = styled(AntRow)``;

const Col = styled(AntCol)`
  ${props =>
    props.right &&
    `
    display: flex;
    justify-content: flex-end;
  `}
`;

const InputGroup = styled.div`
  display: flex;
  flex-direction: column;
  width: 100%;
`;

const Input = styled(AntInput)`
  width: 100%;
  color: var(--text-title);
  font-weight: 500;
`;

const TextArea = styled(AntInput.TextArea)`
  margin-top: 0.5rem;
  width: 100%;
`;

export { Wrapper, Button, ListSection, Row, Col, Input, InputGroup, TextArea };
