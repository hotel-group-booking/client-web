import React, { useState, useEffect } from 'react';
import { compose, range, filter } from 'ramda';
import { connect } from 'react-redux';
import { getUser } from 'reducers/user';
import { useTranslation } from 'react-i18next';
import { validator } from 'config/validator';
import moment from 'moment';
import Map from './map/map.component';
import DynamicInformation from './dynamic-information/dynamic-information.component';
import { toast } from 'react-toastify';
import PhotoLibrary from './photo-library/photo-library.component';
import { Icon } from 'antd';
import { LocationAutocomplete } from 'components';
import {
  doUpdate,
  doUploadPhotos,
  getDeletingPhoto,
  doDeletePhoto,
  getResult,
  doClearResult,
  getIsUpdating
} from 'reducers/hotel';
import {
  Wrapper,
  Container,
  HeaderSection,
  RedirectButton,
  Form,
  FormSection,
  Input,
  FormItem,
  InputNumber,
  Select,
  Option,
  TimePicker,
  Slider,
  Label,
  Col,
  Row,
  FooterSection,
  SubmitButton,
  AdditionalSection
} from './hotel-settings.styles';

const HotelSettings = ({
  history,
  user,
  deletingPhoto,
  result,
  isUpdating,
  form: { getFieldDecorator, validateFields, setFieldsValue },
  update,
  uploadPhotos,
  deletePhoto,
  clearResult,
  ...rest
}) => {
  const [address, setAddress] = useState();
  const [extraInfo, setExtraInfo] = useState(user.hotel.extraInfo);
  const [location, setLocation] = useState(user.hotel.localtion)
  const [t] = useTranslation(['partner-page', 'validatior']);

  useEffect(() => {
    return () => {
      clearResult();
    };
  }, []);

  useEffect(() => {
    if (result && result.type === 'success') {
      toast.success(t('partner-page:hotel-settings.noti.edit success'), 0.8);
    }
  }, [result]);

  const handleRedirect = url => e => {
    history.push(url);
  };

  const handleSliderChange = values => {
    setFieldsValue({
      budgetFrom: values[0],
      budgetTo: values[1]
    });
  };

  const handleLocationChange = location => {
    setLocation(location);
  };

  const handleAddressChange = address => {
    setAddress(address);
  };

  const handleDynamicInfoChange = infos => {
    setExtraInfo(infos);
  };

  const handleUploadPhotos = photos => {
    uploadPhotos(user._id, user.hotel._id, photos);
  };

  const handleDeletePhoto = photo => {
    deletePhoto(user._id, user.hotel._id, photo.id);
  };

  const handleSubmit = e => {
    e.preventDefault();
    validateFields((err, values) => {
      if (!err) {
        const hotelInfo = {
          ...values,
          address,
          checkinTime: values.checkinTime.format('h:mm A'),
          checkoutTime: values.checkoutTime.format('h:mm A'),
          location,
          extraInfo
        };

        update(user._id, user.hotel._id, filter(Boolean)(hotelInfo));
      }
    });
  };

  const rules = validator(t);
  const name = () =>
    getFieldDecorator('name', {
      initialValue: user.hotel.name,
      rules: [rules.required]
    })(<Input />);

  const starRating = () =>
    getFieldDecorator('starRating', {
      initialValue: user.hotel.starRating,
      rules: [rules.required]
    })(
      <Select>
        {range(1, 6).map(star => (
          <Option key={star} value={star}>
            <span>
              {star}{' '}
              {star > 1
                ? t('partner-page:count.stars')
                : t('partner-page:count.star')}
            </span>
          </Option>
        ))}
      </Select>
    );

  const totalRoom = () =>
    getFieldDecorator('totalRoom', {
      initialValue: user.hotel.totalRoom,
      rules: [rules.required]
    })(<InputNumber />);

  const checkin = () =>
    getFieldDecorator('checkinTime', {
      initialValue: moment(user.hotel.checkinTime || '0:00', 'h:mm A')
    })(<TimePicker use12Hours format='h:mm A' />);

  const checkout = () =>
    getFieldDecorator('checkoutTime', {
      initialValue: moment(user.hotel.checkoutTime || '0:00', 'h:mm A')
    })(<TimePicker use12Hours format='h:mm A' />);

  const budgetFrom = () =>
    getFieldDecorator('budgetFrom', {
      initialValue: user.hotel.budgetFrom
    })(<Input addonAfter='000 VND' />);

  const budgetTo = () =>
    getFieldDecorator('budgetTo', {
      rules: [rules.required],
      initialValue: user.hotel.budgetTo
    })(<Input addonAfter='000 VND' />);

  return (
    <Wrapper>
      <Container>
        <HeaderSection>
          <RedirectButton
            ghost
            type='primary'
            onClick={handleRedirect(`/hotel/${user.hotel._id}`)}
          >
            {t('partner-page:hotel-settings.buttons.view hotel page')}
          </RedirectButton>
        </HeaderSection>

        <FormSection>
          <Form onSubmit={handleSubmit}>
            <FormItem
              label={t('partner-page:hotel-settings.labels.hotel name')}
            >
              {name()}
            </FormItem>
            <Row gutter={24}>
              <Col xs={12} lg={6}>
                <FormItem
                  label={t('partner-page:hotel-settings.labels.total room')}
                >
                  {totalRoom()}
                </FormItem>
              </Col>
              <Col xs={12} lg={6}>
                <FormItem
                  label={t('partner-page:hotel-settings.labels.star rating')}
                >
                  {starRating()}
                </FormItem>
              </Col>
              <Col xs={12} lg={6}>
                <FormItem
                  label={t('partner-page:hotel-settings.labels.check in time')}
                >
                  {checkin()}
                </FormItem>
              </Col>
              <Col xs={12} lg={6}>
                <FormItem
                  label={t('partner-page:hotel-settings.labels.check out time')}
                >
                  {checkout()}
                </FormItem>
              </Col>
            </Row>

            <Row>
              <Col xs={24} md={8}>
                <FormItem
                  label={t('partner-page:hotel-settings.labels.nightly budget')}
                  required
                  mb='0'
                >
                  {budgetFrom()}
                </FormItem>
                <FormItem>{budgetTo()}</FormItem>
              </Col>
              <Col xs={24} md={16}>
                <Slider onSliderChange={handleSliderChange} />
              </Col>
            </Row>

            <Row>
              <Col xs={24} mb='0.5rem'>
                <Label>{t('partner-page:hotel-settings.labels.address')}</Label>
              </Col>
              <Col xs={24}>
                <Map
                  defaultAddress={user.hotel.address}
                  name={user.hotel.name}
                  onAddressChange={handleAddressChange}
                />
              </Col>
            </Row>
            <Row mt='1rem'>
              <Col xs={24} mb='0.5rem'>
                <Label>
                  {t('partner-page:hotel-settings.labels.location')}
                </Label>
              </Col>
              <Col xs={24}>
                <LocationAutocomplete onSelect={handleLocationChange} defaultValue={user.hotel.location.name}/>
              </Col>
            </Row>
            <AdditionalSection>
              <Row>
                <Col xs={24} mb='0.5rem'>
                  <Label>
                    {t('partner-page:hotel-settings.labels.hotel information')}
                  </Label>
                </Col>
                <Col xs={24}>
                  <DynamicInformation
                    infos={extraInfo}
                    onChange={handleDynamicInfoChange}
                  />
                </Col>
              </Row>
            </AdditionalSection>
            <FooterSection>
              <SubmitButton>
                {isUpdating && <Icon type='loading' />}
                {t('partner-page:hotel-settings.buttons.save')}
              </SubmitButton>
            </FooterSection>
          </Form>
        </FormSection>
        <Row>
          <Col xs={24}>
            <Label>{t('partner-page:hotel-settings.labels.photos')}</Label>
          </Col>
          <Col xs={24}>
            <PhotoLibrary
              onUpload={handleUploadPhotos}
              onDelete={handleDeletePhoto}
              photos={user.hotel.images}
              deletingPhoto={deletingPhoto}
            />
          </Col>
        </Row>
      </Container>
    </Wrapper>
  );
};

const mapStateToProps = state => ({
  user: getUser(state),
  deletingPhoto: getDeletingPhoto(state),
  result: getResult(state),
  isUpdating: getIsUpdating(state)
});

const mapDispatchToProps = dispatch => ({
  update: (userId, hotelId, data) => dispatch(doUpdate(userId, hotelId, data)),
  uploadPhotos: (userId, hotelId, images) =>
    dispatch(doUploadPhotos(userId, hotelId, images)),
  deletePhoto: (userId, hotelId, photoId) =>
    dispatch(doDeletePhoto(userId, hotelId, photoId)),
  clearResult: () => dispatch(doClearResult())
});

export default compose(
  connect(
    mapStateToProps,
    mapDispatchToProps
  ),
  Form.create()
)(HotelSettings);
