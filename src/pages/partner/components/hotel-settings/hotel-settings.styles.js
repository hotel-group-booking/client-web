import styled from 'styled-components';
import {
  Button as AntButton,
  Form as AntForm,
  Input as AntInput,
  InputNumber as AntInputNumber,
  Select as AntSelect,
  TimePicker as AntTimePicker,
  Row as AntRow,
  Col as AntCol
} from 'antd';
import { BudgetSlider, Button } from 'components';

const Wrapper = styled.div`
  background-color: var(--white);
  box-shadow: var(--highlight);
  padding: 0.5rem;
  border-radius: 0.5rem;
  margin: 0.25rem;
`;

const Container = styled.div`
  display: flex;
  flex-direction: column;
`;

const HeaderSection = styled.div`
  padding: 0.5rem 0;
  margin: 0 0.5rem;
  display: flex;
  flex-direction: row;
  justify-content: flex-end;
  border-bottom: 1px solid #c2c6da;
`;

const FormSection = styled.div`
  display: flex;
  flex-direction: column;
  padding: 0.5rem 0;
  margin: 0 0.5rem;
`;

const RedirectButton = styled(AntButton)``;

const Form = styled(AntForm)``;

const FormItem = styled(AntForm.Item)`
  margin-bottom: ${props => props.mb};
  label {
    font-size: 1rem;
    color: var(--text-em);
    font-weight: 500;
  }
`;

const Input = styled(AntInput)`
  width: 100%;
`;

const InputNumber = styled(AntInputNumber)`
  width: 100%;
`;

const Select = styled(AntSelect)`
  width: 100%;
`;

const Option = styled(AntSelect.Option)``;

const TimePicker = styled(AntTimePicker)`
  width: 100%;
`;

const Slider = styled(BudgetSlider)`
  margin-bottom: 0.5rem;
  @media (min-width: 768px) {
    margin-top: 46px;
    padding: 0 4rem;
  }
`;

const Label = styled.span`
  font-size: 1rem;
  font-weight: 500;
  color: var(--text-em);
  margin-bottom: 0.5rem;
`;

const Row = styled(AntRow)`
  margin-top: ${props => props.mt};
`;

const Col = styled(AntCol)`
  margin-bottom: ${props => props.mb};
`;

const AdditionalSection = styled.div`
  display: flex;
  flex-direction: column;
  margin-top: 1rem;
`;

const FooterSection = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: center;
  margin-top: 1rem;
`;

const SubmitButton = styled(Button).attrs(props => ({
  type: 'gradient',
  htmlType: 'subbmit'
}))`
  text-transform: uppercase;
  i {
    margin-right: 1rem;
  }
`;

export {
  Wrapper,
  Container,
  HeaderSection,
  RedirectButton,
  FormSection,
  Form,
  FormItem,
  Input,
  InputNumber,
  Select,
  Option,
  TimePicker,
  Slider,
  Label,
  Row,
  Col,
  FooterSection,
  SubmitButton,
  AdditionalSection,
};
