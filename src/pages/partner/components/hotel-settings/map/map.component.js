import React, { useState } from 'react';
import { GeocodeAutocomplete } from 'components';
import GoogleMapReact from 'google-map-react';
import { Wrapper, Marker, Tooltip, MapSection } from './map.styles';

const MapMarker = ({ name }) => {
  return (
    <Tooltip title={name} placement='top'>
      <Marker/>
    </Tooltip>
  );
};

const Map = ({defaultAddress, name, onAddressChange, ...rest}) => {

  const [address, setAddress] = useState(defaultAddress);
  const [draggable, setDraggable] = useState(true);
  const [zoom, setZoom] = useState(15);
  const [center, setCenter] = useState({
    lat: defaultAddress.lat,
    lng: defaultAddress.lng
  });

  const handleAddressSelect = newAddress => {
    onAddressChange(newAddress);
  };

  const onMapChange = ({ zoom }) => {
    setZoom(zoom);
  };

  const handleChildMouseDown = (_, __, mouse) => {
    setDraggable(false);
  };

  const handleChildMouseUp = (_, __, mouse) => {
    setDraggable(true);
    setCenter({
      lat: mouse.lat,
      lng: mouse.lng
    });
    onAddressChange(address);
  };

  const handleChildMouseMove = (_, __, mouse) => {
    setAddress({
      ...address,
      lat: mouse.lat,
      lng: mouse.lng
    });
  };

  return (
    <Wrapper>
      <GeocodeAutocomplete
        onSelect={handleAddressSelect}
        defaultValue={address.name}
      />
      <MapSection>
        <GoogleMapReact
          draggable={draggable}
          onChange={onMapChange}
          zoom={zoom}
          center={center}
          onChildMouseDown={handleChildMouseDown}
          onChildMouseUp={handleChildMouseUp}
          onChildMouseMove={handleChildMouseMove}

        >
          <MapMarker
            lat={address && address.lat}
            lng={address && address.lng}
            name={name}
          />
        </GoogleMapReact>
      </MapSection>
    </Wrapper>
  );
};

export default Map;
