import React, { useState, useEffect } from 'react';
import { map, isEmpty } from 'ramda';
import { base64Reader } from 'lib';
import { useTranslation } from 'react-i18next';
import { XCircle } from 'react-feather';
import {
  Wrapper,
  UploadLabel,
  Icon,
  Upload,
  PreviewSection,
  Carousel,
  ImageContainer,
  SubmitSection,
  Button,
  LibrarySection,
  PhotoContainer,
  Photo,
  DeleteButton,
  Loading
} from './photo-library.styles';

const PhotoLibrary = ({
  deletingPhoto,
  photos,
  onUpload,
  onDelete,
  ...rest
}) => {
  const [temps, setTemps] = useState([]);
  const [loading, setLoading] = useState(false);
  const [uploading, setUploading] = useState(false);
  const [t] = useTranslation('partner-page');

  useEffect(() => {
    setUploading(false);
    setTemps([]);
  }, [photos]);

  const handleFileChange = async e => {
    const files = e.target.files;
    setLoading(true);
    const promises = map(file => base64Reader(500)(file), files);
    const temporaryUploaded = await Promise.all(promises);
    setTemps([...temps, ...temporaryUploaded]);
    setLoading(false);
  };

  const handleUpload = e => {
    onUpload(temps);
    setUploading(true);
  };

  const handleDelete = photo => e => {
    onDelete(photo);
  };

  return (
    <Wrapper>
      <UploadLabel>
        <Icon type={loading ? 'loading' : 'select'} />
        <span>{t('partner-page:hotel-settings.buttons.select photos')}</span>
      </UploadLabel>
      <Upload
        onChange={handleFileChange}
        multiple
        accept='image/jpeg,image/x-png'
      />

      <PreviewSection>
        <Carousel>
          {temps.map((temp, i) => (
            <ImageContainer key={i}>
              <img src={temp} alt='preview' />
            </ImageContainer>
          ))}
        </Carousel>
        <SubmitSection>
          {!isEmpty(temps) && (
            <Button
              icon={uploading ? 'loading' : 'upload'}
              onClick={handleUpload}
            >
              {t('partner-page:hotel-settings.buttons.upload')}
            </Button>
          )}
        </SubmitSection>
      </PreviewSection>

      <LibrarySection>
        {!isEmpty(photos) && (
          <PhotoContainer>
            {photos.map((photo, index) => (
              <Photo key={photo.id} src={photo.link}>
                <DeleteButton onClick={handleDelete(photo)}>
                  <XCircle size={24} />
                </DeleteButton>
                {deletingPhoto === photo.id && (
                  <Loading>
                    <Icon type='loading' />
                  </Loading>
                )}
              </Photo>
            ))}
          </PhotoContainer>
        )}
      </LibrarySection>
    </Wrapper>
  );
};

export default PhotoLibrary;
