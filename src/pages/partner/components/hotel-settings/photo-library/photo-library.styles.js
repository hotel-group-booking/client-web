import styled from 'styled-components';
import {
  Button as AntButton,
  Icon as AntIcon,
  Carousel as AntCarousel
} from 'antd';

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  justify-content: flex-start;
  margin-top: 0.5rem;
`;

const Button = styled(AntButton)`
  color: var(--text-title);
  border-color: var(--text-sub);
  outline: none;

  :focus {
    border-color: var(--text-sub);
    color: var(--text-title);
  }

  :hover {
    color: #40a9ff;
    border-color: #40a9ff;
  }
`;

const UploadLabel = styled.label.attrs(props => ({
  htmlFor: 'file-upload'
}))`
  color: var(--text-title);
  padding: 0.25rem 1rem !important;
  border: 1px solid var(--text-sub);
  border-radius: 0.25rem;
  cursor: pointer;
  transition: 0.2s all;
  :hover {
    color: #40a9ff;
    border-color: #40a9ff;
  }
  span {
    margin-left: 0.5rem;
  }
`;

const Upload = styled.input.attrs(props => ({
  type: 'file',
  id: 'file-upload'
}))`
  display: none !important;
`;

const Icon = styled(AntIcon)``;

const PreviewSection = styled.div`
  width: 100%;
  margin-top: 1rem;
`;
const Carousel = styled(AntCarousel)`
  &.slick-slide {
    height: ${props => props.height || '300px'};
    align-self: center;
    padding: 2rem;
    h3 {
      color: var(--white);
    }
  }
`;

const ImageContainer = styled.div`
  height: ${props => props.height || '300px'};
  display: flex !important;
  justify-content: center;
  align-items: center;
  background-color: #364d79;
  padding: 1rem 0;
  border-radius: 0.5rem;
  & > img {
    max-height: 100%;
    max-width: 100%;
  }
`;

const SubmitSection = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: flex-end;
  margin-top: .5rem;
`;

const LibrarySection = styled.div`
  display: flex;
  flex-direction: column;
  width: 100%;
`;

const PhotoContainer = styled.div`
  margin-top: .5rem;
  width: 100%;
  border-radius: .5rem;
  border: 1px solid var(--text-sub);
  padding: .5rem;
  display: flex;
  flex-direction: row;
  flex-wrap: wrap;
`;

const Photo = styled.div`
  background-image: ${props => props.src && `url(${props.src})`};
  width: 20%;
  position: relative;
  background-size: cover;
  border: 0.25rem solid var(--white);

  ::before {
    content: '';
    display: block;
    padding-top: 100%;
  }
`;

const Loading = styled.div`
  position: absolute;
  top: 0;
  right: 0;
  bottom: 0;
  left: 0;
  display: flex;
  justify-content: center;
  align-items: center;
  padding: 0.3rem;
  background-color: rgba(0, 0, 0, 0.35);

  svg {
    width: 48px;
    height: 48px;
    color: var(--white);
  }
`;

const DeleteButton = styled.div`
  position: absolute;
  top: 0;
  right: 0;
  bottom: 0;
  left: 0;
  display: flex;
  justify-content: flex-end;
  align-items: flex-start;
  padding: 0.3rem;

  :hover {
    background-color: rgba(0, 0, 0, 0.35);
    svg {
      cursor: pointer;
      color: var(--white);
    }
  }
`;



export {
  Wrapper,
  Button,
  UploadLabel,
  Upload,
  Icon,
  PreviewSection,
  Carousel,
  ImageContainer,
  SubmitSection,
  LibrarySection,
  PhotoContainer,
  Photo,
  Loading,
  DeleteButton
};
