import React, { useEffect } from 'react';
import { compose } from 'ramda';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { useTranslation } from 'react-i18next';
import { getUser } from 'reducers/user';
import { getSize as getRequestSize } from 'reducers/request';
import { getSize as getOfferSize, doFetchPartnerOffers } from 'reducers/offer';
import {
  Wrapper,
  Header,
  HeaderContainer,
  Avatar,
  Username,
  Body,
  ItemGroup,
  Item,
  ColorBlock,
  Label,
  Badge
} from './menu.styles';

const Menu = ({
  history,
  user,
  requestCount,
  offerCount,
  fetchOffers,
  ...rest
}) => {
  const [t] = useTranslation('application');
  useEffect(() => {
    if (user) {
      fetchOffers(user._id, 0);
    }
  }, [user]);
  const handleRedirect = url => e => {
    history.push(url);
  };

  return (
    <Wrapper {...rest}>
      <Header>
        <HeaderContainer>
          {user.photo ? (
            <Avatar src={user.photo.link} size={96} />
          ) : (
            <Avatar icon='user' size={96} />
          )}
          <Username>{user.displayName}</Username>
        </HeaderContainer>
      </Header>
      <Body>
        <ItemGroup>
          <Item onClick={handleRedirect('/partner/requests')}>
            <ColorBlock color='#005b6d' />
            <Label>{t('application:menu.requests')}</Label>
            <Badge count={requestCount} bg='#005b6d' />
          </Item>
          <Item onClick={handleRedirect('/partner/offers')}>
            <ColorBlock color='#1c3f62' />
            <Label>{t('application:menu.offers')}</Label>
            <Badge count={offerCount} bg='#1c3f62' />
          </Item>
          <Item onClick={handleRedirect('/partner/hotel-settings')}>
            <ColorBlock color='#000f28' />
            <Label>{t('application:menu.hotel settings')}</Label>
          </Item>
        </ItemGroup>
      </Body>
    </Wrapper>
  );
};

const mapStateToProps = state => ({
  user: getUser(state),
  requestCount: getRequestSize(state),
  offerCount: getOfferSize(state)
});

const mapDispatchToProps = dispatch => ({
  fetchOffers: (partnerId, page) =>
    dispatch(doFetchPartnerOffers(partnerId, page))
});

export default compose(
  connect(
    mapStateToProps,
    mapDispatchToProps
  ),
  withRouter
)(Menu);
