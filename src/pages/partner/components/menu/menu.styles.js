import styled from 'styled-components';
import { Avatar as AntAvatar, Badge as AntBadge } from 'antd';

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
`;

const Header = styled.div`
  display:  flex;
  flex-direction: column;
  padding: 2rem 1rem 1rem 1rem;
`;

const HeaderContainer = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  padding-bottom: 1rem;
  border-bottom: 1px solid var(--text-title);
`;

const Username = styled.div`
  font-size: 1.5rem;
  font-weight: 500;
  margin-top: .5rem;
  color: var(--text-em);
`;

const Avatar = styled(AntAvatar)``;

const Badge = styled(AntBadge).attrs(props => ({
  showZero: true
}))`

  margin-left: auto;
  min-width: 40px;

  .ant-badge-count {
    background-color: ${props => props.bg};
  }
`;

const Body = styled.div`
  display: flex;
  flex-direction: column;
  padding: 0 1rem;
`;

const ItemGroup = styled.div`
  display: flex;
  flex-direction: column;
`;

const Item = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: flex-start;
  align-items: center;
`;

const ColorBlock = styled.div`
  width: 16px;
  height: 16px;
  background-color: ${props => props.color};
  border-radius: .2rem;
`;

const Label = styled.div`
  font-size: .9rem;
  color: #669199;
  margin-left: #661999;
  padding: .3rem;
  cursor: pointer;
  margin-left: .5rem;

  :hover {
    color: var(--text-em);
  }
`;

export {
  Wrapper,
  Avatar,
  Badge,
  Header,
  Username,
  HeaderContainer,
  Body,
  ItemGroup,
  Item,
  Label,
  ColorBlock
}