import React from 'react';
import { Drawer } from 'components';
import { useTranslation } from 'react-i18next';
import { validator } from 'config/validator';
import moment from 'moment';
import { ROOM_TYPES } from 'config/constants';
import { budgetFormat } from 'lib';
import {
  Wrapper,
  Input,
  RangePicker,
  FormSection,
  Form,
  Row,
  Col,
  FormItem,
  RequestSection,
  HeaderSection,
  HeaderLabel,
  Item,
  Label,
  Content,
  Body,
  ButtonContainer,
  SaveButton
} from './edit-drawer.styles';

const EditDrawer = ({
  offer,
  onSubmit,
  form: { getFieldDecorator, validateFields },
  ...rest
}) => {
  const [t] = useTranslation(['partner-page', 'validator']);

  const handleSubmit = e => {
    e.preventDefault();
    validateFields((err, values) => {
      if (!err) {
        const offer = {
          budget: Number(values.budget),
          rooms: Number(values.rooms),
          checkin: values.time[0].toISOString(),
          checkout: values.time[1].toISOString()
        };
        onSubmit(offer);
      }
    });
  };

  const rules = validator(t);
  const budget = () =>
    getFieldDecorator('budget', {
      rules: [rules.required],
      initialValue: offer.budget
    })(<Input addonAfter='000 VND' type='number' />);

  const rooms = () =>
    getFieldDecorator('rooms', {
      rules: [rules.required],
      initialValue: offer.rooms
    })(<Input type='number' />);

  const time = () =>
    getFieldDecorator('time', {
      rules: [rules.required],
      initialValue: [moment(offer.checkin), moment(offer.checkout)]
    })(<RangePicker />);

  return (
    <Drawer
      {...rest}
      title={t('partner-page:edit-drawer.title')}
      closable
      placement='right'
    >
      <Wrapper>
        <FormSection>
          <Form onSubmit={handleSubmit}>
            <Row>
              <Col xs={12}>
                <FormItem
                  label={t('partner-page:offer-detail.labels.nightly budget')}
                >
                  {budget()}
                </FormItem>
              </Col>
              <Col xs={12}>
                <FormItem label={t('partner-page:offer-detail.labels.rooms')}>
                  {rooms()}
                </FormItem>
              </Col>

              <Col xs={24}>
                <FormItem label={t('partner-page:offer-detail.labels.time')}>
                  {time()}
                </FormItem>
              </Col>
            </Row>
            <ButtonContainer>
              <SaveButton>
                {t('partner-page:offer-detail.buttons.save')}
              </SaveButton>
            </ButtonContainer>
          </Form>
        </FormSection>
        <RequestSection>
          <HeaderSection>
            <HeaderLabel>
              {t('partner-page:offer-detail.labels.request information')}
            </HeaderLabel>
          </HeaderSection>
          <Body>
            <Item>
              <Label>{t('partner-page:offer-detail.labels.destination')}</Label>
              <Content>{offer.request.destination.name}</Content>
            </Item>
            <Item>
              <Label>{t('partner-page:offer-detail.labels.created at')}</Label>
              <Content>
                {moment(offer.request.created).format('D/M/YYYY - LTS')}
              </Content>
            </Item>
            <Item>
              <Label>{t('partner-page:offer-detail.labels.checkin')}</Label>
              <Content>
                {moment(offer.request.checkin).format('D/M/YYYY')}
              </Content>
            </Item>
            <Item>
              <Label>{t('partner-page:offer-detail.labels.checkout')}</Label>
              <Content>
                {moment(offer.request.checkout).format('D/M/YYYY')}
              </Content>
            </Item>
            <Item>
              <Label>
                {t('partner-page:offer-detail.labels.nightly budget')}
              </Label>
              <Content>
                {budgetFormat(offer.request.budgetFrom, offer.request.budgetTo)}
              </Content>
            </Item>
            <Item>
              <Label>{t('partner-page:offer-detail.labels.room type')}</Label>
              <Content>
                {
                  ROOM_TYPES.find(
                    type => type.value === (offer.request.roomType || 1)
                  ).name
                }
              </Content>
            </Item>
            <Item>
              <Label>{t('partner-page:offer-detail.labels.group type')}</Label>
              <Content>{offer.request.groupType}</Content>
            </Item>
            <Item>
              <Label>{t('partner-page:offer-detail.labels.rooms')}</Label>
              <Content>{`${offer.request.rooms} ${
                offer.request.rooms > 1
                  ? t('partner-page:count.rooms')
                  : t('partner-page:count.room')
              }`}</Content>
            </Item>
            <Item>
              <Label>{t('partner-page:offer-detail.labels.star rating')}</Label>
              <Content>{`${offer.request.starRating} ${
                offer.request.starRating > 1
                  ? t('partner-page:count.stars')
                  : t('partner-page:count.star')
              }`}</Content>
            </Item>
          </Body>
        </RequestSection>
      </Wrapper>
    </Drawer>
  );
};

export default Form.create()(EditDrawer);
