import styled from 'styled-components';
import {
  Form as AntForm,
  Row as AntRow,
  Col as AntCol,
  DatePicker,
  Input as AntInput
} from 'antd';
import { Button } from 'components';

const Wrapper = styled.div`
  padding: 0.5rem;
  display: flex;
  flex-direction: column;
`;

const Form = styled(AntForm)``;

const FormItem = styled(AntForm.Item)`
  label {
    font-size: 1rem;
    font-weight: 500;
    color: var(--text-em);
  }
`;

const Row = styled(AntRow).attrs(props => ({
  gutter: 24
}))``;

const Col = styled(AntCol)``;

const RangePicker = styled(DatePicker.RangePicker)`
  width: 100%;
`;

const Input = styled(AntInput)`
  width: 100%;
`;

const FormSection = styled.div`
  background-color: var(--white);
  border-radius: 0.5rem;
  box-shadow: var(--highlight);
  padding: 0.5rem;
`;

const RequestSection = styled.div`
  background-color: var(--white);
  border-radius: 0.5rem;
  box-shadow: var(--highlight);
  padding: 0.5rem;
  margin-top: 1rem;
`;

const HeaderSection = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  padding: 0.5rem 0.25rem;
  align-items: center;
  border-bottom: 1px solid var(--text-em);
`;

const HeaderLabel = styled.div`
  color: var(--text-title);
  font-size: 1.25rem;
  font-weight: 500;
  line-height: 32px;
`;

const Item = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  padding: 0.5rem 0;
`;

const Label = styled.span`
  font-size: 1rem;
  font-weight: 500;
  color: var(--text-em);
  white-space: nowrap;
  margin-right: 1rem;

  ::after {
    content: ':';
    font-size: 1rem;
    color: var(--text-em);
  }
`;

const Content = styled.span`
  font-size: 1rem;
  color: var(--text-em);
  word-break: break-word;
`;

const Body = styled.div`
  display: flex;
  flex-direction: column;
  margin-top: 0.5rem;
  padding: ${props => props.nopadding ? '0' : '0 0.5rem'};
`;

const ButtonContainer = styled.div`
  display: flex;
  justify-content: flex-end;
`;

const SaveButton = styled(Button).attrs(props => ({
  type: 'gradient',
  htmlType: 'submit'
}))`
  text-transform: uppercase;
`;


export {
  Wrapper,
  Form,
  Row,
  Col,
  FormItem,
  RangePicker,
  Input,
  FormSection,
  RequestSection,
  HeaderSection,
  HeaderLabel,
  Item,
  Label,
  Content,
  Body,
  ButtonContainer,
  SaveButton
};
