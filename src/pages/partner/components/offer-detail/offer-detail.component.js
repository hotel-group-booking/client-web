import React, { useEffect, useState } from 'react';
import EditDrawer from './edit-drawer/edit-drawer.component';
import { connect } from 'react-redux';
import { getUser } from 'reducers/user';
import {
  doFetchDetail,
  getDetail,
  getIsFetchingDetail,
  doUpdateOffer,
  doClearResult
} from 'reducers/offer';
import { useTranslation } from 'react-i18next';
import moment from 'moment';
import { currencyFormat, getDuration } from 'lib';
import { Messager } from 'components';
import { Skeleton } from 'antd';
import { Check, Lock } from 'react-feather';
import {
  Wrapper,
  OfferContainer,
  HeaderSection,
  HeaderLabel,
  ButtonsContainer,
  Button,
  Body,
  Divider,
  Item,
  Label,
  Content,
  Row,
  Col,
  UserContainer,
  DiscussionContainer,
  OfferOverlay,
  OverlayContent
} from './offer-detail.styles';

const OfferDetail = ({
  history,
  user,
  offer,
  isFetchingDetail,
  fetchDetail,
  updateOffer,
  clearResult,
  match,
  ...rest
}) => {
  const [t] = useTranslation('partner-page');
  const [visible, setVisible] = useState(false);

  useEffect(() => {
    fetchDetail(match.params.id);
  }, []);

  const handleOfferEdit = e => {
    setVisible(true);
  };

  const handleOfferToggle = e => {
    updateOffer(offer._id, {
      active: !offer.active
    });
  };

  const handleEdit = data => {
    setVisible(false);
    updateOffer(offer._id, data);
  };

  const handleRedirect = url => e => {
    history.push(url);
  };

  return (
    <Wrapper {...rest}>
      <OfferContainer>
        <OfferOverlay visible={offer && offer.accepted} type='accepted'>
          <OverlayContent type='accepted'>
            <Check size={48} />
            <span>{t('partner-page:offer-detail.accepted')}</span>
          </OverlayContent>
        </OfferOverlay>

        <OfferOverlay visible={offer && offer.blocked} type='blocked'>
          <OverlayContent type='blocked'>
            <Lock size={48} />
            <span>{t('partner-page:offer-detail.blocked')}</span>
          </OverlayContent>
        </OfferOverlay>
        <HeaderSection>
          <HeaderLabel>
            {t('partner-page:offer-detail.labels.offer information')}
          </HeaderLabel>

          {offer && !offer.accepted && !offer.blocked && (
            <ButtonsContainer>
              <Button
                type={offer.active ? 'danger' : 'primary'}
                ghost
                onClick={handleOfferToggle}
              >
                {offer.active
                  ? t('partner-page:offer-detail.buttons.cancel')
                  : t('partner-page:offer-detail.buttons.active')}
              </Button>
              {offer.active && (
                <Button type='primary' ghost onClick={handleOfferEdit}>
                  {t('partner-page:offer-detail.buttons.edit')}
                </Button>
              )}
            </ButtonsContainer>
          )}
        </HeaderSection>

        <Skeleton loading={isFetchingDetail} paragraph={{ rows: 6 }}>
          {offer && (
            <Body>
              <Item>
                <Label>
                  {t('partner-page:offer-detail.labels.nightly budget')}
                </Label>
                <Content>{currencyFormat(offer.budget)}</Content>
              </Item>

              <Item>
                <Label>{t('partner-page:offer-detail.labels.checkin')}</Label>
                <Content>{moment(offer.checkin).format('D/M/YYYY')}</Content>
              </Item>
              <Item>
                <Label>{t('partner-page:offer-detail.labels.checkout')}</Label>
                <Content>{moment(offer.checkout).format('D/M/YYYY')}</Content>
              </Item>

              <Item>
                <Label>{t('partner-page:offer-detail.labels.rooms')}</Label>
                <Content>
                  {`${offer.rooms} ${
                    offer.rooms > 1
                      ? t('partner-page:count.rooms')
                      : t('partner-page:count.room')
                  }`}
                </Content>
              </Item>

              <Item>
                <Label>
                  {t('partner-page:offer-detail.labels.created at')}
                </Label>
                <Content>
                  {moment(offer.created).format('D/M/YYYY - LTS')}
                </Content>
              </Item>

              <Divider dashed />

              <Item>
                <Label>{t('partner-page:offer-detail.labels.total')}</Label>
                <Content>
                  {currencyFormat(
                    offer.budget *
                      offer.rooms *
                      getDuration(offer.checkin, offer.checkout)
                  )}
                </Content>
              </Item>
            </Body>
          )}
        </Skeleton>
      </OfferContainer>

      <Row gutter={24}>
        <Col xs={24} lg={12}>
          <UserContainer>
            <HeaderSection>
              <HeaderLabel>
                {t('partner-page:offer-detail.labels.customer')}
              </HeaderLabel>
            </HeaderSection>
            <Skeleton loading={isFetchingDetail}>
              {offer && (
                <Body>
                  <Item>
                    <Label>{t('partner-page:offer-detail.labels.name')}</Label>
                    <Content>{offer.request.member.displayName}</Content>
                  </Item>
                  <Item>
                    <Label>
                      {t('partner-page:offer-detail.labels.first name')}
                    </Label>
                    <Content>{offer.request.member.firstName}</Content>
                  </Item>
                  <Item>
                    <Label>
                      {t('partner-page:offer-detail.labels.last name')}
                    </Label>
                    <Content>{offer.request.member.lastName}</Content>
                  </Item>
                  <Item>
                    <Label>{t('partner-page:offer-detail.labels.email')}</Label>
                    <Content>{offer.request.member.email}</Content>
                  </Item>
                  <Item>
                    <Label>{t('partner-page:offer-detail.labels.phone')}</Label>
                    <Content>{offer.request.member.phone}</Content>
                  </Item>
                </Body>
              )}
            </Skeleton>
          </UserContainer>
        </Col>

        <Col xs={24} lg={12}>
          <UserContainer>
            <HeaderSection>
              <HeaderLabel>
                {t('partner-page:offer-detail.labels.hotel information')}
              </HeaderLabel>

              <ButtonsContainer>
                <Button
                  type='primary'
                  ghost
                  onClick={handleRedirect(
                    `/hotel/${offer && offer.partner.hotel._id}`
                  )}
                >
                  {t('partner-page:offer-detail.buttons.hotel redirect')}
                </Button>
              </ButtonsContainer>
            </HeaderSection>

            <Skeleton loading={isFetchingDetail}>
              {offer && (
                <Body>
                  <Item>
                    <Label>
                      {t('partner-page:offer-detail.labels.hotel name')}
                    </Label>
                    <Content>{offer.partner.hotel.name}</Content>
                  </Item>
                  <Item>
                    <Label>
                      {t('partner-page:offer-detail.labels.address')}
                    </Label>
                    <Content>{offer.partner.hotel.address.name}</Content>
                  </Item>
                  <Item>
                    <Label>
                      {t('partner-page:offer-detail.labels.star rating')}
                    </Label>
                    <Content>{`${offer.partner.hotel.starRating} ${
                      offer.partner.hotel.starRating > 1
                        ? t('partner-page:count.stars')
                        : t('partner-page:count.star')
                    }`}</Content>
                  </Item>
                </Body>
              )}
            </Skeleton>
          </UserContainer>
        </Col>
      </Row>

      {offer && (
        <DiscussionContainer>
          <HeaderSection>
            <HeaderLabel>
              {t('partner-page:offer-detail.labels.discussion')}
            </HeaderLabel>
          </HeaderSection>
          <Body nopadding>
            <Messager room={offer._id} user={user} />
          </Body>
        </DiscussionContainer>
      )}

      {offer && (
        <EditDrawer
          offer={offer}
          visible={visible}
          onClose={() => setVisible(false)}
          onSubmit={handleEdit}
        />
      )}
    </Wrapper>
  );
};

const mapStateToProps = state => ({
  user: getUser(state),
  offer: getDetail(state),
  isFetchingDetail: getIsFetchingDetail(state)
});

const mapDispatchToProps = dispatch => ({
  fetchDetail: id => dispatch(doFetchDetail(id)),
  updateOffer: (id, data) => dispatch(doUpdateOffer(id, data)),
  clearResult: () => dispatch(doClearResult())
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(OfferDetail);
