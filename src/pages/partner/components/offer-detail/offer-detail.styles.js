import styled from 'styled-components';
import {
  Button as AntButton,
  Divider as AntDivider,
  Row as AntRow,
  Col as AntCol
} from 'antd';

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  padding: 0.75rem;
`;

const OfferContainer = styled.div`
  box-shadow: var(--highlight);
  background-color: var(--white);
  padding: 0.5rem;
  border-radius: 0.5rem;
  margin-bottom: 1rem;
  position: relative;
`;

const HeaderSection = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  padding: 0.5rem 0.25rem;
  align-items: center;
  border-bottom: 1px solid var(--text-em);
`;

const HeaderLabel = styled.div`
  color: var(--text-title);
  font-size: 1.25rem;
  font-weight: 500;
  line-height: 32px;
`;

const ButtonsContainer = styled.div`
  display: flex;
  flex-direction: row;
`;

const Button = styled(AntButton)`
  margin-right: 0.25rem;
`;

const Body = styled.div`
  display: flex;
  flex-direction: column;
  margin-top: 0.5rem;
  padding: ${props => (props.nopadding ? '0' : '0 0.5rem')};
`;

const Item = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  padding: 0.5rem 0;
`;

const Label = styled.span`
  font-size: 1rem;
  font-weight: 500;
  color: var(--text-em);
  white-space: nowrap;
  margin-right: 1rem;

  ::after {
    content: ':';
    font-size: 1rem;
    color: var(--text-em);
  }
`;

const Content = styled.span`
  font-size: 1rem;
  color: var(--text-em);
  word-break: break-word;
`;

const Divider = styled(AntDivider)`
  background-color: var(--text-title);
  margin: 1rem 0 0.5rem 0;
`;

const HotelContainer = styled.div`
  box-shadow: var(--highlight);
  background-color: var(--white);
  padding: 0.5rem;
  border-radius: 0.5rem;
`;

const UserContainer = styled.div`
  box-shadow: var(--highlight);
  background-color: var(--white);
  padding: 0.5rem;
  border-radius: 0.5rem;
`;

const Row = styled(AntRow)`
  @media (min-width: 992px) {
    margin-bottom: 1rem;
  }
`;

const Col = styled(AntCol)`
  margin-bottom: 1rem;
  @media (min-width: 992px) {
    margin-bottom: 0;
  }
`;

const DiscussionContainer = styled.div`
  background-color: var(--white);
  border-radius: 0.5rem;
  padding: 1rem;
  box-shadow: var(--highlight);
  display: flex;
  flex-direction: column;
`;

const OfferOverlay = styled.div`
  position: absolute;
  top: 0;
  bottom: 0;
  left: 0;
  right: 0;
  background-color: ${props =>
    props.type === 'accepted'
      ? 'rgba(23, 162, 184, 0.1)'
      : 'rgba(108, 117, 125, .2)'};
  border-radius: 0.5rem;
  display: ${props => (props.visible ? 'block' : 'none')};
`;

const OverlayContent = styled.div`
  width: 100%;
  height: 100%;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;

  span {
    font-size: 1.25rem;
    font-weight: 600;
    color: ${props =>
      props.type === 'accepted' ? 'var(--success)' : 'var(--text-sub)'};
    opacity: ${props => (props.type === 'accepted' ? '0.7' : '1')};
  }

  svg {
    color: ${props =>
      props.type === 'accepted' ? 'var(--success)' : 'var(--text-sub)'};
    opacity: ${props => (props.type === 'accepted' ? '0.7' : '1')};
  }
`;


export {
  Wrapper,
  OfferContainer,
  HeaderSection,
  HeaderLabel,
  ButtonsContainer,
  Button,
  Body,
  Item,
  Label,
  Content,
  Divider,
  HotelContainer,
  UserContainer,
  Row,
  Col,
  DiscussionContainer,
  OfferOverlay,
  OverlayContent
};
