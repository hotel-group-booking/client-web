import React from 'react';
import { useTranslation } from 'react-i18next';
import { Wrapper, ActivityIcon, Tip } from './empty-list.styles';  

const EmptyList = (props) => {
  const [t] = useTranslation('partner-page');
  return (
    <Wrapper {...props}>
      <ActivityIcon/>
      <span>{t('partner-page:offer empty')}</span>
      <Tip>{t('partner-page:tips.offer empty')}</Tip>
    </Wrapper>
  );
};

export default EmptyList;
