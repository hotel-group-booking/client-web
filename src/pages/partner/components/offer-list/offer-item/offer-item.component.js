import React from 'react';
import { Row } from 'antd';
import { Tag } from 'components';
import moment from 'moment';
import { useTranslation } from 'react-i18next';
import { currencyFormat } from 'lib';
import { withRouter } from 'react-router-dom';
import {
  Wrapper,
  Avatar,
  AvatarSection,
  DescriptionSection,
  Description,
  Created,
  TagSection,
  StatusSection
} from './offer-item.styles';

const OfferItem = ({ offer, history, ...rest }) => {
  const [t] = useTranslation('partner-page');

  const handleClick = e => {
    history.push(`/partner/offer/${offer._id}`);
  };

  return (
    <Wrapper {...rest}>
      <Row onClick={handleClick}>
        <AvatarSection>
          {offer.partner.photo ? (
            <Avatar src={offer.partner.photo.link} />
          ) : (
            <Avatar icon='user' />
          )}
        </AvatarSection>
        <DescriptionSection>
          <Description>RE: {offer.request.description}</Description>
          <Created>{`${t('partner-page:offer-item.created at')} ${moment(
            offer.created
          ).format('D/M/YYYY - LTS')}`}</Created>
          <TagSection>
            <Tag type='primary'>{currencyFormat(offer.budget)}</Tag>
            <Tag>{`${t('partner-page:offer-item.checkin')}: ${moment(
              offer.checkin
            ).format('D/M/YYYY')}`}</Tag>
            <Tag type='secondary'>{`${t('partner-page:offer-item.checkout')}: ${moment(
              offer.checkout
            ).format('D/M/YYYY')}`}</Tag>
          </TagSection>
        </DescriptionSection>
        <StatusSection />
      </Row>
    </Wrapper>
  );
};

export default withRouter(OfferItem);
