import styled from 'styled-components';
import { Avatar as AntAvatar, Col as AntCol } from 'antd';

const Wrapper = styled.div`
  border-radius: 0.25rem;
  padding: 0.5rem;
  display: flex;
  flex-direction: column;
  transition: background-color 0.2s;
  cursor: pointer;

  &:nth-of-type(odd) {
    background-color: #3279790f;
  }

  :hover {
    background-color: #e4eaec;
  }
`;

const Avatar = styled(AntAvatar)`
  margin-top: .2rem;
`;

const AvatarSection = styled(AntCol).attrs(props => ({
  xs: 2
}))`
  display: flex;
  justify-content: center;
`;

const DescriptionSection = styled(AntCol).attrs(props => ({
  xs: 18
}))``;

const StatusSection = styled(AntCol).attrs(props => ({
  xs: 4
}))`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
`;

const Description = styled.div`
  font-weight: 500;
  font-size: .9rem;
  color: var(--text-em);
`;

const Created = styled.div`
  color: var(--text-sub);
`;

const TagSection = styled.div`
  display: flex;
  flex-direction: row;
  white-space: nowrap;
  flex-wrap: wrap;
  margin-top: .5rem;
`;

export {
  Wrapper,
  AvatarSection,
  Avatar,
  DescriptionSection,
  Description,
  Created,
  TagSection,
  StatusSection
};