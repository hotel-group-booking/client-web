import React, { useEffect } from 'react';
import { isEmpty } from 'ramda';
import { connect } from 'react-redux';
import { getUser } from 'reducers/user';
import OfferItem from './offer-item/offer-item.component';
import { getOffers, doFetchPartnerOffers, doClearResult } from 'reducers/offer';
import EmptyList from './empty-list/empty-list.component';
import { Wrapper, Container } from './offer-list.styles';


const OfferList = ({ offers, fetchOffers, user, ...rest }) => {
  useEffect(() => {
    if (user) {
      fetchOffers(user._id, 0);
    }
  }, [user]);

  return (
    <Wrapper {...rest}>
      {isEmpty(offers.values) 
        ? <EmptyList/>
        : 
        <Container>
          {offers.values.map(offer => 
            <OfferItem offer={offer} key={offer._id}/>  
          )}
        </Container>
      }
    </Wrapper>
  );
};

const mapStateToProps = state => ({
  user: getUser(state),
  offers: getOffers(state)
});

const mapDispatchToProps = dispatch => ({
  fetchOffers: (partnerId, page) =>
    dispatch(doFetchPartnerOffers(partnerId, page)),
  clearResult: () => dispatch(doClearResult())
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(OfferList);
