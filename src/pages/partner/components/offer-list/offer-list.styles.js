import styled from 'styled-components';

const Wrapper = styled.div`
  padding: .5rem;
`;

const Container = styled.div`
  display: flex;
  flex-direction: column;
`;

export {
  Wrapper,
  Container
}