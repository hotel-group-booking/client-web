import React from 'react';
import { compose } from 'ramda';
import { connect } from 'react-redux';
import { getUser } from 'reducers/user';
import { doDeletePhoto, getDeletingPhoto } from 'reducers/hotel';
import { XCircle } from 'react-feather';
import { Icon } from 'antd';
import {
  Wrapper,
  HeaderSection,
  Title,
  PhotoSection,
  Photo,
  DeleteButton,
  Loading
} from './photo-library.styles';

const PhotoLibrary = ({ history, user, deletingPhoto, deletePhoto }) => {
  const handleDelete = image => e => {
    deletePhoto(user._id, user.hotel._id, image.id);
  };

  return (
    user && (
      <Wrapper>
        <HeaderSection>
          <Title>Photo Library</Title>
        </HeaderSection>
        <PhotoSection>
          {user.hotel.images.map(image => (
            <Photo
              key={image.id}
              src={image.link}
            >
              <DeleteButton onClick={handleDelete(image)}>
                <XCircle size={24} />
              </DeleteButton>
              {
                image.id === deletingPhoto &&
                <Loading>
                  <Icon type='loading' />
                </Loading>
              }
            </Photo>
          ))}
        </PhotoSection>
      </Wrapper>
    )
  );
};

const mapStateToProps = state => ({
  user: getUser(state),
  deletingPhoto: getDeletingPhoto(state)
});

const mapDispatchToProps = dispatch => ({
  deletePhoto: (userId, hotelId, photoId) =>
    dispatch(doDeletePhoto(userId, hotelId, photoId))
});

export default compose(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )
)(PhotoLibrary);
