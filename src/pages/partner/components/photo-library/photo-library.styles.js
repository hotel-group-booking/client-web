import styled from 'styled-components';

const Wrapper = styled.div`
  background-color: var(--white);
  padding: 1rem;
  border-radius: 0.5rem;
  margin: 0.1rem;
  box-shadow: var(--highlight);
  display: flex;
  flex-direction: column;
`;

const HeaderSection = styled.div`
  border-bottom: 1px solid var(--text-sub);
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  padding-bottom: 0.5rem;
`;

const Title = styled.span`
  font-family: var(--font-baloo);
  font-size: 1.25rem;
  color: var(--text-title);
`;

const PhotoSection = styled.div`
  display: flex;
  flex-direction: row;
  flex-wrap: wrap;
`;

const Photo = styled.div`
  background-image: ${props => props.src && `url(${props.src})`};
  width: 20%;
  position: relative;
  background-size: cover;
  border: 0.25rem solid var(--white);

  ::before {
    content: '';
    display: block;
    padding-top: 100%;
  }
`;

const DeleteButton = styled.div`
  position: absolute;
  top: 0;
  right: 0;
  bottom: 0;
  left: 0;
  display: flex;
  justify-content: flex-end;
  align-items: flex-start;
  padding: 0.3rem;

  :hover {
    background-color: rgba(0, 0, 0, 0.35);
    svg {
      cursor: pointer;
      color: var(--white);
    }
  }
`;

const Loading = styled.div`
  position: absolute;
  top: 0;
  right: 0;
  bottom: 0;
  left: 0;
  display: flex;
  justify-content: center;
  align-items: center;
  padding: 0.3rem;
  background-color: rgba(0, 0, 0, 0.35);

  svg {
    width: 48px;
    height: 48px;
    color: var(--white);
  }
`;

export {
  Wrapper,
  HeaderSection,
  Title,
  PhotoSection,
  Photo,
  DeleteButton,
  Loading
};
