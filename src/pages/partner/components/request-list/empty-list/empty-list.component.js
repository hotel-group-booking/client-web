import React from 'react';
import { Wrapper, ActivityIcon, Tip } from './empty-list.styles';
import { useTranslation } from 'react-i18next';

const EmptyList = props => {

  const [t] = useTranslation('partner-page:request empty');

  return (
    <Wrapper {...props}>
      <ActivityIcon/>
      <span>{t('partner-page:request empty')}</span>
      <Tip>{t('partner-page:tips.request empty')}</Tip>
    </Wrapper>
  )
};

export default EmptyList;
