import styled from 'styled-components';
import { Activity } from 'react-feather';

const Wrapper = styled.div`
  height: 240px;
  background-color: var(--white);
  padding: 0 5rem;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  border-radius: 0.5rem;
  box-shadow: var(--highlight);

  span {
    font-size: 1.2rem;
    color: var(--text-sub);
    font-weight: 600;
    text-align: center;
  }
`;

const ActivityIcon = styled(Activity).attrs(props => ({
  size: 96
}))`
  color: #daf2fe;
`;

const Tip = styled.p`
  font-size: 1rem;
  color: var(--text-sub);
  font-weight: 500;
  text-align: center;
`;

export {
  Wrapper,
  ActivityIcon,
  Tip
};