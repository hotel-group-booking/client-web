import React from 'react';
import { validator } from 'config/validator';
import { useTranslation } from 'react-i18next';
import {
  Wrapper,
  Form,
  FormItem,
  Input,
  RangePicker,
  Row,
  Col,
  ButtonContainer,
  SendButton
} from './offer-form.styles';

const OfferForm = ({
  onSubmit,
  form: { getFieldDecorator, validateFields },
  ...rest
}) => {
  const [t] = useTranslation(['partner-page', 'validator']);

  const handleSubmit = e => {
    e.preventDefault();
    validateFields((err, values) => {
      if (!err) {
        const offer = {
          budget: Number(values.budget),
          rooms: Number(values.rooms),
          checkin: values.time[0].toISOString(),
          checkout: values.time[1].toISOString(),
        }
        onSubmit(offer);
      }
    });
  };

  const rules = validator(t);
  const budget = () =>
    getFieldDecorator('budget', {
      rules: [rules.required]
    })(
      <Input
        type='number'
        placeholder={t('partner-page:offer-form.placeholders.budget')}
        addonAfter='000 VND'
      />
    );

  const rooms = () =>
    getFieldDecorator('rooms', {
      rules: [rules.required]
    })(
      <Input
        type='number'
        placeholder={t('partner-page:offer-form.placeholders.rooms')}
      />
    );

  const time = () =>
    getFieldDecorator('time', {
      rules: [rules.required]
    })(
      <RangePicker
        placeholder={[
          t('partner-page:offer-form.placeholders.checkin'),
          t('partner-page:offer-form.placeholders.checkout')
        ]}
      />
    );

  return (
    <Wrapper>
      <Form onSubmit={handleSubmit}>
        <Row>
          <Col xs={12}>
            <FormItem
              label={t('partner-page:offer-form.labels.nightly budget')}
            >
              {budget()}
            </FormItem>
          </Col>
          <Col xs={12}>
            <FormItem label={t('partner-page:offer-form.labels.rooms')}>
              {rooms()}
            </FormItem>
          </Col>
          <Col xs={24}>
            <FormItem label={t('partner-page:offer-form.labels.time')}>
              {time()}
            </FormItem>
          </Col>
        </Row>

        <ButtonContainer>
          <SendButton>{t('partner-page:offer-form.buttons.send')}</SendButton>
        </ButtonContainer>
      </Form>
    </Wrapper>
  );
};

export default Form.create()(OfferForm);
