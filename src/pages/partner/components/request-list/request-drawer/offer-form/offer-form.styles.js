import styled from 'styled-components';
import {
  Form as AntForm,
  InputNumber as AntInputNumber,
  Input as AntInput,
  DatePicker,
  Row as AntRow,
  Col as AntCol
} from 'antd';
import { Button } from 'components';

const Wrapper = styled.div`
  padding: 0.5rem;
`;

const Form = styled(AntForm)`
  background-color: var(--white);
  border-radius: 0.5rem;
  box-shadow: var(--highlight);
  padding: 1rem 0.5rem;
`;

const FormItem = styled(AntForm.Item)`
  label {
    font-size: 1rem;
    color: var(--text-em);
    font-weight: 500;
  }
`;

const Input = styled(AntInput)`
  width: 100%;
`;

const InputNumber = styled(AntInputNumber)`
  width: 100%;
`;

const RangePicker = styled(DatePicker.RangePicker).attrs(props => ({
  format: 'D/M/YYYY'
}))`
  width: 100%;
`;

const Row = styled(AntRow).attrs(props => ({
  gutter: 24
}))``;

const Col = styled(AntCol)``;

const ButtonContainer = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
`;

const SendButton = styled(Button).attrs(props => ({
  type: 'gradient',
  htmlType: 'submit'
}))`
  text-transform: uppercase;
`;

export {
  Wrapper,
  Form,
  FormItem,
  InputNumber,
  Input,
  RangePicker,
  Row,
  Col,
  ButtonContainer,
  SendButton
};
