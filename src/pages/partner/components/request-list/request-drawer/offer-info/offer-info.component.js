import React from 'react';
import { useTranslation } from 'react-i18next';
import { withRouter } from 'react-router-dom';
import { currencyFormat, getDuration } from 'lib';
import moment from 'moment';
import { RotateSpinner } from 'react-spinners-kit';
import {
  Wrapper,
  Container,
  HeaderSection,
  HeaderLabel,
  ButtonsContainer,
  Button,
  BodySection,
  Item,
  Label,
  Content,
  Divider,
  LoadingContainer,
  LoadingText
} from './offer-info.styles';

const OfferInfo = ({ offer, loading, history, ...rest }) => {
  const [t] = useTranslation('partner-page');

  const handleRedirect = url => e => {
    history.push(url);
  };

  return (
    <Wrapper>
      {loading ? (
        <LoadingContainer>
          <RotateSpinner size={48} color='#c2c6da' />
          <LoadingText>
            {t('partner-page:offer-info.loading.please wait')}
          </LoadingText>
        </LoadingContainer>
      ) : (
        <Container>
          <HeaderSection>
            <HeaderLabel>
              {t('partner-page:offer-info.labels.header')}
            </HeaderLabel>
            <ButtonsContainer>
              <Button
                ghost
                type='primary'
                onClick={handleRedirect(`/partner/offer/${offer._id}`)}
              >
                {t('partner-page:offer-info.buttons.detail')}
              </Button>
            </ButtonsContainer>
          </HeaderSection>

          <BodySection>
            <Item>
              <Label>
                {t('partner-page:offer-info.labels.nightly budget')}
              </Label>
              <Content>{currencyFormat(offer.budget)}</Content>
            </Item>

            <Item>
              <Label>{t('partner-page:offer-info.labels.checkin')}</Label>
              <Content>{moment(offer.checkin).format('D/M/YYYY')}</Content>
            </Item>
            <Item>
              <Label>{t('partner-page:offer-info.labels.checkout')}</Label>
              <Content>{moment(offer.checkout).format('D/M/YYYY')}</Content>
            </Item>

            <Item>
              <Label>{t('partner-page:offer-info.labels.rooms')}</Label>
              <Content>
                {`${offer.rooms} ${
                  offer.rooms > 1
                    ? t('partner-page:count.rooms')
                    : t('partner-page:count.rooms')
                }`}
              </Content>
            </Item>

            <Item>
              <Label>{t('partner-page:offer-info.labels.created at')}</Label>
              <Content>
                {moment(offer.created).format('D/M/YYYY - LTS')}
              </Content>
            </Item>

            <Divider dashed />

            <Item>
              <Label>{t('partner-page:offer-info.labels.total')}</Label>
              <Content>
                {currencyFormat(
                  offer.budget *
                  offer.rooms *
                  getDuration(offer.checkin, offer.checkout)
                )}
              </Content>
            </Item>
          </BodySection>
        </Container>
      )}
    </Wrapper>
  );
};

export default withRouter(OfferInfo);
