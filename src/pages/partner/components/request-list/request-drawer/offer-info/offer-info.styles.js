import styled from 'styled-components';
import { Button as AntButton, Divider as AntDivider } from 'antd';

const Wrapper = styled.div`
  padding: .5rem;
`;

const Container = styled.div`
  background-color: var(--white);
  border-radius: .5rem;
  box-shadow: var(--highlight);
  padding: .5rem;
`;

const HeaderSection = styled.div`
  display: flex;
  flex-direction: row;
  border-bottom: 1px solid var(--text-em);
  padding: .5rem .25rem;
  justify-content: space-between;
  align-items: center;
`;

const HeaderLabel = styled.span`
  color: var(--text-title);
  font-size: 1.25rem;
  font-weight: 500;
  line-height: 32px;
`;

const ButtonsContainer = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-around;
`;

const Button = styled(AntButton)``;

const BodySection = styled.div`
  display: flex;
  flex-direction: column;
  margin-top: .5rem;
  padding: 0 .5rem;
`;

const Item = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  padding: .5rem 0;
`;

const Label = styled.span`
  font-size: 1rem;
  font-weight: 500;
  color: var(--text-em);
  white-space: nowrap;
  margin-right: 1rem;

  ::after {
    content: ':';
    font-size: 1rem;
    color: var(--text-em);
  }
`;

const Content = styled.span`
  font-size: 1rem;
  color: var(--text-em);
  word-break: break-word;
`;

const Divider = styled(AntDivider)`
  background-color: var(--text-title);
  margin: 1rem 0 0.5rem 0;
`;

const LoadingContainer = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  padding: .5rem;
  border-radius: .5rem;
  height: 180px;
`;

const LoadingText = styled.span`
  font-size: 1rem;
  font-weight: 500;
  color: var(--text-sub);
  margin-top: .5rem;
`;

export {
  Wrapper,
  Container,
  HeaderSection,
  HeaderLabel,
  ButtonsContainer,
  Button,
  BodySection,
  Item,
  Label,
  Content,
  Divider,
  LoadingContainer,
  LoadingText
};