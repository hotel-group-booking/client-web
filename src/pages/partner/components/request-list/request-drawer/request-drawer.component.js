import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { useTranslation } from 'react-i18next';
import { Bookmark } from 'react-feather';
import { Tag } from 'components';
import moment from 'moment';
import { budgetFormat } from 'lib';
import { ROOM_TYPES } from 'config/constants';
import { getUser } from 'reducers/user';
import {
  getCreatedOffer,
  doFetchCreatedOffer,
  doCreateOffer,
  getIsCreatingOffer,
  getIsFetchingCreatedOffer
} from 'reducers/offer';
import OfferForm from './offer-form/offer-form.component';
import OfferInfo from './offer-info/offer-info.component';
import {
  Wrapper,
  DrawerContainer,
  Header,
  Description,
  HeaderTopRight,
  Body,
  Item,
  Title,
  Text
} from './request-drawer.styles';

const RequestDrawer = ({
  user,
  request,
  isCreatingOffer,
  isFetchingCreatedoffer,
  createdOffer,
  fetchCreatedOffer,
  createOffer,
  ...rest
}) => {
  const [t] = useTranslation('partner-page');

  const handleOfferSubmit = offer => {
    createOffer(request._id, offer);
  };

  useEffect(() => {
    if (request) {
      fetchCreatedOffer(user._id, request._id);
    }
  }, [request]);

  return (
    <Wrapper {...rest} title={t('partner-page:request-drawer.title')}>
      {request && (
        <DrawerContainer>
          <Header>
            <Tag large>{request.destination.name}</Tag>
            <Description>{request.description}</Description>
            <HeaderTopRight>
              <Bookmark />
            </HeaderTopRight>
          </Header>

          <Body>
            <Item>
              <Title>{t('partner-page:request-drawer.labels.checkin')}</Title>
              <Text>{moment(request.checkin).format('D/M/YYYY')}</Text>
            </Item>
            <Item>
              <Title>{t('partner-page:request-drawer.labels.checkout')}</Title>
              <Text>{moment(request.checkout).format('D/M/YYYY')}</Text>
            </Item>
            <Item>
              <Title>
                {t('partner-page:request-drawer.labels.nightly budget')}
              </Title>
              <Text>{budgetFormat(request.budgetFrom, request.budgetTo)}</Text>
            </Item>
            <Item>
              <Title>{t('partner-page:request-drawer.labels.rooms')}</Title>
              <Text>{request.rooms}</Text>
            </Item>
            <Item>
              <Title>
                {t('partner-page:request-drawer.labels.star rating')}
              </Title>
              <Text>{request.starRating}</Text>
            </Item>
            <Item>
              <Title>{t('partner-page:request-drawer.labels.room type')}</Title>
              <Text>
                {
                  ROOM_TYPES.find(
                    type => type.value === (request.roomType || 1)
                  ).name
                }
              </Text>
            </Item>
            <Item>
              <Title>
                {t('partner-page:request-drawer.labels.group type')}
              </Title>
              <Text>{request.groupType}</Text>
            </Item>
          </Body>
          {createdOffer || isFetchingCreatedoffer ? (
            <OfferInfo offer={createdOffer} loading={isFetchingCreatedoffer}/>
          ) : (
            <OfferForm onSubmit={handleOfferSubmit} />
          )}
        </DrawerContainer>
      )}
    </Wrapper>
  );
};

const mapStateToProps = state => ({
  user: getUser(state),
  createdOffer: getCreatedOffer(state),
  isCreatingOffer: getIsCreatingOffer(state),
  isFetchingCreatedOffer: getIsFetchingCreatedOffer(state)
});

const mapDispatchToProps = dispatch => ({
  fetchCreatedOffer: (partnerId, requestId) =>
    dispatch(doFetchCreatedOffer(partnerId, requestId)),
  createOffer: (requestId, offer) =>
    dispatch(doCreateOffer({ ...offer, request: requestId }))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(RequestDrawer);
