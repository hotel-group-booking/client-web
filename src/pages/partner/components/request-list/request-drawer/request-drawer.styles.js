import styled from 'styled-components';
import { min } from 'ramda';
import { Drawer as AntDrawer } from 'antd';

const Wrapper = styled(AntDrawer).attrs(props => ({
  placement: 'right',
  width: `${min(window.innerWidth, 520)}px`
}))`
  .ant-drawer-body {
    padding: 0;
  }

  .ant-drawer-wrapper-body {
    background-color: var(--background-primary);
    /* overflow: hidden !important; */
  }
`;

const DrawerContainer = styled.div`
  display: flex;
  flex-direction: column;
  height: 94vh;
`;

const Header = styled.div`
  background-color: var(--text-em);
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  color: var(--white);
  padding: 2rem 1rem;
  margin-bottom: 0.5rem;
  position: relative;
`;

const Description = styled.div`
  margin-top: 1.25rem;
  font-family: var(--font-baloo);
  font-size: 1rem;
  text-align: center;
`;

const HeaderTopRight = styled.div`
  position: absolute;
  top: 0.25rem;
  right: 0.5rem;
  display: flex;
  flex-direction: column;
  cursor: pointer;

  svg {
    margin-bottom: 0.2rem;
  }
`;

const Body = styled.div`
  display: flex;
  flex-direction: column;
  color: var(--text-em);
  padding: 0.5rem 1.5rem;
  border-top: 1px dotted var(--text-em);
  border-bottom: 1px dotted var(--text-em);
`;

const Item = styled.div`
  display: flex;
  flex-direction: row;
  color: var(--text-em);
  font-size: 1rem;
  margin: 0.2rem;
`;

const Title = styled.span`
  font-weight: 500;
`;

const Text = styled.span`
  margin-left: auto;
`;

export {
  Wrapper,
  DrawerContainer,
  Header,
  Description,
  HeaderTopRight,
  Body,
  Item,
  Title,
  Text
};
