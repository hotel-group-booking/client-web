import React, { useState, useEffect } from 'react';
import { compose, omit, range, filter } from 'ramda';
import { connect } from 'react-redux';
import { getUser } from 'reducers/user';
import { validator } from 'config/validator';
import { useTranslation } from 'react-i18next';
import { ROOM_TYPES, GROUP_TYPES } from 'config/constants';
import {
  Wrapper,
  Header,
  Name,
  ExpandedButton,
  Form,
  FormItem,
  Row,
  Col,
  Input,
  InputNumber,
  Select,
  Option,
  Slider,
  ButtonContainer,
  FilterButton
} from './request-filter.styles';

const RequestFilter = ({
  onChange,
  user,
  form: { getFieldDecorator, validateFields, setFieldsValue },
  ...rest
}) => {
  const [visible, setVisible] = useState();
  const [term, setTerm] = useState();

  const [t] = useTranslation(['partner-page', 'validator']);

  useEffect(() => {
    onChange(term);
  }, [term]);

  useEffect(() => {
    if (user) {
      setTerm({
        ...omit(['name', 'address', 'totalRoom', 'images', 'extraInfo'])(
          user.hotel
        ),
        rooms: user.hotel.totalRoom,
        roomType: 1
      });
    }
  }, [user]);
  
  const handleSubmit = e => {
    e.preventDefault();
    validateFields((err, values) => {
      if (!err) {
        setTerm({
          ...term,
          ...filter(Boolean)(values),
          budgetFrom: Number(values.budgetFrom),
          budgetTo: Number(values.budgetTo),
        });
      }
    })
  };

  const handleFilterToggle = e => {
    setVisible(!visible);
  };

  const handleSliderChange = values => {
    setFieldsValue({
      budgetFrom: values[0],
      budgetTo: values[1]
    });
  };

  const rules = validator(t);
  const rooms = () =>
    getFieldDecorator('rooms', {
      rules: [rules.required],
      initialValue: term && term.rooms
    })(<InputNumber min={1} max={100} />);

  const starRating = () =>
    getFieldDecorator('starRating', {
      rules: [rules.required],
      initialValue: term && term.starRating
    })(
      <Select>
        {range(1, 6).map(star => (
          <Option key={star} value={star}>
            <span>
              {`${star} ${
                star > 1
                  ? t('partner-page:count.star')
                  : t('partner-page:count.stars')
              }`}
            </span>
          </Option>
        ))}
      </Select>
    );

  const roomType = () =>
    getFieldDecorator('roomType', {
      rules: [rules.required],
      initialValue: term && term.roomType
    })(
      <Select>
        {ROOM_TYPES.map(type => (
          <Option key={type.value} value={type.value}>
            {type.name}
          </Option>
        ))}
      </Select>
    );

  const groupType = () =>
    getFieldDecorator('groupType')(
      <Select
        placeholder={t('partner-page:request-filter.placeholders.group type')}
      >
        {GROUP_TYPES.map(type => (
          <Option key={type} value={type}>
            {type}
          </Option>
        ))}
      </Select>
    );

  const budgetFrom = () => getFieldDecorator('budgetFrom', {
    rules: [rules.required],
    initialValue: term && term.budgetFrom
  })(<Input addonAfter='000 VND'/>);

  const budgetTo = () => getFieldDecorator('budgetTo', {
    rules: [rules.required],
    initialValue: term && term.budgetTo
  })(<Input addonAfter='000 VND'/>);

  return (
    <Wrapper {...rest}>
      <Header>
        <Name>{t('partner-page:request-filter.name')}</Name>
        <ExpandedButton expanded={visible} onClick={handleFilterToggle} />
      </Header>
      <Form onSubmit={handleSubmit} visible={visible}>
        <Row>
          <Col xs={12} lg={6}>
            <FormItem label={t('partner-page:request-filter.labels.rooms')}>
              {rooms()}
            </FormItem>
          </Col>

          <Col xs={12} lg={6}>
            <FormItem
              label={t('partner-page:request-filter.labels.star rating')}
            >
              {starRating()}
            </FormItem>
          </Col>

          <Col xs={12} lg={6}>
            <FormItem label={t('partner-page:request-filter.labels.room type')}>
              {roomType()}
            </FormItem>
          </Col>

          <Col xs={12} lg={6}>
            <FormItem
              label={t('partner-page:request-filter.labels.group type')}
            >
              {groupType()}
            </FormItem>
          </Col>
        </Row>
        
        <Row>
          <Col xs={24} lg={6}>
            <FormItem
              mb='0'
              label={t('partner-page:request-filter.labels.nightly budget')}
              colon={false}
              required
            >
              {budgetFrom()}
            </FormItem>
            <FormItem>
              {budgetTo()}
            </FormItem>
          </Col>

          <Col xs={24} lg={18}>
            <Slider
              onSliderChange={handleSliderChange}
            />
          </Col>
        </Row>

        <ButtonContainer>
          <FilterButton>
            {t('partner-page:request-filter.buttons.filter')}
          </FilterButton>
        </ButtonContainer>
      </Form>
    </Wrapper>
  );
};

const mapStateToProps = state => ({
  user: getUser(state)
});

export default compose(
  connect(mapStateToProps),
  Form.create()
)(RequestFilter);
