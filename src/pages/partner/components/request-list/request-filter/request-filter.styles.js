import styled from 'styled-components';
import {
  Form as AntForm,
  Input as AntInput,
  InputNumber as AntInputNumber,
  Row as AntRow,
  Col as AntCol,
  Icon,
  Select as AntSelect
} from 'antd';
import { BudgetSlider, Button } from 'components';

const Wrapper = styled.div`
  background-color: var(--white);
  padding: 1rem;
  border-radius: 0.5rem;
  box-shadow: var(--highlight);

  @media (min-width: 576px) {
    padding: 1rem 2rem;
  }
`;

const Header = styled.div`
  display: flex;
  justify-content: space-between;
  margin-bottom: 0.5rem;
  border-bottom: 1px solid var(--text-title);
`;

const Name = styled.span`
  font-size: 1.2rem;
  font-family: var(--font-baloo);
  color: var(--text-em);
`;

const ExpandedButton = styled(Icon).attrs(props => ({
  type: 'up'
}))`
  font-size: 1.25rem;
  cursor: pointer;
  transition: all 0.2s;
  transform: ${props => (props.expanded ? 'rotate(0deg)' : 'rotate(180deg)')};

  svg {
    color: var(--text-em);
  }
`;

const Form = styled(AntForm)`
  display: ${props => (props.visible ? 'block' : 'none')};
`;

const FormItem = styled(AntForm.Item)`
  margin-bottom: ${props => props.mb || '24px'};

  label {
    font-size: 1rem;
    color: var(--text-em);
    font-weight: 500;
  }
`;

const Row = styled(AntRow).attrs(props => ({
  gutter: 24
}))``;

const Col = styled(AntCol)``;

const Input = styled(AntInput)`
  width: 100%;
`;

const InputNumber = styled(AntInputNumber)`
  width: 100%;
`;

const Select = styled(AntSelect)`
  width: 100%;
`;

const Option = styled(AntSelect.Option)``;

const Slider = styled(BudgetSlider)`
  margin-bottom: 0.5rem;
  @media (min-width: 992px) {
    margin-top: 46px;
    padding: 0 4rem;
  }
`;

const ButtonContainer = styled.div`
  display: flex;
  justify-content: center;
  margin-top: 1.5rem;
`;

const FilterButton = styled(Button).attrs(props => ({
  type: 'gradient',
  htmlType: 'submit'
}))`
  font-family: var(--font-baloo);
  text-transform: uppercase;
`;

export {
  Wrapper,
  Header,
  Name,
  ExpandedButton,
  Form,
  FormItem,
  Row,
  Col,
  Input,
  InputNumber,
  Select,
  Option,
  ButtonContainer,
  FilterButton,
  Slider
};
