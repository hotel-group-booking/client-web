import React from 'react';
import { useTranslation } from 'react-i18next';
import moment from 'moment';
import { Mail } from 'react-feather';
import {
  Wrapper,
  Row,
  AvatarSection,
  Avatar,
  DescriptionSection,
  Description,
  Creator,
  TagsSection,
  Tag,
  StatusSection,
  Status
} from './request-item.styles';

const RequestItem = ({ request, onSelect, ...rest }) => {
  const [t] = useTranslation('partner-page');

  const budgetFormat = (budgetFrom, budgetTo) => {
    const from = Number(budgetFrom * 1000).toLocaleString('vi-VN', {
      style: 'currency',
      currency: 'VND'
    });
    const to = Number(budgetTo * 1000).toLocaleString('vi-VN', {
      style: 'currency',
      currency: 'VND'
    });
    return `${from} - ${to}`;
  };

  const handleClick = e => {
    onSelect(request);
  };

  return (
    <Wrapper {...rest}>
      <Row onClick={handleClick}>
        <AvatarSection>
          {request.member.photo ? (
            <Avatar src={request.member.photo.link} />
          ) : (
            <Avatar icon='user' />
          )}
        </AvatarSection>
        <DescriptionSection>
          <Description>{request.description || 'No Description'}</Description>
          <Creator>
            {`${t('member-page:created by')} ${request.member.displayName}`}
          </Creator>
          <TagsSection>
            <Tag type='primary'>{request.destination.name}</Tag>
            <Tag>{budgetFormat(request.budgetFrom, request.budgetTo)}</Tag>
            <Tag type='secondary'>
              {moment(request.created).format('D/M/YYYY - LTS')}
            </Tag>
          </TagsSection>
        </DescriptionSection>
        <StatusSection>
          <Status>
            <Mail />
            <span>
              {request.offerCount}{' '}
              {request.offerCount > 1
                ? t('member-page:offer')
                : t('member-page:offers')}
            </span>
          </Status>
        </StatusSection>
      </Row>
    </Wrapper>
  );
};

export default RequestItem;
