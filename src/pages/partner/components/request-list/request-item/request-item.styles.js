import styled from 'styled-components';
import { Row as AntRow, Col as AntCol, Avatar as AntAvatar } from 'antd';


const Wrapper = styled.div`
  border-radius: 0.25rem;
  padding: 0.5rem;
  display: flex;
  flex-direction: column;
  transition: background-color 0.2s;
  cursor: pointer;

  &:nth-of-type(odd) {
    background-color: #3279790f;
  }

  :hover {
    background-color: #e4eaec;
  }
`;


const Row = styled(AntRow).attrs(props => ({
  gutter: 0
}))``;

const Col = styled(AntCol)``;

const AvatarSection = styled(AntCol).attrs(props => ({
  xs: 2
}))`
  display: flex;
  justify-content: center;
`;

const Avatar = styled(AntAvatar)`
  margin-top: 0.2rem;
`;

const DescriptionSection = styled(AntCol).attrs(props => ({
  xs: 18
}))``;

const Description = styled.div`
  font-weight: 500;
  font-size: 0.9rem;
  color: var(--text-em);
`;

const Creator = styled.div`
  color: var(--text-sub);
`;

const TagsSection = styled.div`
  display: flex;
  flex-direction: row;
  white-space: nowrap;
  flex-wrap: wrap;
`;

const Tag = styled.span`
  background-color: ${props => {
    switch (props.type) {
      case 'primary':
        return '#005b6d';
      case 'secondary':
        return 'rgb(224, 56, 143)';
      default:
        return 'rgb(61, 164, 211)';
    }
  }};
  color: var(--white);
  font-size: ${props => (props.large ? '1rem' : '.75rem')};
  padding: ${props => (props.large ? '.25rem 1rem' : '0 .25rem')};
  border-radius: ${props => (props.large ? '.3rem' : '.2rem')};
  margin-right: 0.5rem;
  margin-top: .5rem;
`;

const StatusSection = styled(AntCol).attrs(props => ({
  xs: 4
}))`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
`;

const Status = styled.div`
  display: flex;
  width: fit-content;
  padding: 0.25rem;
  border: 0.5px solid transparent;
  border-radius: 0.3rem;
  transition: border-color 0.2s, background-color 0.2s;
  margin-bottom: 0.1rem;
  span {
    margin-left: 0.5rem;
  }

  :hover {
    border-color: var(--text-em);
    background-color: var(--white);
  }
`;


export {
  Wrapper,
  Row,
  Col,
  AvatarSection,
  Avatar,
  DescriptionSection,
  Description,
  Creator,
  TagsSection,
  Tag,
  StatusSection,
  Status
};
