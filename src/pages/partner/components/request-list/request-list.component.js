import React, { useEffect, useState } from 'react';
import RequestFilter from './request-filter/request-filter.component';
import EmptyList from './empty-list/empty-list.component';
import RequestItem from './request-item/request-item.component';
import RequestDrawer from './request-drawer/request-drawer.component';
import { isEmpty } from 'ramda';
import { connect } from 'react-redux';
import {
  getList,
  getIsSearchingRequest,
  getSelected,
  doSearchRequest,
  doSelect
} from 'reducers/request';
import { Wrapper, Container } from './request-list.styles';

const RequestList = ({
  list,
  isSearchingRequest,
  searchRequest,
  selected,
  select,
  ...rest
}) => {
  const [requests, setRequests] = useState([]);
  const [visible, setVisible] = useState();

  useEffect(() => {
    setRequests(list.values);
  }, [list]);

  const handleFilterChange = term => {
    if (term) {
      searchRequest(0, term);
    }
  };

  const handleRequestSelect = request => {
    select(request);
    setVisible(true);
  };

  return (
    <Wrapper {...rest}>
      <RequestFilter onChange={handleFilterChange} />
      <Container>
        {isEmpty(list.values) && !isSearchingRequest ? (
          <EmptyList />
        ) : (
          requests.map(request => (
            <RequestItem
              key={request._id}
              request={request}
              onSelect={handleRequestSelect}
            />
          ))
        )}
      </Container>
      <RequestDrawer
        visible={visible}
        onClose={() => setVisible(false)}
        request={selected}
      />
    </Wrapper>
  );
};

const mapStateToProps = state => ({
  list: getList(state),
  isSearchingRequest: getIsSearchingRequest(state),
  selected: getSelected(state)
});

const mapDispatchToProps = dispatch => ({
  searchRequest: (page, term) => dispatch(doSearchRequest(page, term)),
  select: request => dispatch(doSelect(request))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(RequestList);
