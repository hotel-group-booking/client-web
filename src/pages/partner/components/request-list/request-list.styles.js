import styled from 'styled-components';

const Wrapper = styled.div`
  padding: .5rem;
`;

const Container = styled.div`
  margin-top: 1rem;
`;

export {
  Wrapper,
  Container
};