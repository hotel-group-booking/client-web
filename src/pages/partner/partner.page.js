import React from 'react';
import { connect } from 'react-redux';
import { getUser } from 'reducers/user';
import { ApplicationNavigator } from 'components';
import { Switch, Route } from 'react-router-dom';
import {
  Menu,
  RequestList,
  OfferDetail,
  HotelSettings,
  PhotoLibrary,
  OfferList
} from './components';
import { Wrapper, MainSection, Row, Col } from './partner.styles';

const PartnerPage = ({ history, user, ...rest }) => {
  return (
    <>
      {user && (
        <Wrapper {...rest}>
          <ApplicationNavigator>
            <Menu />
          </ApplicationNavigator>

          <MainSection>
            <Row>
              <Col xs={0} md={6} lg={5}>
                <Menu standalone />
              </Col>
              <Col xs={24} md={18} lg={19}>
                <Switch>
                  <Route exact path='/partner' component={RequestList} />
                  <Route path='/partner/requests' component={RequestList} />
                  <Route path='/partner/offers' component={OfferList} />
                  <Route path='/partner/offer/:id' component={OfferDetail} />
                  <Route
                    path='/partner/hotel-settings'
                    component={HotelSettings}
                  />
                  <Route
                    path='/partner/photo-library'
                    component={PhotoLibrary}
                  />
                </Switch>
              </Col>
            </Row>
          </MainSection>
        </Wrapper>
      )}
    </>
  );
};

const mapStateToProps = state => ({
  user: getUser(state)
});

export default connect(mapStateToProps)(PartnerPage);
