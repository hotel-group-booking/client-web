import styled from 'styled-components';
import { Row as AntRow, Col as AntCol } from 'antd';

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  min-height: 100vh;
  overflow-x: hidden;
`;

const Row = styled(AntRow).attrs(props => ({
  gutter: 0
}))``;

const Col = styled(AntCol)``;

const MainSection = styled.div`
  width: 100%;
  max-width: 1100px;
  margin: 0 auto;
  margin-top: 1rem;
  
  @media (min-width: 768px) {
    margin-top: 2rem;
  }
`;

export {
  Wrapper,
  Row,
  Col,
  MainSection
};
