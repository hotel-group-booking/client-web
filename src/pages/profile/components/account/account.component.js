import React from 'react';
import { compose } from 'ramda';
import { connect } from 'react-redux';
import { getUser, getIsUpdating, doUpdate } from 'reducers/user';
import { validator } from 'config/validator';
import { useTranslation } from 'react-i18next';
import {
  Wrapper,
  Form,
  FormItem,
  Input,
  Row,
  Col,
  Button
} from './account.styles';

const Account = ({
  form: { getFieldDecorator, validateFields },
  user,
  isUpdating,
  update,
  ...rest
}) => {
  const [t] = useTranslation(['profile-page', 'validator']);

  const handleSubmit = e => {
    e.preventDefault();
    validateFields((err, values) => {
      if (!err) {
        update(user._id, values);
      }
    });
  };

  const rules = validator(t);
  const firstName = () =>
    getFieldDecorator('firstName', {
      rules: [rules.required],
      initialValue: user.username
    })(<Input disabled={isUpdating}/>);

  const lastName = () =>
    getFieldDecorator('lastName', {
      rules: [rules.required],
      initialValue: user.lastName
    })(<Input disabled={isUpdating}/>);

  const displayName = () =>
    getFieldDecorator('displayName', {
      rules: [rules.minLength, rules.maxLength],
      initialValue: user.displayName
    })(<Input disabled={isUpdating}/>);

  const phone = () =>
    getFieldDecorator('phone', {
      rules: [rules.required],
      initialValue: user.phone
    })(<Input disabled={isUpdating}/>);

  const address = () =>
    getFieldDecorator('address', {
      rules: [rules.required],
      initialValue: user.address
    })(<Input disabled={isUpdating}/>);

  const email = () =>
    getFieldDecorator('email', {
      rules: [rules.required],
      initialValue: user.email
    })(<Input disabled />);

  return (
    <Wrapper {...rest}>
      <Form
        labelCol={{ xs: 7 }}
        wrapperCol={{ xs: 17 }}
        labelAlign='left'
        onSubmit={handleSubmit}
      >
        <FormItem label={t('profile-page:account.labels.first name')}>
          {firstName()}
        </FormItem>
        <FormItem label={t('profile-page:account.labels.last name')}>
          {lastName()}
        </FormItem>
        <FormItem label={t('profile-page:account.labels.display name')}>
          {displayName()}
        </FormItem>
        <FormItem label={t('profile-page:account.labels.phone')}>
          {phone()}
        </FormItem>
        <FormItem label={t('profile-page:account.labels.address')}>
          {address()}
        </FormItem>
        <FormItem label={t('profile-page:account.labels.email')}>
          {email()}
        </FormItem>
        <Row>
          <Col sm={{ push: 7 }}>
            <Button type='gradient' htmlType='submit'>
              {t('profile-page:account.buttons.save')}
            </Button>
          </Col>
        </Row>
      </Form>
    </Wrapper>
  );
};

const mapStateToProps = state => ({
  user: getUser(state),
  isUpdating: getIsUpdating(state)
});

const mapDispatchToProps = dispatch => ({
  update: (userId, data) => dispatch(doUpdate(userId, data))
});

export default compose(
  connect(mapStateToProps, mapDispatchToProps),
  Form.create()
)(Account);
