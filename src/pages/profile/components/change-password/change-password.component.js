import React, { useEffect, useState } from 'react';
import { compose } from 'ramda';
import { connect } from 'react-redux';
import {
  getUser,
  getResult,
  getIsChangingPassword,
  doChangePassword,
  doClearResult
} from 'reducers/user';
import { useTranslation } from 'react-i18next';
import { validator } from 'config/validator';
import { toast } from 'react-toastify';
import {
  Wrapper,
  Form,
  Input,
  Row,
  Col,
  SaveButton,
  FormItem
} from './change-password.styles';

const ChangePassword = ({
  form: { getFieldDecorator, validateFields, getFieldValue, resetFields },
  user,
  isChangingPassword,
  result,
  changePassword,
  clearResult,
  ...rest
}) => {
  const [isSaved, setIsSaved] = useState(false);
  const [t] = useTranslation(['profile-page', 'validator']);

  useEffect(() => {
    return () => {
      clearResult();
    };
  }, []);

  useEffect(() => {
    if (isSaved && result && result.type === 'success' && isSaved) {
      toast.success(
        t('profile-page:change-password.results.password changed'),
        {
          autoClose: 2000,
          hideProgressBar: true
        }
      );
    }
    if (isSaved && result && result.type === 'error') {
      toast.error(
        t(`profile-page:change-password.results.${result.message}`),
        {
          autoClose: 5000,
          hideProgressBar: false
        }
      );
    }
  }, [result, isSaved]);

  const rules = validator(t);

  const handleSubmit = e => {
    e.preventDefault();
    validateFields((err, values) => {
      if (!err) {
        changePassword(user._id, values);
        resetFields();
        setIsSaved(true);
      }
    });
  };

  const currentPassword = () =>
    getFieldDecorator('currentPassword', {
      rules: [rules.required]
    })(
      <Input
        type='password'
        disabled={isChangingPassword}
        placeholder={t(
          'profile-page:change-password.placeholders.current password'
        )}
      />
    );

  const password = () =>
    getFieldDecorator('password', {
      rules: [rules.required, rules.minLength, rules.maxLength]
    })(
      <Input
        type='password'
        disabled={isChangingPassword}
        placeholder={t('profile-page:change-password.placeholders.password')}
      />
    );

  const passwordConfirm = props =>
    getFieldDecorator('passowrdConfirm', {
      rules: [
        rules.required,
        rules.confirm(
          getFieldValue('password')
        )
      ]
    })(
      <Input
        type='password'
        disabled={isChangingPassword}
        placeholder={t(
          'profile-page:change-password.placeholders.password confirm'
        )}
      />
    );

  return (
    <Wrapper {...rest}>
      <Form
        labelCol={{ xs: 8 }}
        wrapperCol={{ xs: 16, lg: 8 }}
        onSubmit={handleSubmit}
        labelAlign='left'
      >
        <FormItem
          label={t('profile-page:change-password.labels.current password')}
        >
          {currentPassword()}
        </FormItem>
        <FormItem label={t('profile-page:change-password.labels.password')}>
          {password()}
        </FormItem>
        <FormItem
          label={t('profile-page:change-password.labels.password confirm')}
        >
          {passwordConfirm()}
        </FormItem>

        <Row>
          <Col sm={{ push: 8 }}>
            <SaveButton>
              {t('profile-page:change-password.buttons.update')}
            </SaveButton>
          </Col>
        </Row>
      </Form>
    </Wrapper>
  );
};

const mapStateToProps = state => ({
  user: getUser(state),
  result: getResult(state),
  isChangingPassword: getIsChangingPassword(state)
});

const mapDispatchToProps = dispatch => ({
  changePassword: (userId, data) => dispatch(doChangePassword(userId, data)),
  clearResult: () => dispatch(doClearResult())
});

export default compose(
  connect(
    mapStateToProps,
    mapDispatchToProps
  ),
  Form.create()
)(ChangePassword);
