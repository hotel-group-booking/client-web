import styled from 'styled-components';
import {
  Form as AntForm,
  Row as AntRow,
  Col as AntCol,
  Input as AntInput
} from 'antd';
import { Button } from 'components';

const Wrapper = styled.div`
  padding: 1.5rem 1rem;
`;

const Form = styled(AntForm)`
  label {
    font-size: 1rem;
    color: var(--text-em);
    font-weight: 500;
  }
`;

const FormItem = styled(AntForm.Item)``;

const Row = styled(AntRow).attrs(props => ({
  gutter: 0
}))``;

const Col = styled(AntCol)``;

const SaveButton = styled(Button).attrs(props => ({
  type: 'gradient',
  htmlType: 'submit'
}))``;

const Input = styled(AntInput)`
  width: 100%;
`;

export { Wrapper, Form, FormItem, Row, Col, SaveButton, Input };
