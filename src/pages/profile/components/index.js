export { default as Menu } from './menu/menu.component';
export { default as ProfileHeader } from './profile-header/profile-header.component';
export { default as Account } from './account/account.component';
export { default as ChangePassword } from './change-password/change-password.component';