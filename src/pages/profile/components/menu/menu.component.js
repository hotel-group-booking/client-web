import React from 'react';
import { connect } from 'react-redux';
import { compose } from 'ramda';
import { Info, Key, Settings } from 'react-feather';
import { useTranslation } from 'react-i18next';
import { withRouter } from 'react-router-dom';
import { getUser } from 'reducers/user';
import { Wrapper, HeaderSection, BodySection, Item } from './menu.styles';

const Menu = ({standalone, history, user, ...rest}) => {

  const [t] = useTranslation('profile-page');

  const handleRedirect = url => e => {
    history.push(url);
  };

  return (
    <Wrapper {...rest}>
      {
        !standalone && 
        <HeaderSection onClick={handleRedirect(`/${user.role}`)}>
          <span>Hotel Group Booking</span>
        </HeaderSection>
      }
      <BodySection>
        <Item onClick={handleRedirect('/profile/account')}>
          <Info size={16}/>
          <span>{t('profile-page:menu.account')}</span>
        </Item>
        <Item onClick={handleRedirect('/profile/change-password')}>
          <Key size={16}/>
          <span>{t('profile-page:menu.change password')}</span>
        </Item>
        <Item onClick={handleRedirect('/profile/settings')}>
          <Settings size={16}/>
          <span>{t('profile-page:menu.settings')}</span>
        </Item>
      </BodySection>
    </Wrapper>
  );
};

const mapStateToProps = state => ({
  user: getUser(state)
});

export default compose(
  connect(mapStateToProps),
  withRouter
)(Menu);
