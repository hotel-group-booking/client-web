import styled from 'styled-components';

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
`;

const HeaderSection = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  height: 3rem;
  border-bottom: 1px solid var(--text-em);
  font-weight: 500;
  color: #669199;
  cursor: pointer;

  :hover {
    color: var(--text-title);
  }
`;

const BodySection = styled.div`
  padding: 1rem;
  display: flex;
  flex-direction: column;
`;

const Item = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  font-size: .9rem;
  margin-top: .5rem;
  color: #669199;
  font-weight: 500;
  cursor: pointer;
  transition: color .2s;

  :hover {
    color: var(--text-em);
  }

  svg {
    margin-right: .5rem;
  }
`;


export {
  Wrapper,
  HeaderSection,
  BodySection,
  Item
}
