import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { getUser, doUploadAvatar, getIsUploadingAvatar } from 'reducers/user';
import { useTranslation } from 'react-i18next';
import { PlusCircle } from 'react-feather';
import moment from 'moment';
import { base64Reader } from 'lib';
import { Icon } from 'antd';
import {
  Wrapper,
  Container,
  Row,
  Col,
  AvatarSection,
  Avatar,
  InformationSection,
  Username,
  JoinDate,
  Uploader,
  UploaderLabel,
  LoadingContainer
} from './profile-header.styles';

const ProfileHeader = ({ user, uploadAvatar, isUploadingAvatar, ...rest }) => {
  const [loading, setLoading] = useState(false);
  const [t] = useTranslation('profile-page');

  useEffect(() => {
    setLoading(isUploadingAvatar);
  }, [isUploadingAvatar]);

  const handleFileChange = async e => {
    setLoading(true);
    const file = e.target.files[0];
    const base64Img = await base64Reader(200)(file);
    uploadAvatar(user._id, base64Img);
  };

  return (
    <Wrapper>
      <Container>
        <Row>
          <Col xs={24} md={6}>
            <AvatarSection>
              {user.photo ? (
                <Avatar src={user.photo.link} />
              ) : (
                <Avatar icon='user' />
              )}
              <Uploader>
                {loading ? (
                  <LoadingContainer>
                    <Icon type='loading' />
                  </LoadingContainer>
                ) : (
                  <UploaderLabel htmlFor='avatar-input'>
                    <PlusCircle size={48} />
                  </UploaderLabel>
                )}
                <input
                  type='file'
                  hidden
                  id='avatar-input'
                  onChange={handleFileChange}
                />
              </Uploader>
            </AvatarSection>
          </Col>
          <Col xs={24} md={18}>
            <InformationSection>
              <Username>{`${user.username} (${user.displayName})`}</Username>
              <JoinDate>{`${t('profile-page:joined')} ${moment(
                user.created
              ).fromNow()}`}</JoinDate>
            </InformationSection>
          </Col>
        </Row>
      </Container>
    </Wrapper>
  );
};

const mapStateToProps = state => ({
  user: getUser(state),
  isUploadingAvatar: getIsUploadingAvatar(state)
});

const mapDispatchToProps = dispatch => ({
  uploadAvatar: (userId, image) => dispatch(doUploadAvatar(userId, image))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ProfileHeader);
