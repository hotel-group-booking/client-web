import styled from 'styled-components';
import { Row as AntRow, Col as AntCol, Avatar as AntAvatar } from 'antd';

const Wrapper = styled.div`
  width: 100%;
  background-color: var(--text-em);
  padding: 2rem 1rem;

  @media (min-width: 768px) {
    padding: 1rem;
    height: 280px;
  }
`;

const Container = styled.div`
  width: 100%;
  height: 100%;
  max-width: 1100px;
  margin: 0 auto;
`;

const Row = styled(AntRow).attrs(props => ({
  gutter: 0
}))`
  height: 100%;
`;

const Col = styled(AntCol)`
  height: 100%;
`;

const AvatarSection = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  position: relative;

  @media (min-width: 768px) {
    height: 100%;
  }
`;

const Avatar = styled(AntAvatar).attrs(props => ({
  size: 120
}))`
  border: 4px solid var(--white);
`;

const Uploader = styled.div`
  position: absolute;
  top: 0;
  bottom: 0;
  right: 0;
  left: 0;
  display: flex;
  justify-content: center;
  align-items: center;
`;

const UploaderLabel = styled.label`
  width: 120px;
  height: 120px;
  display: flex;
  justify-content: center;
  align-items: center;
  border-radius: 50%;
  border: 4px solid transparent;
  cursor: pointer;

  svg {
    color: rgba(255, 255, 255, 0.3);
  }

  :hover {
    background-color: rgba(0, 0, 0, .3);
    border-color: var(--white);

    svg {
      color: rgba(255, 255, 255, 1);
    }
  }
`;

const InformationSection = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;

  @media (min-width: 768px) {
    align-items: flex-start;
    height: 100%;
  }
`;

const Username = styled.span`
  font-size: 1.5rem;
  font-family: var(--font-baloo);
  color: var(--white);
  margin-top: 1rem;
`;

const JoinDate = styled.span`
  color: var(--white);
  font-weight: 500;
`;

const LoadingContainer = styled.label`
  width: 120px;
  height: 120px;
  display: flex;
  justify-content: center;
  align-items: center;
  border-radius: 50%;
  border: 4px solid var(--white);

  svg {
    width: 48px;
    height: 48px;
    color: var(--white);
  }
`;

export {
  Wrapper,
  Container,
  Row,
  Col,
  AvatarSection,
  Avatar,
  InformationSection,
  Username,
  JoinDate,
  Uploader,
  UploaderLabel,
  LoadingContainer
};
