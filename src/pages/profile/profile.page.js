import React from 'react';
import { connect } from 'react-redux';
import { getUser } from 'reducers/user';
import { Switch, Route } from 'react-router-dom';
import { ApplicationNavigator } from 'components';
import { Menu, ProfileHeader, Account, ChangePassword } from './components';
import { Wrapper, MainSection, Row, Col } from './profile.styles';

const ProfilePage = ({ history, user, ...rest }) => {
  return (
    <>
      {user && (
        <Wrapper {...rest}>
          <ApplicationNavigator>
            <Menu />
          </ApplicationNavigator>

          <Row>
            <Col xs={24}>
              <ProfileHeader />
            </Col>
          </Row>
          
          <MainSection>
            <Row>
              <Col xs={0} md={6} lg={5}>
                <Menu standalone />
              </Col>
              <Col xs={24} md={18} lg={19}>
                <Switch>
                  <Route
                    exact
                    path='/profile'
                    component={Account}
                  />
                  <Route
                    path='/profile/account'
                    component={Account}
                  />
                  <Route
                    path='/profile/change-password'
                    component={ChangePassword}
                  />
                  <Route
                    path='/profile/settings'
                    component={() => <div>Settings</div>}
                  />
                </Switch>
              </Col>
            </Row>
          </MainSection>
        </Wrapper>
      )}
    </>
  );
};

const mapStateToProps = state => ({
  user: getUser(state)
});

export default connect(mapStateToProps)(ProfilePage);
