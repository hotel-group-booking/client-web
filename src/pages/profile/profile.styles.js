import styled from 'styled-components';
import { Row as AntRow, Col as AntCol } from 'antd';

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  overflow-x: hidden;
  min-height: 100vh;
`;

const MainSection = styled.div`
  max-width: 1100px;
  width: 100%;
  margin: 0 auto;
`;

const Row = styled(AntRow).attrs(props => ({
  gutter: 0
}))``;

const Col = styled(AntCol)``;

export { Wrapper, MainSection, Row, Col };
