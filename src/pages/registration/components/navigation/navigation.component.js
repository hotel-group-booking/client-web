import React from 'react';
import { withRouter } from 'react-router-dom';
import { Wrapper, Brand } from './navigation.styles';

const Navigation = ({history, ...rest}) => {

  const handleRedirect = url => e => {
    history.push(url);
  };
  
  return (
    <Wrapper {...rest}>
      <Brand onClick={handleRedirect('/')}>Hotel Group Booking</Brand>
    </Wrapper>
  );
};

export default withRouter(Navigation);
