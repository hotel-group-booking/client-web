import styled from 'styled-components';

const Wrapper = styled.div`
  height: 5rem;
  background-color: var(--white);
  box-shadow: var(--highlight);
  display: flex;
  align-items: center;
  padding: 0 4rem;

  @media (max-width: 430px) {
    padding: 0 1rem;
  }
`;

const Brand = styled.div`
  font-size: 2rem;
  color: var(--text-title);
  font-family: var(--font-baloo);
  width: 100%;
  text-align: center;
  cursor: pointer;


  :hover {
    color: var(--brand-color);
  }

  @media (min-width: 576px) {
    text-align: left;
    width: fit-content;
  }

  @media (max-width: 430px) {
    font-size: 1.5rem;
  }
`;

export {
  Wrapper,
  Brand
};