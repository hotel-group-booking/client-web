import React from 'react';
import { compose, pick, filter } from 'ramda';
import { useTranslation } from 'react-i18next';
import { validator } from 'config/validator';
import {
  Wrapper,
  Form,
  Input,
  Item,
  Row,
  Col,
  Footer,
  Warnning,
  ContinueButton
} from './account-pane.styles';

const AccountPane = ({
  form: { getFieldDecorator, getFieldValue, validateFields },
  onSubmit,
  ...rest
}) => {
  const [t] = useTranslation(['registation-page', 'validator']);

  const handleSubmit = e => {
    e.preventDefault();
    validateFields((err, values) => {
      if (!err) {
        const account = compose(
          pick(['username', 'password', 'email', 'displayName']),
          filter(Boolean)
        )(values);
        onSubmit(account);
      }
    });
  };

  const rules = validator(t);

  const username = () =>
    getFieldDecorator('username', {
      rules: [rules.required, rules.minLength, rules.maxLength]
    })(<Input placeholder={t('registration-page:placeholders.username')} />);

  const displayName = () =>
    getFieldDecorator('displayName', {
      rules: [rules.minLength, rules.maxLength]
    })(
      <Input placeholder={t('registration-page:placeholders.display name')} />
    );

  const email = () =>
    getFieldDecorator('email', {
      rules: [rules.required]
    })(<Input placeholder={t('registration-page:placeholders.email')} />);

  const confirmEmail = () =>
    getFieldDecorator('confirmEmail', {
      rules: [
        rules.required,
        rules.confirm(getFieldValue('email'), 'email confirm wrong')
      ]
    })(
      <Input
        type='email'
        placeholder={t('registration-page:placeholders.confirm email')}
      />
    );

  const password = () =>
    getFieldDecorator('password', {
      rules: [rules.required, rules.minLength, rules.maxLength]
    })(
      <Input
        type='password'
        placeholder={t('registration-page:placeholders.password')}
      />
    );

  const confirmPassword = () =>
    getFieldDecorator('confirmPassword', {
      rules: [
        rules.required,
        rules.confirm(getFieldValue('password'), 'confirm password wrong')
      ]
    })(
      <Input
        type='password'
        placeholder={t('registration-page:placeholders.confirm password')}
      />
    );

  return (
    <Wrapper {...rest}>
      <Form onSubmit={handleSubmit}>
        <Row>
          <Col xs={24} md={12}>
            <Item label={t('registration-page:labels.username')}>
              {username()}
            </Item>
          </Col>
          <Col xs={24} md={12}>
            <Item label={t('registration-page:labels.display name')}>
              {displayName()}
            </Item>
          </Col>
          <Col xs={24} md={12}>
            <Item label={t('registration-page:labels.email')}>{email()}</Item>
          </Col>
          <Col xs={24} md={12}>
            <Item label={t('registration-page:labels.confirm email')}>
              {confirmEmail()}
            </Item>
          </Col>
          <Col xs={24} md={12}>
            <Item label={t('registration-page:labels.password')}>
              {password()}
            </Item>
          </Col>
          <Col xs={24} md={12}>
            <Item label={t('registration-page:labels.confirm password')}>
              {confirmPassword()}
            </Item>
          </Col>
        </Row>

        <Footer>
          <Warnning>
            {t('registration-page:warn.1')}
            <span className='upper'>{t('registration-page:warn.2')}</span>
            {t('registration-page:warn.3')}
            <span className='link'>{t('registration-page:warn.4')}</span>
          </Warnning>
          <ContinueButton type='gradient' htmlType='submit'>
            {t('registration-page:buttons.continue')}
          </ContinueButton>
        </Footer>
      </Form>
    </Wrapper>
  );
};

export default Form.create()(AccountPane);
