import styled from 'styled-components';
import {
  Form as AntForm,
  Input as AntInput,
  Row as AntRow,
  Col as AntCol
} from 'antd';
import { Button } from 'components';

const Wrapper = styled.div`
  width: 100%;
  height: 100%;
`;

const Form = styled(AntForm)``;

const Item = styled(AntForm.Item)`
  label {
    color: var(--text-title);
    font-size: 1rem;
    font-weight: 500;
  }
`;

const Input = styled(AntInput)`
  width: 100%;
`;

const Row = styled(AntRow).attrs(props => ({
  gutter: 24
}))``;

const Col = styled(AntCol)``;

const Footer = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  margin-top: .5rem;
`;

const Warnning = styled.div`
  font-size: .8rem;
  font-family: var(--font-baloo);
  color: var(--text-em);
  text-align: center;
  margin-bottom: .5rem;

  .link {
    color: #1890ff;
    cursor: pointer;
  }

  .upper {
    text-transform: uppercase;
  }
`;

const ContinueButton = styled(Button)`
  text-transform: uppercase;
  font-family: var(--font-baloo);
`;

export { Wrapper, Form, Item, Input, Row, Col, Footer, Warnning, ContinueButton };
