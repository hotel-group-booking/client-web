import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';
import { validator } from 'config/validator';
import { GeocodeAutocomplete, LocationAutocomplete } from 'components';
import {
  Wrapper,
  Form,
  Row,
  Col,
  Input,
  Item,
  InputNumber,
  Label,
  Slider,
  Footer,
  ContinueButton
} from './hotel-pane.styles';

const HotelPane = ({
  form: { getFieldDecorator, validateFields, setFieldsValue, getFieldValue },
  onSubmit,
  ...rest
}) => {
  const [t] = useTranslation(['validator', 'registration-page']);
  const [location, setLocation] = useState();
  const [address, setAddress] = useState();

  const handleSliderChange = values => {
    setFieldsValue({
      budgetFrom: values[0],
      budgetTo: values[1]
    });
  };

  const handleAddressSelect = address => {
    setAddress(address);
  };

  const handleLocationSelect = location => {
    setLocation(location);
  };

  const handleSubmit = e => {
    e.preventDefault();
    validateFields((err, values) => {
      if (!err) {
        const hotelInformation = {
          ...values,
          address,
          location: {
            placeId: location.placeId,
            name: location.name
          }
        };
        onSubmit(hotelInformation);
      }
    });
  };


  const rules = validator(t);
  const name = () =>
    getFieldDecorator('name', {
      rules: [rules.required]
    })(<Input placeholder={t('registration-page:placeholders.hotel name')} />);

  const starRating = () =>
    getFieldDecorator('starRating', {
      rules: [rules.required]
    })(
      <InputNumber
        placeholder={t('registration-page:placeholders.star rating')}
      />
    );

  const totalRoom = () =>
    getFieldDecorator('totalRoom', {
      rules: [rules.required]
    })(
      <InputNumber
        placeholder={t('registration-page:placeholders.total room')}
      />
    );

  const budgetFrom = () =>
    getFieldDecorator('budgetFrom')(
      <Input
        addonAfter='000 VND'
        placeholder={t('registration-page:placeholders.from')}
      />
    );

  const budgetTo = () =>
    getFieldDecorator('budgetTo')(
      <Input
        addonAfter='000 VND'
        placeholder={t('registration-page:placeholders.to')}
      />
    );

  return (
    <Wrapper {...rest}>
      <Form onSubmit={handleSubmit}>
        <Row>
          <Col xs={24}>
            <Item label={t('registration-page:labels.hotel name')}>
              {name()}
            </Item>
          </Col>
        </Row>

        <Row>
          <Col xs={24} mb='0.5rem'>
            <Label>{t('registration-page:labels.hotel address')}</Label>
          </Col>
          <Col xs={24}>
            <GeocodeAutocomplete
              onSelect={handleAddressSelect}
              placeholder={t('registration-page:placeholders.hotel address')}
            />
          </Col>
        </Row>

        <Row mt='2rem'>
          <Col xs={24} mb='0.5rem'>
            <Label>{t('registration-page:labels.location')}</Label>
          </Col>
          <Col xs={24}>
            <LocationAutocomplete
              onSelect={handleLocationSelect}
              placeholder={t('registration-page:placeholders.location')}
            />
          </Col>
        </Row>

        <Row mt='2rem'>
          <Col xs={12}>
            <Item label={t('registration-page:labels.total room')}>
              {totalRoom()}
            </Item>
          </Col>
          <Col xs={12}>
            <Item label={t('registration-page:labels.star rating')}>
              {starRating()}
            </Item>
          </Col>
        </Row>

        <Row>
          <Col xs={24} md={6}>
            <Item
              label={t('registration-page:labels.budget range')}
              colon={false}
              required
              mb='0'
            >
              {budgetFrom()}
            </Item>
            <Item>
              {budgetTo()}
            </Item>
          </Col>
          <Col xs={24} md={18}>
            <Slider onSliderChange={handleSliderChange}/>
          </Col>
        </Row>
        <Footer>
          <ContinueButton type='gradient' htmlType='submit'>
            {t('registration-page:continue')}
          </ContinueButton>
        </Footer>
      </Form>
    </Wrapper>
  );
};

export default Form.create()(HotelPane);
