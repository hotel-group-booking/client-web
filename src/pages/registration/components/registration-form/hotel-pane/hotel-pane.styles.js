import styled from 'styled-components';
import {
  Form as AntForm,
  Row as AntRow,
  Col as AntCol,
  Input as AntInput,
  InputNumber as AntInputNumber
} from 'antd';
import { BudgetSlider, Button } from 'components';

const Wrapper = styled.div`
  height: 100%;
  width: 100%;
`;

const Form = styled(AntForm)``;

const Item = styled(AntForm.Item)`
  margin-bottom: ${props => props.mb};
  label {
    color: var(--text-title);
    font-size: 1rem;
    font-weight: 500;
  }
`;

const Input = styled(AntInput)`
  width: 100%;
`;

const InputNumber = styled(AntInputNumber)`
  width: 100%;
`;

const Row = styled(AntRow).attrs(props => ({
  gutter: 24
}))`
  margin-top: ${props => props.mt};
  margin-bottom: ${props => props.mb};
`;

const Col = styled(AntCol)`
  margin-bottom: ${props => props.mb};
`;

const Label = styled.span`
  font-size: 1rem;
  font-weight: 500;
  color: var(--text-em);

  ::before {
    display: inline-block;
    margin-right: 4px;
    color: #f5222d;
    font-size: 14px;
    font-family: SimSun, sans-serif;
    line-height: 1;
    content: '*';
  }
`;

const Slider = styled(BudgetSlider)`
  margin-bottom: 0.5rem;
  @media (min-width: 992px) {
    margin-top: 46px;
    padding: 0 4rem;
  }
`;

const Footer = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  margin-top: .5rem;
`;

const ContinueButton = styled(Button)`
  font-family: var(--font-baloo);
  text-transform: uppercase;
`;

export {
  Wrapper,
  Form,
  Item,
  Input,
  Row,
  Col,
  InputNumber,
  Label,
  Slider,
  Footer,
  ContinueButton
};
