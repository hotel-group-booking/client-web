import React from 'react';
import { pick } from 'ramda';
import moment from 'moment';
import { useTranslation } from 'react-i18next';
import { validator } from 'config/validator';
import {
  Wrapper,
  Form,
  Row,
  Col,
  Input,
  InputNumber,
  Item,
  DateLabel,
  Footer,
  SubmitButton
} from './information-pane.styles';

const InformationPane = ({
  form: { getFieldDecorator, validateFields },
  onSubmit,
  ...rest
}) => {
  const [t] = useTranslation(['registration-page', 'validator']);

  const handleSubmit = e => {
    e.preventDefault();
    validateFields((err, values) => {
      if (!err) {
        const information = {
          ...pick(['firstName', 'lastName', 'address', 'phone'], values),
          birthDate: moment(
            `${values.day}/${values.month}/${values.year}`,
            'D/M/YYYY'
          ).toISOString()
        };
        onSubmit(information);
      }
    });
  };

  const rules = validator(t);
  const firstName = () =>
    getFieldDecorator('firstName', {
      rules: [rules.required]
    })(<Input placeholder={t('registration-page:placeholders.first name')} />);

  const lastName = () =>
    getFieldDecorator('lastName', {
      rules: [rules.required]
    })(<Input placeholder={t('registration-page:placeholders.last name')} />);

  const phone = () =>
    getFieldDecorator('phone', {
      rules: [rules.required]
    })(<Input placeholder={t('registration-page:placeholders.phone')} />);

  const address = () =>
    getFieldDecorator('address', {
      rules: [rules.required]
    })(<Input placeholder={t('registration-page:placeholders.address')} />);

  const day = () =>
    getFieldDecorator('day', {
      rules: [rules.required]
    })(
      <InputNumber
        min={1}
        max={31}
        placeholder={t('registration-page:placeholders.day')}
      />
    );

  const month = () =>
    getFieldDecorator('month', {
      rules: [rules.required]
    })(
      <InputNumber
        min={1}
        max={12}
        placeholder={t('registration-page:placeholders.month')}
      />
    );

  const year = () =>
    getFieldDecorator('year', {
      rules: [rules.required]
    })(
      <InputNumber
        min={1900}
        max={new Date().getFullYear()}
        placeholder={t('registration-page:placeholders.year')}
      />
    );

  return (
    <Wrapper {...rest}>
      <Form onSubmit={handleSubmit}>
        <Row>
          <Col xs={24} md={12}>
            <Item label={t('registration-page:labels.first name')}>
              {firstName()}
            </Item>
          </Col>
          <Col xs={24} md={12}>
            <Item label={t('registration-page:labels.last name')}>
              {lastName()}
            </Item>
          </Col>
          <Col xs={24} md={12}>
            <Item label={t('registration-page:labels.phone')}>
              {phone()}
            </Item>
          </Col>
          <Col xs={24} md={12}>
            <Item label={t('registration-page:labels.address')}>
              {address()}
            </Item>
          </Col>
        </Row>

        <Row>
          <Col xs={8} md={6}>
            <DateLabel>{t('registration-page:labels.date of birth')}</DateLabel>
          </Col>
          <Col xs={5} md={6}>
            <Item>
              {day()}
            </Item>
          </Col>
          <Col xs={5} md={6}>
            <Item>
              {month()}
            </Item>
          </Col>
          <Col xs={5} md={6}>
            <Item>
              {year()}
            </Item>
          </Col>
        </Row>

        <Footer>
          <SubmitButton type='gradient' htmlType='submit'>
            {t('registration-page:finish')}
          </SubmitButton>
        </Footer>
      </Form>
    </Wrapper>
  );
};

export default Form.create()(InformationPane);
