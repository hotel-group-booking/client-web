import styled from 'styled-components';
import {
  Form as AntForm,
  Input as AntInput,
  Row as AntRow,
  Col as AntCol,
  InputNumber as AntInputNumber
} from 'antd';
import { Button } from 'components';

const Wrapper = styled.div`
  width: 100%;
  height: 100%;
`;

const Form = styled(AntForm)``;

const Item = styled(AntForm.Item)`
  label {
    color: var(--text-title);
    font-size: 1rem;
    font-weight: 500;
  }
`;

const Input = styled(AntInput)`
  width: 100%;
`;

const Row = styled(AntRow).attrs(props => ({
  gutter: 24
}))`
  margin-top: ${props => props.top || '0'};
`;

const Col = styled(AntCol)``;

const InputNumber = styled(AntInputNumber)`
  width: 100%;
`;

const DateLabel = styled.span`
  display: block;
  height: 100%;
  line-height: 30px;
  font-size: 1rem;
  color: var(--text-title);
  font-weight: 500;
  margin-top: 3px;
  ::before {
    display: inline-block;
    margin-right: 4px;
    color: #f5222d;
    font-size: 14px;
    font-family: SimSun, sans-serif;
    line-height: 1;
    content: '*';
  }
  ::after {
    content: ':';
  }
`;

const Footer = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
`;

const SubmitButton = styled(Button)`
  text-transform: uppercase;
  font-family: var(--font-baloo);
`;

export {
  Wrapper,
  Form,
  Item,
  Input,
  Row,
  Col,
  InputNumber,
  DateLabel,
  Footer,
  SubmitButton
};
