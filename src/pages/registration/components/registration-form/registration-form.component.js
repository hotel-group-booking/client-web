import React, { useState } from 'react';
import AccountPane from './account-pane/account-pane.component';
import InformationPane from './information-pane/information-pane.component';
import SummaryPane from './summary-pane/summary-pane.component';
import HotelPane from './hotel-pane/hotel-pane.component';
import { connect } from 'react-redux';
import { useTranslation } from 'react-i18next';
import { useTabBlock } from 'hooks';
import { getSavedRequest } from 'reducers/request';
import { doRegistration } from 'reducers/user';
import { Wrapper, Tabs, TabPane } from './registration-form.styles';

const RegistrationForm = ({
  registration,
  result,
  isRegistering,
  savedRequest,
  role,
  ...rest
}) => {
  const [activeKey, setActiveKey] = useState('1');
  const [account, setAccount] = useState();
  const [information, setInformation] = useState();
  const tabs = useTabBlock(activeKey);

  const [t] = useTranslation('registration-page');

  const handleTabClick = key => {
    if (tabs[key] === true) {
      setActiveKey(key);
    }
  };

  const handleAccountSubmit = account => {
    setActiveKey('2');
    setAccount(account);
  };

  const handleInformationSubmit = info => {
    if (role === 'member') {
      setActiveKey('4');
      registration({
        ...account,
        ...info,
        savedRequest
      })
    } else {
      setActiveKey('3');
      setInformation(info);
    }
  };

  const handleHotelSubmit = hotelInformation => {
    setActiveKey('4');
    registration({
      ...account,
      ...information,
      role: 'partner'
    }, hotelInformation);
  };

  return (
    <Wrapper {...rest}>
      <Tabs activeKey={activeKey} onTabClick={handleTabClick}>
        <TabPane tab={t('registration-page:tabs.account')} key='1'>
          <AccountPane onSubmit={handleAccountSubmit} />
        </TabPane>
        <TabPane tab={t('registration-page:tabs.information')} key='2'>
          <InformationPane onSubmit={handleInformationSubmit} />
        </TabPane>
        {role === 'partner' && (
          <TabPane tab={t('registration-page:tabs.hotel information')} key='3'>
            <HotelPane onSubmit={handleHotelSubmit}/>
          </TabPane>
        )}
        <TabPane tab={t('registration-page:tabs.summary')} key='4'>
          <SummaryPane/>
        </TabPane>
      </Tabs>
    </Wrapper>
  );
};

const mapStateToProps = state => ({
  savedRequest: getSavedRequest(state)
});

const mapDispatchToProps = dispatch => ({
  registration: (user, hotel) => dispatch(doRegistration(user, hotel))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(RegistrationForm);
