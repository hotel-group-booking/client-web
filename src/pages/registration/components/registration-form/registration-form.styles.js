import styled from 'styled-components';
import { Tabs as AntTabs } from 'antd';

const Wrapper = styled.div`
  background-color: var(--white);
  box-shadow: var(--highlight);
  width: 100%;
  max-width: 800px;
  padding: 1rem;
  border-radius: .5rem;

  .ant-tabs-nav-container {
    color: var(--text-title);
    font-family: var(--font-baloo);
  }
`;

const Tabs = styled(AntTabs)`

`;

const TabPane = styled(AntTabs.TabPane)``;

export {
  Wrapper,
  Tabs,
  TabPane
};
