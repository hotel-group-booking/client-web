import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { compose } from 'ramda';
import { withRouter } from 'react-router-dom';
import { getResult, getIsRegistering, doClearResult } from 'reducers/user';
import { MetroSpinner } from 'react-spinners-kit';
import { useTranslation } from 'react-i18next';
import { toast } from 'react-toastify';
import { Wrapper, IconSection, Message, Icon } from './summary-pane.styles';

const SummaryPane = ({
  history,
  result,
  isRegistering,
  clearResult,
  ...rest
}) => {
  const [t] = useTranslation('registration-page');

  useEffect(() => {
    clearResult();
  }, []);

  useEffect(() => {
    if (result && result.type === 'success') {
      toast(t('registration-page:summary.redirect countdown'), {
        autoClose: 3000,
        onClose: () => {
          history.push('/login');
        }
      });
    }
  }, [result]);

  return (
    <Wrapper {...rest}>
      {isRegistering && (
        <IconSection>
          <MetroSpinner size={60} color='#263957' />
        </IconSection>
      )}

      {result && result.type === 'error' && (
        <IconSection>
          <Icon
            type='frown'
            theme='twoTone'
            twoToneColor='#eb2f96'
            className='icon'
          />
          <Message>{t(`registration-page:summary.${result.message}`)}</Message>
        </IconSection>
      )}

      {result && result.type === 'success' && (
        <IconSection>
          <Icon
            type='check-circle'
            theme='twoTone'
            twoToneColor='#52c41a'
            className='icon'
          />
          <Message>{t('registration-page:summary.ok')}</Message>
        </IconSection>
      )}
    </Wrapper>
  );
};

const mapStateToProps = state => ({
  result: getResult(state),
  isRegistering: getIsRegistering(state)
});

const mapDispatchToProps = dispatch => ({
  clearResult: () => dispatch(doClearResult())
});

export default compose(
  connect(
    mapStateToProps,
    mapDispatchToProps
  ),
  withRouter
)(SummaryPane);
