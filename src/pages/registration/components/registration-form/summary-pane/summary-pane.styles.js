import styled from 'styled-components';
import { Icon as AntIcon } from 'antd';

const Wrapper = styled.div`
  width: 100%;
  height: 100%;
`;

const IconSection = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
`;

const Icon = styled(AntIcon)`
  svg {
    height: 60px;
    width: 60px;
  }
`;

const Message = styled.span`
  margin-top: 1.5rem;
  font-size: 1.2rem;
  font-family: var(--font-baloo);
  text-align: center;
  line-height: 1.2;
  color: var(--text-em);
`;


export {
  Wrapper,
  IconSection,
  Icon,
  Message
};
