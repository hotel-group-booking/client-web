import React from 'react';
import { Navigation, RegistrationForm } from './components';
import { Wrapper, BodySection, Title } from './registration.styles';
import { useTranslation } from 'react-i18next';

const RegistrationPage = ({history, match, ...rest}) => {

  const [t] = useTranslation('registration-page');
  
  return (
    <Wrapper>
      <Navigation/>
      <BodySection>
        <Title>
          {t(`registration-page:title.${match.params.role}`)}
        </Title>
        <RegistrationForm role={match.params.role}/>
      </BodySection>
    </Wrapper>
  );
};

export default RegistrationPage;
