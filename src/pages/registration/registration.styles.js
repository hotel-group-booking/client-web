import styled from 'styled-components';
import img from 'assets/dot-pattern.png';

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  min-height: 100vh;
  background-image: url(${img});
  background-repeat: repeat;
`;

const BodySection = styled.div`
  padding: 2rem .5rem;
  width: 100%;
  max-width: 1200px;
  margin: 0 auto;
  display: flex;
  flex-direction: column;
  align-items: center;
`;

const Title = styled.div`
  font-size: 2rem;
  font-family: var(--font-baloo);
  color: var(--text-title);
  margin-bottom: 1.5rem;
`;

export { Wrapper, BodySection, Title };
