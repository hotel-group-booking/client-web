const CHANGE_LANGUAGE = '@@application/CHANGE_LANGUAGE';
const FEATURE_SELECT = '@@application/FEATURE_SELECT';
const TOGGLE_SIDEBAR = '@@application/TOGGLE_SIDEBAR';

export default {
  CHANGE_LANGUAGE,
  FEATURE_SELECT,
  TOGGLE_SIDEBAR
};

const doChangeLanguage = lng => ({
  type: CHANGE_LANGUAGE,
  payload: {
    lng
  }
});

const doFeatureSelect = item => ({
  type: FEATURE_SELECT,
  payload: {
    item
  }
});

const doToggleSidebar = () => ({
  type: TOGGLE_SIDEBAR
});

export {
  doChangeLanguage,
  doFeatureSelect,
  doToggleSidebar
};