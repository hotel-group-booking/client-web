import applicationAction from './application.action';

const INITIAL_STATE = {
  lng: 'en',
  selectedFeature: 0,
  isShowSidebar: false,
};

const applyChangeLanguage = (state, action) => {
  const { payload: { lng } } = action;
  return {
    ...state,
    lng
  };
};

const applyFeatureSelect = (state, action) => {
  const { payload: { item } } = action;
  return {
    ...state,
    selectedFeature: item
  };
};

const applyToggleSidebar = (state, action) => {
  return {
    ...state,
    isShowSidebar: !state.isShowSidebar
  };
};

const reducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case applicationAction.CHANGE_LANGUAGE: {
      return applyChangeLanguage(state, action);
    }

    case applicationAction.FEATURE_SELECT: {
      return applyFeatureSelect(state, action);
    }

    case applicationAction.TOGGLE_SIDEBAR: {
      return applyToggleSidebar(state, action);
    }

    default: return state;
  }
};

export default reducer;