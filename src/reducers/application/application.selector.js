const getSettings = ({ applicationState }) => ({
  lng: applicationState.lng
});

const getIsShowSidebar = ({ applicationState }) => applicationState.isShowSidebar;

const getSelectedFeature = ({ applicationState }) =>
  applicationState.selectedFeature;

export { getSettings, getSelectedFeature, getIsShowSidebar };
