const HAS_ERROR = '@@auto-complete/HAS_ERROR';
const CREATE_SESSION_TOKEN = '@@auto-complete/CREATE_SESSION_TOKEN';
const FETCH_PREDICTIONS = '@@auto-complete/FETCH_PREDICTIONS';
const FETCH_PREDICTIONS_DONE = '@@auto-complete/FETCH_PREDICTIONS_DONE';
const FETCH_PREDICTIONS_FAILED = '@@auto-complete/FETCH_PREDICTIONS_FAILED';
const SELECT_PLACE = '@@auto-complete/SELECT_PLACE';
const SELECT_PLACE_DONE = '@@auto-complete/SELECT_PLACE_DONE';

export default {
  HAS_ERROR,
  CREATE_SESSION_TOKEN,
  FETCH_PREDICTIONS,
  FETCH_PREDICTIONS_DONE,
  FETCH_PREDICTIONS_FAILED,
  SELECT_PLACE,
  SELECT_PLACE_DONE
};

const doHasError = message => ({
  type: HAS_ERROR,
  payload: {
    message
  }
});

const doCreateSessionToken = () => ({
  type: CREATE_SESSION_TOKEN,
});

const doFetchPredictions = (input, sessionToken, typeAddress) => ({
  type: FETCH_PREDICTIONS,
  payload: {
    input,
    sessionToken,
    typeAddress
  }
});

const doFetchPredictionsDone = ({ predictions, message }) => ({
  type: FETCH_PREDICTIONS_DONE,
  payload: {
    predictions,
    message
  }
});

const doFetchPredictionsFailed = message => ({
  type: FETCH_PREDICTIONS_FAILED,
  payload: {
    message
  }
});

const doSelectPlace = (placeId, sessionToken) => ({
  type: SELECT_PLACE,
  payload: {
    placeId,
    sessionToken
  }
});

const doSelectPlaceDone = (place, token) => ({
  type: SELECT_PLACE_DONE,
  payload: {
    place,
    token
  }
});

export {
  doHasError,
  doCreateSessionToken,
  doFetchPredictions,
  doFetchPredictionsDone,
  doFetchPredictionsFailed,
  doSelectPlace,
  doSelectPlaceDone
};