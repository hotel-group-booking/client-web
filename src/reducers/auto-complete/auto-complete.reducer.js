import autoCompleteAction from './auto-complete.action';
import { createSessionToken } from '../../api';

const INITIAL_STATE = {
  message: '',
  hasError: false,
  failed: false,
  predictions: [],
  sessionToken: undefined,
  place: {},
};

const applyHasError = (state, action) => {
  const { payload: { message } } = action;
  return {
    ...state,
    message,
    hasError: true,
    failed: true
  };
};

const applyCreateSessionToken = (state, action) => {
  return {
    ...state,
    sessionToken: createSessionToken()
  };
};

const applyGetPredictions = (state, action) => {
  return {
    ...state,
    isLoading: true,
  };
};

const applyGetPredictionsDone = (state, action) => {
  const { payload: { predictions, message } } = action;

  return {
    ...state,
    isLoading: false,
    predictions,
    message,
    hasError: false,
    failed: false
  };
};

const applyGetPredictionsFailed = (state, action) => {
  const { payload: { message } } = action;
  
  return {
    ...state,
    isLoading: false,
    message,
    hasError: false,
    failed: true
  };
};

// const applySelectPlace = (state, action) => {
//   const { payload: { sessionToken } } = action;

//   return {
//     ...state,
//   }
// };

const applySelectPlaceDone = (state, action) => {
  const { payload: { place, token } } = action;
  
  return {
    ...state,
    place: {
      token,
      ...place,
    }
  };
};

const reducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case autoCompleteAction.HAS_ERROR: {
      return applyHasError(state, action);
    }

    case autoCompleteAction.CREATE_SESSION_TOKEN: {
      return applyCreateSessionToken(state, action);
    }

    case autoCompleteAction.FETCH_PREDICTIONS: {
      return applyGetPredictions(state, action);
    }

    case autoCompleteAction.FETCH_PREDICTIONS_DONE: {
      return applyGetPredictionsDone(state, action);
    }

    case autoCompleteAction.FETCH_PREDICTIONS_FAILED: {
      return applyGetPredictionsFailed(state, action);
    }

    // case autoCompleteAction.SELECT_PLACE: {
    //   return applySelectPlace(state, action);
    // }

    case autoCompleteAction.SELECT_PLACE_DONE: {
      return applySelectPlaceDone(state, action);
    }

    default: return state;
  }
}

export default reducer;