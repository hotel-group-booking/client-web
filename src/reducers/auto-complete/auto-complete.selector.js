const getPredictions = ({ autoCompleteState }) => {
  return autoCompleteState.predictions.map(prediction => ({
    description: prediction.description,
    placeId: prediction.place_id
  }));
};

const getStatus = ({ autoCompleteState }) => ({
  isLoading: autoCompleteState.isLoading,
  failed: autoCompleteState.failed,
  hasError: autoCompleteState.hasError,
  message: autoCompleteState.message
});

const getSessionToken = ({autoCompleteState}) => autoCompleteState.sessionToken;

const getPlace = ({ autoCompleteState }) => autoCompleteState.place

export {
  getPredictions,
  getStatus,
  getSessionToken,
  getPlace
};