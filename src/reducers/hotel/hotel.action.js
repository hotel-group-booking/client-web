const CLEAR_RESULT = '@@hotel/CLEAR_RESULT';
const UPLOAD_PHOTOS = '@@hotel/UPLOAD_PHOTOS';
const UPLOAD_PHOTOS_DONE = '@@hotel/UPLOAD_PHOTOS_DONE';
const UPLOAD_PHOTOS_FAILED = '@@hotel/UPLOAD_PHOTOS_FAILED';
const UPDATE = '@@hotel/UPDATE';
const UPDATE_DONE = '@@hotel/UPDATE_DONE';
const UPDATE_FAILED = '@@hotel/UPDATE_FAILED';
const FETCH_HOTEL = '@@hotel/FETCH_HOTEL';
const FETCH_HOTEL_SUCCESS = '@@hotel/FETCH_HOTEL_SUCCESS';
const FETCH_HOTEL_ERROR = '@@hotel/FETCH_HOTEL_ERROR';
const ADD_FIELD = '@@hotel/ADD_FIELD';
const RESET_FIELD = '@@hotel/RESET_FIELD';
const REMOVE_FIELD = '@@hotel/REMOVE_FIELD';
const SET_FIELD = '@@hotel/SET_FIELD';
const DELETE_PHOTO = '@@hotel/DELETE_PHOTO';
const DELETE_PHOTO_DONE = '@@hotel/DELETE_PHOTO_DONE';
const DELETE_PHOTO_FAILED = '@@hotel/DELETE_PHOTO_FAILED';

export default {
  CLEAR_RESULT,
  UPLOAD_PHOTOS,
  UPLOAD_PHOTOS_DONE,
  UPLOAD_PHOTOS_FAILED,
  UPDATE,
  UPDATE_DONE,
  UPDATE_FAILED,
  FETCH_HOTEL,
  FETCH_HOTEL_SUCCESS,
  FETCH_HOTEL_ERROR,
  ADD_FIELD,
  RESET_FIELD,
  REMOVE_FIELD,
  SET_FIELD,
  DELETE_PHOTO,
  DELETE_PHOTO_DONE,
  DELETE_PHOTO_FAILED
};

const doClearResult = () => ({
  type: CLEAR_RESULT
});

const doUploadPhotos = (userId, hotelId, images) => ({
  type: UPLOAD_PHOTOS,
  payload: {
    userId,
    hotelId,
    images
  }
});

const doUploadPhotosDone = () => ({
  type: UPLOAD_PHOTOS_DONE
});

const doUploadPhotosFailed = message => ({
  type: UPLOAD_PHOTOS_FAILED,
  payload: {
    message
  }
});

const doUpdate = (userId, hotelId, data) => ({
  type: UPDATE,
  payload: {
    userId,
    hotelId,
    data
  }
});

const doUpdateDone = () => ({
  type: UPDATE_DONE
});

const doUpdateFailed = message => ({
  type: UPDATE_FAILED,
  payload: {
    message
  }
})

const doFetchHotel = id => ({
  type: FETCH_HOTEL,
  payload: {
    id
  }
});

const doFetchHotelSuccess = (hotel) => ({
  type: FETCH_HOTEL_SUCCESS,
  payload: {
    hotel
  }
});

const doFetchHotelError = message => ({
  type: FETCH_HOTEL_ERROR,
  payload: {
    message
  }
});

const doAddField = (data) => ({
  type: ADD_FIELD,
  payload: {
    data
  }
});

const doRemoveField = index => ({
  type: REMOVE_FIELD,
  payload: {
    index
  }
});

const doResetField = () => ({
  type: RESET_FIELD
});

const doSetField = (index, data) => ({
  type: SET_FIELD,
  payload: {
    index,
    data
  }
});

const doDeletePhoto = (userId, hotelId, photoId) => ({
  type: DELETE_PHOTO,
  payload: {
    userId,
    hotelId,
    photoId
  }
});

const doDeletePhotoDone = () => ({
  type: DELETE_PHOTO_DONE,
});

const doDeletePhotoFailed = message => ({
  type: DELETE_PHOTO_FAILED,
  payload: {
    message
  }
});

export {
  doClearResult,
  doUploadPhotos,
  doUploadPhotosDone,
  doUploadPhotosFailed,
  doUpdate,
  doUpdateDone,
  doUpdateFailed,
  doFetchHotel,
  doFetchHotelSuccess,
  doFetchHotelError,
  doAddField,
  doResetField,
  doRemoveField,
  doSetField,
  doDeletePhoto,
  doDeletePhotoDone,
  doDeletePhotoFailed
};