import hotelAction from './hotel.action';

const INITIAL_STATE = {
  isUploadingPhotos: false,
  isUpdating: false,
  isFetchingHotel: false,
  result: undefined,
  hotel: undefined,
  fieldList: [],
  deletingPhoto: ''
};

const applyClearResult = (state, action) => {
  return {
    ...state,
    result: undefined
  };
};

const applyUploadPhotos = (state, action) => {
  return {
    ...state,
    isUploadingPhotos: true,
    result: undefined
  };
};

const applyUploadPhotosDone = (state, action) => {
  return {
    ...state,
    isUploadingPhotos: false,
  };
};

const applyUploadPhotosFailed = (state, action) => {
  const { payload: { message } } = action;
  return {
    ...state,
    isUploadingPhotos: false,
    result: {
      type: 'error',
      message
    }
  };
};

const applyFetchHotel = (state, action) => {
  return {
    ...state,
    isFetchingHotel: true,
    result: undefined
  };
};

const applyFetchHotelSuccess = (state, action) => {
  const { payload: { hotel } } = action;
  return {
    ...state,
    isFetchingHotel: false,
    result: {
      type: 'success',
    },
    hotel
  };
};

const applyFetchHotelError = (state, action) => {
  const { payload: { message } } = action;
  return {
    ...state,
    isFetchingHotel: false,
    result: {
      type: 'error',
      message
    }
  };
};

const applyUpdate = (state, action) => {
  return {
    ...state,
    result: undefined,
    isUpdating: true
  };
};

const applyUpdateDone = (state, action) => {
  return {
    ...state,
    isUpdating: false,
    result: {
      type: 'success',
    }
  };
};

const applyUpdateFailed = (state, action) => {
  const { payload: { message } } = action;
  return {
    ...state,
    result: {
      type: 'error',
      message
    },
    isUpdating: false
  };
};

const applyAddField = (state, action) => {
  const { payload: { data } } = action;
  return {
    ...state,
    fieldList: [...state.fieldList, data]
  };
};

const applyResetField = (state, action) => {
  return {
    ...state,
    fieldList: []
  };
};

const applyRemoveField = (state, action) => {
  const { payload: index, } = action;
  const { fieldList } = state;
  fieldList.splice(index.index, 1);
  return {
    ...state,
    fieldList: [...fieldList]
  };
};

const applySetField = (state, action) => {
  const { payload: { index, data } } = action;
  const { fieldList } = state;
  fieldList.splice(index, 1, {
    name: data.name,
    description: data.value
  });
  return {
    ...state,
    fieldList: [...fieldList]
  };
};

const applyDeletePhoto = (state, action) => {
  const { payload: { photoId } } = action;
  return {
    ...state,
    deletingPhoto: photoId
  };
};

const applyDeletePhotoDone = (state, action) => {
  return {
    ...state,
    deletingPhoto: ''
  }
};

const applyDeletePhotoFailed = (state, action) => {
  const { payload: { message } } = action;
  return {
    ...state,
    deletingPhoto: '',
    result: {
      type: 'error',
      message
    }
  };
};

const reducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case hotelAction.CLEAR_RESULT: {
      return applyClearResult(state, action);
    }

    case hotelAction.UPLOAD_PHOTOS: {
      return applyUploadPhotos(state, action);
    }

    case hotelAction.UPLOAD_PHOTOS_DONE: {
      return applyUploadPhotosDone(state, action);
    }

    case hotelAction.UPLOAD_PHOTOS_FAILED: {
      return applyUploadPhotosFailed(state, action);
    }

    case hotelAction.UPDATE: {
      return applyUpdate(state, action);
    }

    case hotelAction.UPDATE_DONE: {
      return applyUpdateDone(state, action);
    }

    case hotelAction.UPDATE_FAILED: {
      return applyUpdateFailed(state, action);
    }

    case hotelAction.FETCH_HOTEL: {
      return applyFetchHotel(state, action);
    }

    case hotelAction.FETCH_HOTEL_SUCCESS: {
      return applyFetchHotelSuccess(state, action);
    }

    case hotelAction.FETCH_HOTEL_ERROR: {
      return applyFetchHotelError(state, action);
    }

    case hotelAction.ADD_FIELD: {
      return applyAddField(state, action);
    }

    case hotelAction.RESET_FIELD: {
      return applyResetField(state, action);
    }

    case hotelAction.REMOVE_FIELD: {
      return applyRemoveField(state, action);
    }

    case hotelAction.SET_FIELD: {
      return applySetField(state, action);
    }

    case hotelAction.DELETE_PHOTO: {
      return applyDeletePhoto(state, action);
    }

    case hotelAction.DELETE_PHOTO_DONE: {
      return applyDeletePhotoDone(state, action);
    }

    case hotelAction.DELETE_PHOTO_FAILED: {
      return applyDeletePhotoFailed(state, action);
    }

    default: return state;
  }
};

export default reducer;