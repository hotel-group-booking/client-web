const getHotel = ({ hotelState }) => hotelState.hotel;

const getIsFetchingHotel = ({ hotelState }) => hotelState.isFetchingHotel;

const getFieldList = ({ hotelState }) => hotelState.fieldList;

const getResult = ({ hotelState }) => hotelState.result;

const getDeletingPhoto = ({hotelState}) => hotelState.deletingPhoto;

const getIsUpdating = ({hotelState}) => hotelState.isUpdating;

export {
  getHotel,
  getIsFetchingHotel,
  getFieldList,
  getResult,
  getDeletingPhoto,
  getIsUpdating
};
