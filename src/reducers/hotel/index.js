export { default as hotelReducer } from './hotel.reducer';
export { default as hotelAction } from './hotel.action';
export * from './hotel.action';
export * from './hotel.selector';