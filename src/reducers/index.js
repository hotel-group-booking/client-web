import { combineReducers } from 'redux';
import { authReducer } from './auth';
import { autoCompleteReducer } from './auto-complete';
import { applicationReducer } from './application';
import { userReducer } from './user';
import { requestReducer } from './request';
import { offerReducer } from './offer'
import { messageReducer } from './message';
import { notificationReducer } from './notification';
import { hotelReducer } from './hotel';
import { mapReducer } from './map';

const rootReducer = combineReducers({
  authState: authReducer,
  autoCompleteState: autoCompleteReducer,
  applicationState: applicationReducer,
  userState: userReducer,
  requestState: requestReducer,
  offerState: offerReducer,
  messageState: messageReducer,
  notificationState: notificationReducer,
  hotelState: hotelReducer,
  mapState: mapReducer
});

export default rootReducer;