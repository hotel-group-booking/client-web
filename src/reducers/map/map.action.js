const DRAG_PLACE = '@@map/DRAG_PLACE';
const DRAG_PLACE_DONE = '@@map/DRAG_PLACE_DONE';
const DRAG_PLACE_FAILED = '@@map/DRAG_PLACE_FAILED';

export default {
  DRAG_PLACE,
  DRAG_PLACE_DONE,
  DRAG_PLACE_FAILED
};

const doDragPlace = (lat, lng) => ({
  type: DRAG_PLACE,
  payload: {
    lat,
    lng
  }
});

const doDragPlaceDone = (data) => ({
  type: DRAG_PLACE_DONE,
  payload: {
    data
  }
});

const doDragPlaceFailed = message => ({
  type: DRAG_PLACE_FAILED,
  payload: {
    message
  }
})

export {
  doDragPlace,
  doDragPlaceDone,
  doDragPlaceFailed
};