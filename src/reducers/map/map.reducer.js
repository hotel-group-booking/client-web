import mapAction from './map.action';

const INITIAL_STATE = {
  isDragging: false,
  place: {}
};


const applyDragPlace = (state, action) => {
  return {
    ...state,
    isDragging: true
  };
};

const applyDragPlaceDone = (state, action) => {
  return {
    ...state,
    isDragging: false,
    place: action.payload.data
  };
};

const applyDragPlaceFailed = (state, action) => {
  const { payload: { message } } = action;
  return {
    ...state,
    result: {
      type: 'error',
      message
    },
    isUpdating: false
  };
};

const reducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {

    case mapAction.DRAG_PLACE: {
      return applyDragPlace(state, action);
    }

    case mapAction.DRAG_PLACE_DONE: {
      return applyDragPlaceDone(state, action);
    }

    case mapAction.DRAG_PLACE_FAILED: {
      return applyDragPlaceFailed(state, action);
    }

    default: return state;
  }
};

export default reducer;