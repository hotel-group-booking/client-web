const getMapPlace = ({ mapState }) => mapState.place;

export {
  getMapPlace
};
