const ADD_MESSAGE = '@@message/ADD_MESSAGE';
const LOAD_HISTORY = '@@message/LOAD_HISTORY';
const ADD_TYPING = '@@message/ADD_TYPING';
const REMOVE_TYPING = '@@message/REMOVE_TYPING';
const SUBCRIBE = '@@message/SUBCRIBE';
const ADD_MESSAGES = '@@message/ADD_MESSAGES';
const SEND = '@@message/SEND';
const EMIT = '@@message/EMIT';

export default {
  ADD_MESSAGE,
  LOAD_HISTORY,
  ADD_TYPING,
  REMOVE_TYPING,
  SUBCRIBE,
  ADD_MESSAGES,
  SEND,
  EMIT
};

const doAddMessage = message => ({
  type: ADD_MESSAGE,
  payload: {
    message
  }
});

const doLoadHistory = messages => ({
  type: LOAD_HISTORY,
  payload: {
    messages
  }
});

const doAddTyping = user => ({
  type: ADD_TYPING,
  payload: {
    user
  }
});

const doRemoveTyping = user => ({
  type: REMOVE_TYPING,
  payload: {
    user
  }
});

const doSubcribe = room => ({
  type: SUBCRIBE,
  payload: {
    room
  }
});

const doAddMessages = messages => ({
  type: ADD_MESSAGES,
  payload: {
    messages
  }
});

const doSend = message => ({
  type: SEND,
  payload: {
    message
  }
});

const doEmit = (room, event, data) => ({
  type: EMIT,
  payload: {
    room, event, data
  }
});

export {
  doAddMessage,
  doLoadHistory,
  doAddTyping,
  doRemoveTyping,
  doSubcribe,
  doAddMessages,
  doSend,
  doEmit
};