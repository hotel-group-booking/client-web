import messageAction from './message.action';
import { filter, uniqBy, prop, find } from 'ramda';

const INITIAL_STATE = {
  messages: [],
  typing: []
};

const applyAddMessage = (state, action) => {
  const {
    payload: { message }
  } = action;
  return {
    ...state,
    messages: [...state.messages, message]
  };
};

const applyLoadHistory = (state, action) => {
  const {
    payload: { messages }
  } = action;
  return {
    ...state,
    messages
  };
};

const applyAddTyping = (state, action) => {
  const {
    payload: { user }
  } = action;
  return {
    ...state,
    typing: find(t => t.id === user.id, state.typing)
      ? filter(t => t.id !== user.id, state.typing)
      : [...state.typing, user]
  };
};

const applyRemoveTyping = (state, action) => {
  const {
    payload: { user }
  } = action;
  return {
    ...state,
    typing: filter(u => u._id !== user._id)(state.typing)
  };
};

const applyAddMessages = (state, action) => {
  const {
    payload: { messages }
  } = action;
  return {
    ...state,
    messages: uniqBy(prop('_id'), [...state.messages, ...messages])
  };
};

const reducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case messageAction.ADD_MESSAGE: {
      return applyAddMessage(state, action);
    }

    case messageAction.LOAD_HISTORY: {
      return applyLoadHistory(state, action);
    }

    case messageAction.ADD_TYPING: {
      return applyAddTyping(state, action);
    }

    case messageAction.REMOVE_TYPING: {
      return applyRemoveTyping(state, action);
    }

    case messageAction.ADD_MESSAGES: {
      return applyAddMessages(state, action);
    }
    default:
      return state;
  }
};

export default reducer;
