const getMessages = ({ messageState }) => messageState.messages;

const getTyping = ({ messageState }) => messageState.typing;

export {
  getMessages,
  getTyping
}