const SUBCRIBE = '@@notification/SUBCRIBE';
const ADD_NOTIFICATION = '@@notification/ADD_NOTIFICATION';
const FETCH_NOTIFICATIONS = '@@notification/FETCH_NOTIFICATIONS';
const FETCH_NOTIFICATIONS_DONE = '@@notification/FETCH_NOTIFICATIONS_DONE';
const FETCH_NOTIFICATIONS_FAILED = '@@notification/FETCH_NOTIFICATIONS_FAILED';
const ADD_NOTIFICAITONS = '@@notification/ADD_NOTIFICATIONS';
const UPDATE = '@@notification/UPDATE';

export default {
  SUBCRIBE,
  ADD_NOTIFICATION,
  FETCH_NOTIFICATIONS,
  FETCH_NOTIFICATIONS_DONE,
  FETCH_NOTIFICATIONS_FAILED,
  ADD_NOTIFICAITONS,
  UPDATE,
}

const doSubcribe = room => ({
  type: SUBCRIBE,
  payload: {
    room
  }
});

const doAddNotification = noti => ({
  type: ADD_NOTIFICATION,
  payload: {
    noti
  }
});

const doFetchNotifications = userId => ({
  type: FETCH_NOTIFICATIONS,
  payload: {
    userId
  }
});

const doFetchNotificationsDone = () => ({
  type: FETCH_NOTIFICATIONS_DONE
});

const doFetchNotificationsFailed = message => ({
  type: FETCH_NOTIFICATIONS_FAILED,
  payload: {
    message
  }
});

const doAddNotifications = notifications => ({
  type: ADD_NOTIFICAITONS,
  payload: {
    notifications
  }
});

const doUpdate = (id, body) => ({
  type: UPDATE,
  payload: {
    id,
    body
  }
});



export {
  doSubcribe,
  doAddNotification,
  doFetchNotifications,
  doFetchNotificationsDone,
  doFetchNotificationsFailed,
  doAddNotifications,
  doUpdate,
}