import notificationAction from './notification.action';
import { findIndex, uniqBy, prop } from 'ramda';

const INITIAL_STATE = {
  room: undefined,
  notifications: [],
  isFetchingNotifications: false,
  result: undefined
};

const applySubcribe = (state, action) => {
  const {
    payload: { room }
  } = action;
  return {
    ...state,
    room
  };
};

const applyAddNotification = (state, action) => {
  const {
    payload: { noti }
  } = action;
  return {
    ...state,
    notifications: uniqBy(prop('_id'))([noti, ...state.notifications])
  };
};

const applyFetchNotifications = (state, action) => {
  return {
    ...state,
    result: undefined,
    isFetchingNotifications: true
  };
};

const applyFetchNotificationsDone = (state, action) => {
  return {
    ...state,
    result: {
      type: 'success'
    },
    isFetchingNotifications: false
  };
};

const applyFetchNotificationsFailed = (state, action) => {
  const {
    payload: { message }
  } = action;
  return {
    ...state,
    result: {
      type: 'error',
      message
    },
    isFetchingNotifications: false
  };
};

const applyAddNotifications = (state, action) => {
  const {
    payload: { notifications }
  } = action;
  return {
    ...state,
    notifications: uniqBy(prop('_id'))([
      ...state.notifications,
      ...notifications
    ])
  };
};

const applyUpdate = (state, action) => {
  const notifications = [...state.notifications];
  const {
    payload: { id }
  } = action;
  const index = findIndex(noti => noti._id === id)(notifications);
  notifications[index].isRead = true;
  return {
    ...state,
    notifications
  };
};

const reducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case notificationAction.SUBCRIBE: {
      return applySubcribe(state, action);
    }

    case notificationAction.ADD_NOTIFICATION: {
      return applyAddNotification(state, action);
    }

    case notificationAction.FETCH_NOTIFICATIONS: {
      return applyFetchNotifications(state, action);
    }

    case notificationAction.FETCH_NOTIFICATIONS_DONE: {
      return applyFetchNotificationsDone(state, action);
    }

    case notificationAction.FETCH_NOTIFICATIONS_FAILED: {
      return applyFetchNotificationsFailed(state, action);
    }

    case notificationAction.ADD_NOTIFICAITONS: {
      return applyAddNotifications(state, action);
    }

    case notificationAction.UPDATE: {
      return applyUpdate(state, action);
    }

    default:
      return state;
  }
};

export default reducer;
