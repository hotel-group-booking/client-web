const getNotifications = ({ notificationState }) =>
  notificationState.notifications;

const getIsFetchingNotifications = ({ notificationState }) =>
  notificationState.isFetchingNotifications;

export { getNotifications, getIsFetchingNotifications };
