const FETCH_USER_INFORMATION = '@@offer/FETCH_USER_INFORMATION';
const RESET_STATUS = '@@offer/RESET_STATUS';


const CLEAR_RESULT = '@@offer/CLEAR_RESULT';
const FETCH_OFFERS = '@@offer/FETCH_OFFERS';
const FETCH_OFFERS_SUCCESS = '@@offer/FETCH_OFFER_SUCCESS';
const FETCH_OFFERS_ERROR = '@@offer/FETCH_OFFER_ERROR';
const ADD_OFFERS = '@@offers/ADD_OFFERS';
const FETCH_DETAIL = '@@offer/FETCH_DETAIL';
const FETCH_DETAIL_SUCCESS = '@@offer/FETCH_DETAIL_SUCCESS';
const FETCH_DETAIL_ERROR = '@@offer/FETCH_DETAIL_ERROR';
const ADD_DETAIL = '@@offer/ADD_DETAIL';
const FETCH_CREATED_OFFER = '@@offer/FETCH_CREATED_OFFER';
const FETCH_CREATED_OFFER_SUCCESS = '@@offer/FETCH_CREATED_OFFER_SUCCESS';
const FETCH_CREATED_OFFER_ERROR = '@@offer/FETCH_CREATED_OFFER_ERROR';
const FETCH_PARTNER_OFFER = '@@offer/FETCH_PARTNER_OFFER';
const FETCH_PARTNER_OFFER_SUCCESS = '@@offer/FETCH_PARTNER_OFFER_SUCCESS';
const FETCH_PARTNER_OFFER_ERROR = '@@offer/FETCH_PARTNER_OFFER_ERROR';
const ADD_CREATED_OFFER = '@@offer/ADD_CREATED_OFFER';
const UPDATE_OFFER = '@@offer/UPDATE_OFFER';
const UPDATE_OFFER_SUCCESS = '@@offer/UPDATE_OFFER_SUCCESS';
const UPDATE_OFFER_ERROR = '@@offer/UPDATE_OFFER_ERROR';
const CREATE_OFFER = '@@offer/CREATE_OFFER';
const CREATE_OFFER_SUCCESS = '@@offer/CREATE_OFFER_SUCCESS';
const CREATE_OFFER_ERROR = '@@offer/CREATE_OFFER_ERROR';
const ACCEPT_OFFER = '@@offer/ACCEPT_OFFER';
const ACCEPT_OFFER_SUCCESS = '@@offer/ACCEPT_OFFER_SUCCESS';
const ACCEPT_OFFER_ERROR = '@@offer/ACCEPT_OFFER_ERROR';

export default {
  FETCH_USER_INFORMATION,
  RESET_STATUS,


  CLEAR_RESULT,
  FETCH_OFFERS,
  FETCH_OFFERS_SUCCESS,
  FETCH_OFFERS_ERROR,
  ADD_OFFERS,
  FETCH_DETAIL,
  FETCH_DETAIL_SUCCESS,
  FETCH_DETAIL_ERROR,
  ADD_DETAIL,
  FETCH_CREATED_OFFER,
  FETCH_CREATED_OFFER_SUCCESS,
  FETCH_CREATED_OFFER_ERROR,
  FETCH_PARTNER_OFFER,
  FETCH_PARTNER_OFFER_SUCCESS,
  FETCH_PARTNER_OFFER_ERROR,
  ADD_CREATED_OFFER,
  UPDATE_OFFER,
  UPDATE_OFFER_SUCCESS,
  UPDATE_OFFER_ERROR,
  CREATE_OFFER,
  CREATE_OFFER_SUCCESS,
  CREATE_OFFER_ERROR,
  ACCEPT_OFFER,
  ACCEPT_OFFER_SUCCESS,
  ACCEPT_OFFER_ERROR
};

const doClearResult = () => ({
  type: CLEAR_RESULT
});

const doFetchOffers = (page, requestId) => ({
  type: FETCH_OFFERS,
  payload: {
    requestId,
    page
  }
});

const doFetchOffersSuccess = () => ({
  type: FETCH_OFFERS_SUCCESS
});

const doFetchOffersErrors = message => ({
  type: FETCH_OFFERS_ERROR,
  payload: {
    message
  }
});

const doAddOffers = (offers, page, size) => ({
  type: ADD_OFFERS,
  payload: {
    offers,
    page,
    size
  }
});

const doFetchDetail = id => ({
  type: FETCH_DETAIL,
  payload: {
    id
  }
});

const doFetchDetailSuccess = () => ({
  type: FETCH_DETAIL_SUCCESS,
});

const doFetchDetailError = message => ({
  type: FETCH_DETAIL_ERROR,
  payload: {
    message
  }
});

const doAddDetail = offer => ({
  type: ADD_DETAIL,
  payload: {
    offer
  }
});

const doFetchCreatedOffer = (partner, request) => ({
  type: FETCH_CREATED_OFFER,
  payload: {
    partner,
    request
  }
});

const doFetchCreatedOfferSuccess = () => ({
  type: FETCH_CREATED_OFFER_SUCCESS,
});

const doFetchCreatedOfferError = message => ({
  type: FETCH_CREATED_OFFER_ERROR,
  payload: {
    message
  }
});

const doFetchPartnerOffers = (partner, page) => ({
  type: FETCH_PARTNER_OFFER,
  payload: {
    partner,
    page
  }
});

const doFetchPartnerOffersSuccess = () => ({
  type: FETCH_PARTNER_OFFER_SUCCESS,
});

const doFetchPartnerOffersError = message => ({
  type: FETCH_PARTNER_OFFER_ERROR,
  payload: {
    message
  }
});

const doAddCreatedOffer = offer => ({
  type: ADD_CREATED_OFFER,
  payload: {
    offer
  }
});

const doUpdateOffer = (id, offer) => ({
  type: UPDATE_OFFER,
  payload: {
    id,
    offer
  }
});

const doUpdateOfferSuccess = () => ({
  type: UPDATE_OFFER_SUCCESS,
});

const doUpdateOfferError = message => ({
  type: UPDATE_OFFER_ERROR,
  payload: {
    message
  }
});

const doCreateOffer = offer => ({
  type: CREATE_OFFER,
  payload: {
    offer
  }
});

const doCreateOfferSuccess = () => ({
  type: CREATE_OFFER_SUCCESS
});

const doCreateOfferError = message => ({
  type: CREATE_OFFER_ERROR,
  payload: {
    message
  }
});

const doAcceptOffer = (id, accepted) => ({
  type: ACCEPT_OFFER,
  payload: {
    id,
    accepted
  }
});

const doAcceptOfferSuccess = () => ({
  type: ACCEPT_OFFER_SUCCESS
});

const doAcceptOfferError = message => ({
  type: ACCEPT_OFFER_ERROR,
  payload: {
    message
  }
});

export {
  doClearResult,
  doFetchOffers,
  doFetchOffersSuccess,
  doFetchOffersErrors,
  doAddOffers,
  doFetchDetail,
  doFetchDetailSuccess,
  doFetchDetailError,
  doAddDetail,
  doFetchCreatedOffer,
  doFetchCreatedOfferSuccess,
  doFetchCreatedOfferError,
  doFetchPartnerOffers,
  doFetchPartnerOffersSuccess,
  doFetchPartnerOffersError,
  doAddCreatedOffer,
  doUpdateOffer,
  doUpdateOfferSuccess,
  doUpdateOfferError,
  doCreateOffer,
  doCreateOfferSuccess,
  doCreateOfferError,
  doAcceptOffer,
  doAcceptOfferSuccess,
  doAcceptOfferError
};