import offerAction from './offer.action';

const INITIAL_STATE = {
  isFetchingOffers: false,
  isFetchingDetail: false,
  isFetchingCreatedOffer: false,
  isFetchingPartnerOffer: false,
  isCreatingOffer: false,
  isAcceptingOffer: false,
  list: {
    size: 0,
    page: 0,
    values: []
  },
  createdOffer: undefined,
  detail: undefined,
  result: undefined
};

const applyClearResult = (state, action) => {
  return {
    ...state,
    result: undefined
  };
};

const applyFetchOffers = (state, action) => {
  return {
    ...state,
    result: undefined,
    isFetchingOffers: true,
    list: {
      size: 0,
      page: 0,
      values: []
    }
  };
};

const applyFetchOffersSuccess = (state, action) => {
  return {
    ...state,
    result: {
      type: 'success'
    },
    isFetchingOffers: false
  };
};

const applyFetchOffersError = (state, action) => {
  const { payload: { message } } = action;
  return {
    ...state,
    result: {
      type: 'error',
      message
    },
    isFetchingOffers: false
  };
};

const applyAddOffers = (state, action) => {
  const { payload: { offers, size, page } } = action;
  return {
    ...state,
    list: {
      size,
      page,
      values: [...state.list.values, ...offers]
    }
  };
};

const applyFetchDetail = (state, action) => {
  return {
    ...state,
    isFetchingDetail: true,
    result: undefined,
    detail: undefined
  };
};

const applyFetchDetailSuccess = (state, action) => {
  return {
    ...state,
    isFetchingDetail: false,
    result: {
      type: 'success'
    }
  };
};

const applyFetchDetailError = (state, action) => {
  const { payload: { message } } = action;
  return {
    ...state,
    isFetchingDetail: false,
    result: {
      type: 'error',
      message
    }
  };
};

const applyAddDetail = (state, action) => {
  const { payload: { offer } } = action;
  return {
    ...state,
    detail: offer
  };
};

const applyFetchCreatedOffer = (state, action) => {
  return {
    ...state,
    isFetchingCreatedOffer: true,
    result: undefined
  };
};

const applyFetchCreatedOfferSuccess = (state, action) => {
  return {
    ...state,
    isFetchingCreatedOffer: false
  };
};

const applyFetchCreatedOfferError = (state, action) => {
  const { payload: { message } } = action;
  return {
    ...state,
    isFetchingCreatedOffer: false,
    result: {
      type: 'error',
      message
    }
  };
};

const applyFetchPartnerOffers = (state, action) => {
  return {
    ...state,
    result: undefined,
    isFetchingPartnerOffer: true,
    list: {
      size: 0,
      page: 0,
      values: []
    }
  };
};

const applyFetchPartnerOffersSuccess = (state, action) => {
  return {
    ...state,
    result: {
      type: 'success'
    },
    isFetchingPartnerOffer: false
  };
};

const applyFetchPartnerOffersError = (state, action) => {
  const { payload: { message } } = action;
  return {
    ...state,
    result: {
      type: 'error',
      message
    },
    isFetchingPartnerOffer: false
  };
};

const applyAddCreatedOffer = (state, action) => {
  const { payload: { offer } } = action;
  return {
    ...state,
    createdOffer: offer
  };
};

const applyUpdateOffer = (state, action) => {
  return {
    ...state,
    isFetchingDetail: true,
    result: undefined
  };
};

const applyUpdateOfferSuccess = (state, action) => {
  return {
    ...state,
    isFetchingDetail: false
  };
};

const applyUpdateOfferError = (state, action) => {
  const { payload: { message } } = action;
  return {
    ...state,
    isFetchingDetail: false,
    result: {
      type: 'error',
      message
    }
  };
};

const applyCreateOffer = (state, action) => {
  return {
    ...state,
    isCreatingOffer: true,
    result: undefined
  }
};

const applyCreateOfferSuccess = (state, action) => {
  return {
    ...state,
    isCreatingOffer: false,
    result: {
      type: 'success'
    }
  };
};

const applyCreateOfferError = (state, action) => {
  const { payload: { message } } = action;
  return {
    ...state,
    isCreatingOffer: false,
    result: {
      type: 'error',
      message
    }
  };
};

const applyAcceptOffer = (state, action) => {
  return {
    ...state,
    isAcceptingOffer: true,
    result: undefined
  };
};

const applyAcceptOfferSuccess = (state, action) => {
  return {
    ...state,
    isAcceptingOffer: false
  };
};

const applyAcceptOfferError = (state, action) => {
  const { payload: { message } } = action;
  return {
    ...state,
    isAcceptingOffer: false,
    result: {
      type: 'error',
      message
    }
  };
};

const reducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case offerAction.CLEAR_RESULT: {
      return applyClearResult(state, action);
    }

    case offerAction.FETCH_OFFERS: {
      return applyFetchOffers(state, action);
    }

    case offerAction.FETCH_OFFERS_SUCCESS: {
      return applyFetchOffersSuccess(state, action);
    }

    case offerAction.FETCH_OFFERS_ERROR: {
      return  applyFetchOffersError(state, action);
    }

    case offerAction.ADD_OFFERS: {
      return  applyAddOffers(state, action);
    }

    case offerAction.FETCH_DETAIL: {
      return applyFetchDetail(state, action);
    }

    case offerAction.FETCH_DETAIL_SUCCESS: {
      return applyFetchDetailSuccess(state, action);
    }

    case offerAction.FETCH_DETAIL_ERROR: {
      return applyFetchDetailError(state, action);
    }

    case offerAction.ADD_DETAIL: {
      return applyAddDetail(state, action);
    }

    case offerAction.FETCH_CREATED_OFFER: {
      return applyFetchCreatedOffer(state, action);
    }

    case offerAction.FETCH_CREATED_OFFER_SUCCESS: {
      return applyFetchCreatedOfferSuccess(state, action);
    }

    case offerAction.FETCH_CREATED_OFFER_ERROR: {
      return applyFetchCreatedOfferError(state, action);
    }

    case offerAction.FETCH_PARTNER_OFFER: {
      return applyFetchPartnerOffers(state, action);
    }

    case offerAction.FETCH_PARTNER_OFFER_SUCCESS: {
      return applyFetchPartnerOffersSuccess(state, action);
    }

    case offerAction.FETCH_PARTNER_OFFER_ERROR: {
      return applyFetchPartnerOffersError(state, action);
    }

    case offerAction.ADD_CREATED_OFFER: {
      return applyAddCreatedOffer(state, action);
    }

    case offerAction.UPDATE_OFFER: {
      return applyUpdateOffer(state, action);
    }

    case offerAction.UPDATE_OFFER_SUCCESS: {
      return applyUpdateOfferSuccess(state, action);
    }

    case offerAction.UPDATE_OFFER_ERROR: {
      return applyUpdateOfferError(state, action);
    }

    case offerAction.CREATE_OFFER: {
      return applyCreateOffer(state, action);
    }

    case offerAction.CREATE_OFFER_SUCCESS: {
      return applyCreateOfferSuccess(state, action);
    }

    case offerAction.CREATE_OFFER_ERROR: {
      return applyCreateOfferError(state, action);
    }

    case offerAction.ACCEPT_OFFER: {
      return applyAcceptOffer(state, action);
    }

    case offerAction.ACCEPT_OFFER_SUCCESS: {
      return applyAcceptOfferSuccess(state, action);
    }

    case offerAction.ACCEPT_OFFER_ERROR: {
      return applyAcceptOfferError(state, action);
    }

    default:
      return state;
  }
};

export default reducer;
