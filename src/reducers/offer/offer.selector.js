import { pick } from 'ramda';

const getStatus = ({ offerState }) =>
  pick(['isLoading', 'isSuccess', 'isFailed', 'message'])(offerState);

const getUser = ({ offerState }) => offerState.user;

const getOffer = ({ offerState }) => offerState.offer;

const getIsFetchingOffers = ({ offerState }) => offerState.isFetchingOffers;

const getOffers = ({ offerState }) => offerState.list;

const getIsFetchingDetail = ({ offerState }) => offerState.isFetchingDetail;

const getDetail = ({ offerState }) => offerState.detail;

const getResult = ({ offerState }) => offerState.result;

const getIsFetchingCreatedOffer = ({ offerState }) => offerState.isFetchingCreatedOffer;

const getCreatedOffer = ({ offerState }) => offerState.createdOffer;

const getIsCreatingOffer = ({ offerState }) => offerState.isCreatingOffer;

const getIsAcceptingOffer = ({ offerState }) => offerState.isAcceptingOffer;

const getSize = ({ offerState }) => offerState.list.size;

export {
  getStatus,
  getUser,
  getOffer,
  getIsFetchingOffers,
  getOffers,
  getIsFetchingDetail,
  getDetail,
  getResult,
  getIsFetchingCreatedOffer,
  getCreatedOffer,
  getIsCreatingOffer,
  getIsAcceptingOffer,
  getSize
};
