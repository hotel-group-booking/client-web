const CLEAR_RESULT = '@@request/CLEAR_RESULT';
const CREATE_REQUEST = '@@request/CREATE_REQUEST';
const CREATE_REQUEST_SUCCESS = '@@request/CREATE_REQUEST_SUCCESS';
const CREATE_REQUEST_ERROR = '@@request/CREATE_REQUEST_ERROR';
const FETCH_REQUESTS = '@@request/FETCH_REQUESTS';
const FETCH_REQUESTS_SUCCESS = '@@request/FETCH_REQUEST_SUCCESS';
const FETCH_REQUESTS_ERROR = '@@request/FETCH_REQUEST_ERROR';
const ADD_REQUESTS_TO_LIST = '@@request/ADD_REQUESTS_TO_LIST';
const FETCH_DETAIL = '@@request/FETCH_DETAIL';
const FETCH_DETAIL_SUCCESS = '@@request/FETCH_DETAIL_SUCCESS';
const FETCH_DETAIL_ERROR = '@@request/FETCH_DETAIL_ERROR';
const ADD_DETAIL = '@@request/ADD_DETAIL';
const CLEAR_DETAIL = '@@request/CLEAR_DETAIL';
const SEARCH_REQUEST = '@@request/SEARCH_REQUEST';
const SEARCH_REQUEST_SUCCESS = '@@request/SEARCH_REQUEST_SUCCESS';
const SEARCH_REQUEST_ERROR = '@@request/SEARCH_REQUEST_ERROR';
const UPDATE = '@@request/UPDATE';
const UPDATE_DONE = '@@request/UPDATE_DONE';
const UPDATE_FAILED = '@@request/UPDATE_FAILED';
const SELECT = '@@request/SELECT';
const SAVE_REQUEST = '@@request/SAVE_REQUEST';
const FETCH_COUNT = '@@request/FETCH_COUNT';
const FETCH_COUNT_DONE = '@@request/FETCH_COUNT_DONE';
const RESET = '@@request/RESET';

export default {
  CLEAR_RESULT,
  CREATE_REQUEST,
  CREATE_REQUEST_SUCCESS,
  CREATE_REQUEST_ERROR,
  FETCH_REQUESTS,
  FETCH_REQUESTS_SUCCESS,
  FETCH_REQUESTS_ERROR,
  ADD_REQUESTS_TO_LIST,
  FETCH_DETAIL,
  FETCH_DETAIL_SUCCESS,
  FETCH_DETAIL_ERROR,
  ADD_DETAIL,
  CLEAR_DETAIL,
  SEARCH_REQUEST,
  SEARCH_REQUEST_SUCCESS,
  SEARCH_REQUEST_ERROR,
  UPDATE,
  UPDATE_DONE,
  UPDATE_FAILED,
  SELECT,
  SAVE_REQUEST,
  FETCH_COUNT,
  FETCH_COUNT_DONE,
  RESET
};


const doCreateRequest = request => ({
  type: CREATE_REQUEST,
  payload: {
    request
  }
});

const doCreateRequestSuccess = id => ({
  type: CREATE_REQUEST_SUCCESS,
  payload: {
    id
  }
});

const doCreateRequestError = message => ({
  type: CREATE_REQUEST_ERROR,
  payload: {
    message
  }
});

const doClearResult = () => ({
  type: CLEAR_RESULT
});

const doFetchRequests = (page, id) => ({
  type: FETCH_REQUESTS,
  payload: {
    page,
    id
  }
});

const doFetchRequestSuccess = () => ({
  type: FETCH_REQUESTS_SUCCESS,
});

const doFetchREquestError = message => ({
  type: FETCH_REQUESTS_ERROR,
  payload: {
    message
  }
});

const doAddRequestToList = (requests, page, size) => ({
  type: ADD_REQUESTS_TO_LIST,
  payload: {
    requests,
    page,
    size
  }
});

const doFetchDetail = id => ({
  type: FETCH_DETAIL,
  payload: {
    id
  }
});

const doFetchDetailSuccess = () => ({
  type: FETCH_DETAIL_SUCCESS
});

const doFetchDetailError = message => ({
  type: FETCH_DETAIL_ERROR,
  payload: {
    message
  }
});

const doAddDetail = request => ({
  type: ADD_DETAIL,
  payload: {
    request
  }
});


const doClearDetail = () => ({
  type: CLEAR_DETAIL
});

const doSearchRequest = (page, filter) => ({
  type: SEARCH_REQUEST,
  payload: {
    page,
    filter
  }
});

const doSearchRequestSuccess = () => ({
  type: SEARCH_REQUEST_SUCCESS,
});

const doSearchRequestError = message => ({
  type: SEARCH_REQUEST_ERROR,
  payload: {
    message
  }
});

const doUpdate = (id, data, userId) => ({
  type: UPDATE,
  payload: {
    id,
    data,
    userId
  }
});

const doUpdateDone = () => ({
  type: UPDATE_DONE
});

const doUpdateFailed = message => ({
  type: UPDATE_FAILED,
  payload: {
    message
  }
});

const doSelect = request => ({
  type: SELECT,
  payload: {
    request
  }
});

const doSaveRequest = request => ({
  type: SAVE_REQUEST,
  payload: {
    request
  }
});

const doFetchCount = id => ({
  type: FETCH_COUNT,
  payload: {
    id
  }
});

const doFetchCountDone = count => ({
  type: FETCH_COUNT_DONE,
  payload: {
    count
  }
});

const doReset = () => ({
  type: RESET
});

export {
  doClearResult,
  doCreateRequest,
  doCreateRequestSuccess,
  doCreateRequestError,
  doFetchRequests,
  doFetchRequestSuccess,
  doFetchREquestError,
  doAddRequestToList,
  doFetchDetail,
  doFetchDetailSuccess,
  doFetchDetailError,
  doAddDetail,
  doClearDetail,
  doSearchRequest,
  doSearchRequestSuccess,
  doSearchRequestError,
  doUpdate,
  doUpdateDone,
  doUpdateFailed,
  doSelect,
  doSaveRequest,
  doFetchCount,
  doFetchCountDone,
  doReset
};
