import requestAction from './request.action';
// import { uniqBy, prop } from 'ramda';

const INITIAL_STATE = {
  isCreatingRequest: false,
  isFetchingRequests: true,
  isFetchingDetail: false,
  isSearchingRequest: false,
  isUpdating: false,
  list: {
    page: 0,
    size: 0,
    values: []
  },
  detail: undefined,
  selected: undefined,
  result: undefined,
  savedRequest: undefined,
  count: {

  }
};


const applyClearResult = (state, action) => {
  return {
    ...state,
    result: undefined
  };
};

const applyCreateRequest = (state, action) => {
  return {
    ...state,
    isCreatingRequest: true,
    result: undefined
  };
};

const applyCreateRequestSuccess = (state, action) => {
  const { payload: { id } } = action;
  return {
    ...state,
    isCreatingRequest: false,
    result: {
      type: 'success',
      id
    }
  };
};

const applyCreateRequestError = (state, action) => {
  const { payload: { message } } = action;
  return {
    ...state,
    isCreatingRequest: false,
    result: {
      type: 'error',
      message
    }
  };
};

const applyFetchRequests = (state, action) => {
  return {
    ...state,
    isFetchingRequests: true,
    result: undefined,
    list: {
      page: 0,
      size: 0,
      values: []
    }
  };
};

const applyFetchRequestsSuccess = (state, action) => {
  return {
    ...state,
    isFetchingRequests: false,
    // result: {
    //   type: 'success'
    // }
  };
};

const applyFetchRequestsError = (state, action) => {
  return {
    ...state,
    isFetchingRequests: false,
    result: {
      type: 'error'
    }
  };
};

const applyAddRequestToList = (state, action) => {
  const { payload: { requests, page, size } } = action;
  return {
    ...state,
    list: {
      page,
      size,
      values: requests
    }
  };
};

const applyFetchDetail = (state, action) => {
  return {
    ...state,
    isFetchingDetail: true,
    result: undefined,
    detail: undefined
  };
};

const applyFetchDetailSuccess = (state, action) => {
  return {
    ...state,
    isFetchingDetail: false,
    result: {
      type: 'success'
    }
  };
};

const applyFetchDetailError = (state, action) => {
  const { payload: { message } } = action;
  return {
    ...state,
    isFetchingDetail: false,
    result: {
      type: 'error',
      message
    }
  };
};

const applyAddDetail = (state, action) => {
  const { payload: { request } } = action;
  return {
    ...state,
    detail: request
  };
};

const applyClearDetail = (state, action) => {
  return {
    ...state,
    detail: undefined
  };
};

const applySearchRequest = (state, action) => {
  return {
    ...state,
    isSearchingRequest: true,
    result: undefined
  };
};

const applySearchRequestSuccess = (state, action) => {
  return {
    ...state,
    isSearchingRequest: false
  };
};

const applySearchRequestError = (state, action) => {
  const { payload: { message } } = action;
  return {
    ...state,
    isSearchingRequest: false,
    result: {
      type: 'error',
      message
    }
  };
};

const applyUpdate = (state, action) => {
  return {
    ...state,
    isUpdating: true
  };
};

const applyUpdateDone = (state, action) => {
  return {
    ...state,
    isUpdating: false
  };
};

const applyUpdateFailed = (state, action) => {
  const { payload: { message } } = action;
  return {
    ...state,
    isUpdating: false,
    result: {
      type: 'error',
      message
    }
  };
};

const applySelect = (state, action) => {
  const { payload: { request } } = action;
  return {
    ...state,
    selected: request
  };
};

const applySaveRequest = (state, action) => {
  const { payload: { request } } = action;
  return {
    ...state,
    savedRequest: request
  };
};

const applyFetchCountDone = (state, action) => {
  const { payload: { count } } = action;
  return {
    ...state,
    count
  }
}

const reducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case requestAction.CLEAR_RESULT: {
      return applyClearResult(state, action);
    }

    case requestAction.CREATE_REQUEST: {
      return applyCreateRequest(state, action);
    }

    case requestAction.CREATE_REQUEST_SUCCESS: {
      return applyCreateRequestSuccess(state, action);
    }

    case requestAction.CREATE_REQUEST_ERROR: {
      return applyCreateRequestError(state, action);
    }

    case requestAction.FETCH_REQUESTS: {
      return applyFetchRequests(state, action);
    }

    case requestAction.FETCH_REQUESTS_SUCCESS: {
      return applyFetchRequestsSuccess(state, action);
    }

    case requestAction.FETCH_REQUESTS_ERROR: {
      return applyFetchRequestsError(state, action)
    }

    case requestAction.ADD_REQUESTS_TO_LIST: {
      return applyAddRequestToList(state, action);
    }

    case requestAction.FETCH_DETAIL: {
      return applyFetchDetail(state, action);
    }

    case requestAction.FETCH_DETAIL_SUCCESS: {
      return applyFetchDetailSuccess(state, action);
    }

    case requestAction.FETCH_DETAIL_ERROR: {
      return applyFetchDetailError(state, action);
    }

    case requestAction.ADD_DETAIL: {
      return applyAddDetail(state, action);
    }

    case requestAction.CLEAR_DETAIL: {
      return applyClearDetail(state, action);
    }

    case requestAction.SEARCH_REQUEST: {
      return applySearchRequest(state, action);
    }

    case requestAction.SEARCH_REQUEST_SUCCESS: {
      return applySearchRequestSuccess(state, action);
    }

    case requestAction.SEARCH_REQUEST_ERROR: {
      return applySearchRequestError(state, action);
    }

    case requestAction.UPDATE: {
      return applyUpdate(state, action);
    }

    case requestAction.UPDATE_DONE: {
      return applyUpdateDone(state, action);
    }

    case requestAction.UPDATE_FAILED: {
      return applyUpdateFailed(state, action);
    }

    case requestAction.SELECT: {
      return applySelect(state, action);
    }

    case requestAction.SAVE_REQUEST: {
      return applySaveRequest(state, action);
    }

    case requestAction.FETCH_COUNT_DONE: {
      return applyFetchCountDone(state, action);
    }

    case requestAction.RESET: {
      return INITIAL_STATE;
    }

    default: return state;
  }
};

export default reducer;