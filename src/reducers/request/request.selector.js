import { clone, filter } from 'ramda';

const getDetail = ({ requestState }) => requestState.detail;

const getResult = ({ requestState }) => requestState.result;

const getIsCreatingRequest = ({ requestState }) =>
  requestState.isCreatingRequest;

const getList = ({ requestState }) => requestState.list;

const getActiveRequests = ({ requestState }) => {
  const list = clone(requestState.list);
  list.values = filter(req => {
    const date = new Date(req.checkin);
    const now = new Date();
    return (date - now > 0) && req.active;
  })(requestState.list.values);
  return list;
};

const getInactiveRequests = ({ requestState }) => {
  const list = clone(requestState.list);
  list.values = filter(req => !req.active)(requestState.list.values);
  return list;
};

const getTimeoutRequests = ({ requestState }) => {
  const list = clone(requestState.list);
  list.values = filter(req => {
    const date = new Date(req.checkin);
    const now = new Date();
    return (date - now <= 0) && req.active;
  })(requestState.list.values);
  return list;
};

const getIsFetchingRequests = ({ requestState }) =>
  requestState.isFetchingRequests;

const getIsFetchingDetail = ({ requestState }) => requestState.isFetchingDetail;

const getIsUpdating = ({ requestState }) => requestState.isUpdating;

const getSelected = ({ requestState }) => requestState.selected;

const getSavedRequest = ({ requestState }) => requestState.savedRequest;

const getSize = ({ requestState }) => requestState.list.size;

const getIsSearchingRequest = ({ requestState }) =>
  requestState.isSearchingRequest;

const getCount = ({ requestState }) => requestState.count;

export {
  getDetail,
  getResult,
  getIsCreatingRequest,
  getList,
  getIsFetchingRequests,
  getIsFetchingDetail,
  getIsUpdating,
  getSelected,
  getSavedRequest,
  getSize,
  getIsSearchingRequest,
  getActiveRequests,
  getInactiveRequests,
  getTimeoutRequests,
  getCount
};
