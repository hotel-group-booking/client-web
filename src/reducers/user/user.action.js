const CLEAR_RESULT = '@@user/CLEAR_RESULT';
const FETCH_DATA = '@@user/FETCH_DATA';
const FETCH_DATA_SUCCESS = '@@user/FETCH_DATA_SUCCESS';
const FETCH_DATA_ERROR = '@@user/FETCH_DATA_ERROR';
const ADD_DATA = '@@user/ADD_DATA';
const REGISTRATION = '@@user/REGISTRATION';
const REGISTRATION_SUCCESS = '@@user/REGISTRATION_SUCCESS';
const REGISTRATION_ERROR = '@@user/REGISTRATION_ERROR';
const UPDATE = '@@user/UPDATE';
const UPDATE_SUCCESS = '@@user/UPDATE_SUCCESS';
const UPDATE_ERROR = '@@user/UPDATE_ERROR';
const CHANGE_PASSWORD = '@@user/CHANGE_PASSWORD';
const CHANGE_PASSWORD_SUCCESS = '@@user/CHANGE_PASSWORD_SUCCESS';
const CHANGE_PASSWORD_ERROR = '@@user/CHANGE_PASSWORD_ERROR';
const UPDATE_HOTEL_INFO = '@@user/UPDATE_HOTEL_INFO';
const UPDATE_HOTEL_INFO_DONE = '@@user/UPDATE_HOTEL_INFO_DONE';
const UPDATE_HOTEL_INFO_FAILED = '@@user/UPDATE_HOTEL_INFO_FAILED';
const UPLOAD_PHOTO = '@@user/UPLOAD_PHOTO';
const UPLOAD_PHOTO_DONE = '@@user/UPLOAD_PHOTO_DONE';
const UPLOAD_PHOTO_FAIL = '@@user/UPLOAD_PHOTO_FAIL';
const UPLOAD_AVATAR = '@@user/UPLOAD_AVATAR';
const UPLOAD_AVATAR_DONE = '@@user/UPLOAD_AVATAR_DONE';
const UPLOAD_AVATAR_FAILED = '@@user/UPLOAD_AVATAR_FAILED';

export default {
  FETCH_DATA,
  FETCH_DATA_SUCCESS,
  FETCH_DATA_ERROR,
  ADD_DATA,
  CLEAR_RESULT,
  REGISTRATION,
  REGISTRATION_SUCCESS,
  REGISTRATION_ERROR,
  UPDATE,
  UPDATE_SUCCESS,
  UPDATE_ERROR,
  CHANGE_PASSWORD,
  CHANGE_PASSWORD_SUCCESS,
  CHANGE_PASSWORD_ERROR,
  UPDATE_HOTEL_INFO,
  UPDATE_HOTEL_INFO_DONE,
  UPDATE_HOTEL_INFO_FAILED,
  UPLOAD_PHOTO,
  UPLOAD_PHOTO_DONE,
  UPLOAD_PHOTO_FAIL,
  UPLOAD_AVATAR,
  UPLOAD_AVATAR_DONE,
  UPLOAD_AVATAR_FAILED,
};

const doClearResult = () => ({
  type: CLEAR_RESULT
});

const doFetchData = id => ({
  type: FETCH_DATA,
  payload: {
    id
  }
});

const doFetchDataSuccess = () => ({
  type: FETCH_DATA_SUCCESS
});

const doFetchDataError = message => ({
  type: FETCH_DATA_ERROR,
  payload: {
    message
  }
});

const doAddData = data => ({
  type: ADD_DATA,
  payload: {
    data
  }
});

const doRegistration = (user, hotel) => ({
  type: REGISTRATION,
  payload: {
    user,
    hotel
  }
});

const doRegistrationSuccess = () => ({
  type: REGISTRATION_SUCCESS,
});

const doRegistrationError = message => ({
  type: REGISTRATION_ERROR,
  payload: {
    message
  }
});

const doUpdate = (id, data) => ({
  type: UPDATE,
  payload: {
    id,
    data
  }
});

const doUpdateSuccess = () => ({
  type: UPDATE_SUCCESS
});

const doUpdateError = message => ({
  type: UPDATE_ERROR,
  payload: {
    message
  }
});

const doChangePassword = (id, data) => ({
  type: CHANGE_PASSWORD,
  payload: {
    id,
    data
  }
});

const doChangePasswordSuccess = () => ({
  type: CHANGE_PASSWORD_SUCCESS,
});

const doChangePasswordError = message => ({
  type: CHANGE_PASSWORD_ERROR,
  payload: {
    message
  }
});

const doUpdateHotelInfo = (id ,hotelInfo) => ({
  type: UPDATE_HOTEL_INFO,
  payload: {
    id, 
    hotelInfo
  }
});

const doUpdateHotelInfoDone = () => ({
  type: UPDATE_HOTEL_INFO_DONE
});

const doUpdateHotelInfoFailed = message => ({
  type: UPDATE_HOTEL_INFO_FAILED,
  payload: {
    message
  }
});

const doUploadPhoto = (id, images) => ({
  type: UPLOAD_PHOTO,
  payload: {
    id,
    images
  }
});

const doUploadPhotoDone = () => ({
  type: UPLOAD_PHOTO_DONE
});

const doUploadPhotoFail = message => ({
  type: UPLOAD_PHOTO_FAIL,
  payload: {
    message
  }
});

const doUploadAvatar = (id, image) => ({
  type: UPLOAD_AVATAR,
  payload: {
    id,
    image
  }
});

const doUploadAvatarDone = () => ({
  type: UPLOAD_AVATAR_DONE
});

const doUploadAvatarFailed = message => ({
  type: UPLOAD_AVATAR_FAILED,
  payload: {
    message
  }
});

export {
  doClearResult,
  doFetchData,
  doFetchDataSuccess,
  doFetchDataError,
  doAddData,
  doRegistration,
  doRegistrationSuccess,
  doRegistrationError,
  doUpdate,
  doUpdateSuccess,
  doUpdateError,
  doChangePassword,
  doChangePasswordSuccess,
  doChangePasswordError,
  doUpdateHotelInfo,
  doUpdateHotelInfoDone,
  doUpdateHotelInfoFailed,
  doUploadPhoto,
  doUploadPhotoDone,
  doUploadPhotoFail,
  doUploadAvatar,
  doUploadAvatarDone,
  doUploadAvatarFailed
};
