import userAction from './user.action';

const INITIAL_STATE = {
  result: undefined,
  isFetchingData: false,
  isRegistering: false,
  isUpdating: false,
  isUpdatingHotelInfo: false,
  isUploadingPhoto: false,
  isUploadingAvatar: false,
  data: undefined
};

const applyClearResult = (state, action) => {
  return {
    ...state,
    result: undefined
  }
};

const applyFetchData = (state, action) => {
  return {
    ...state,
    result: undefined,
    isFetchingData: true
  }
};

const applyFetchDataSuccess = (state, action) => {
  return {
    ...state,
    isFetchingData: false,
    result: {
      type: 'success'
    }
  };
};

const applyFetchDataError = (state, action) => {
  const { payload: { message } } = action;
  return {
    ...state,
    isFetchingData: false,
    result: {
      type: 'error',
      message
    }
  }
};

const applyAddData = (state, action) => {
  const { payload: { data } } = action;
  return {
    ...state,
    data
  };
};

const applyRegistration = (state, action) => {
  return {
    ...state,
    isRegistering: true,
    result: undefined
  };
};

const applyRegistrationSuccess = (state, action) => {
  return {
    ...state,
    isRegistering: false,
    result: {
      type: 'success'
    }
  };
};

const applyRegistrationError = (state, action) => {
  const { payload: { message } } = action;
  return {
    ...state,
    isRegistering: false,
    result: {
      type: 'error',
      message
    }
  };
};

const applyUpdate = (state, action) => {
  return {
    ...state,
    isUpdating: true,
    result: undefined
  };
};

const applyUpdateSuccess = (state, action) => {
  return {
    ...state,
    isUpdating: false,
    result: {
      type: 'success'
    }
  };
};

const applyUpdateError = (state, action) => {
  const { payload: { message } } = action;
  return {
    ...state,
    isUpdating: false,
    result: {
      type: 'error',
      message
    }
  };
};

const applyChangePassword = (state, action) => {
  return {
    ...state,
    isChangingPassword: true,
    result: undefined
  };
};

const applyChangePasswordSuccess = (state, action) => {
  return {
    ...state,
    isChangingPassword: false,
    result: {
      type: 'success'
    }
  };
};

const applyChangePasswordError = (state, action) => {
  return {
    ...state,
    isChangingPassword: false,
    result: {
      type: 'error',
      message: action.payload.message
    }
  };
};

const applyUpdateHotelInfo = (state, action) => {
  return {
    ...state,
    isUpdatingHotelInfo: true,
    result: undefined
  };
};

const applyUpdateHotelInfoDone = (state, action) => {
  return {
    ...state,
    isUpdatingHotelInfo: false,
    result: {
      type: 'success'
    }
  };
};

const applyUpdateHotelInfoFailed = (state, action) => {
  const { payload: { message } } = action;
  return {
    ...state,
    isUpdatingHotelInfo: false,
    result: {
      type: 'error',
      message
    }
  }
};

const applyUploadPhoto = (state, action) => {
  return {
    ...state,
    isUploadingPhoto: true,
    result: undefined
  };
};

const applyUploadPhotoDone = (state, action) => {
  return {
    ...state,
    isUploadingPhoto: false,
    result: {
      type: 'success'
    }
  };
};

const applyUploadPhotoFailed = (state, action) => {
  const { payload: { message } } = action;
  return {
    ...state,
    isUploadingPhoto: false,
    result: {
      type: 'error',
      message
    }
  }
};

const applyUploadAvatar = (state, action) => {
  return {
    ...state,
    isUploadingAvatar: true,
    result: undefined
  };
};

const applyUploadAvatarDone = (state, action) => {
  return {
    ...state,
    isUploadingAvatar: false
  };
};

const applyUploadAvatarFailed = (state, action) => {
  const { payload : { message } } = action;
  return {
    ...state,
    isUploadingAvatar: false,
    result: {
      type: 'error',
      message
    }
  };
};

const reducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case userAction.CLEAR_RESULT: {
      return applyClearResult(state, action);
    }
    
    case userAction.FETCH_DATA: {
      return applyFetchData(state, action);
    }

    case userAction.FETCH_DATA_SUCCESS: {
      return applyFetchDataSuccess(state, action);
    }

    case userAction.FETCH_DATA_ERROR: {
      return applyFetchDataError(state, action);
    }

    case userAction.ADD_DATA: {
      return applyAddData(state, action);
    }

    case userAction.REGISTRATION: {
      return applyRegistration(state, action);
    }

    case userAction.REGISTRATION_SUCCESS: {
      return applyRegistrationSuccess(state, action);
    }

    case userAction.REGISTRATION_ERROR: {
      return applyRegistrationError(state, action);
    }

    case userAction.UPDATE: {
      return applyUpdate(state, action);
    }

    case userAction.UPDATE_SUCCESS: {
      return applyUpdateSuccess(state, action);
    }

    case userAction.UPDATE_ERROR: {
      return applyUpdateError(state, action);
    }

    case userAction.CHANGE_PASSWORD: {
      return applyChangePassword(state, action);
    }

    case userAction.CHANGE_PASSWORD_SUCCESS: {
      return applyChangePasswordSuccess(state, action);
    }

    case userAction.CHANGE_PASSWORD_ERROR: {
      return applyChangePasswordError(state, action);
    }

    case userAction.UPDATE_HOTEL_INFO: {
      return applyUpdateHotelInfo(state, action);
    }

    case userAction.UPDATE_HOTEL_INFO_DONE: {
      return applyUpdateHotelInfoDone(state, action);
    }

    case userAction.UPDATE_HOTEL_INFO_FAILED: {
      return applyUpdateHotelInfoFailed(state, action);
    }

    case userAction.UPLOAD_PHOTO: {
      return applyUploadPhoto(state, action);
    }

    case userAction.UPLOAD_PHOTO_DONE: {
      return applyUploadPhotoDone(state, action);
    }

    case userAction.UPLOAD_PHOTO_FAIL: {
      return applyUploadPhotoFailed(state, action);
    }

    case userAction.UPLOAD_AVATAR: {
      return applyUploadAvatar(state, action);
    }

    case userAction.UPLOAD_AVATAR_DONE: {
      return applyUploadAvatarDone(state, action);
    }

    case userAction.UPLOAD_AVATAR_FAILED: {
      return applyUploadAvatarFailed(state, action);
    }
    
    default: return state;
  }
};

export default reducer;