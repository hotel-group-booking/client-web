const getUser = ({ userState }) => userState.data;

const getIsRegistering = ({ userState }) => userState.isRegistering;

const getResult = ({ userState }) => userState.result;

const getIsUpdating = ({ userState }) => userState.isUpdating;

const getIsChangingPassword = ({ userState }) => userState.isChangingPassword;

const getIsUploadingAvatar = ({ userState }) => userState.isUploadingAvatar;

const getIsFetchingData = ({ userState }) => userState.isFetchingData;

export {
  getUser,
  getIsRegistering,
  getResult,
  getIsUpdating,
  getIsChangingPassword,
  getIsUploadingAvatar,
  getIsFetchingData
};
