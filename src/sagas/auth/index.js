import { fork, all } from 'redux-saga/effects';
import loginWatcher from './login';
import logoutWatcher from './logout';
import tokenVerifyWatcher from './token-verify';

function* authSaga() {
  yield all([
    fork(loginWatcher),
    fork(logoutWatcher),
    fork(tokenVerifyWatcher)
  ])
};

export default authSaga;