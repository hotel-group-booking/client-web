import { fork, call, take, put, delay } from 'redux-saga/effects';
import { doFetchData } from '../../reducers/user';
import { auth, requests } from '../../api';
import {
  authAction,
  doLoginError,
  doLoginSuccess,
  doClearResult
} from '../../reducers/auth';
import { doSaveRequest } from '../../reducers/request';

function* clearResult() {
  yield delay(4000);
  yield put(doClearResult());
};

function* authorize(username, password, savedRequest) {
  try {
    const response = yield call(auth.login(), {
      body: {
        username,
        password
      }
    });

    localStorage.setItem('token', response.token);
    localStorage.setItem('role', response.role);
    localStorage.setItem('active', response.active);
    localStorage.setItem('isAuth', true);

    yield put(doLoginSuccess());
    if (savedRequest) {
      yield call(requests.post(response.token), {
        body: savedRequest
      });
      yield put(doSaveRequest(undefined));
    }
    yield fork(clearResult);
    yield put(doFetchData(response._id));
  } catch (error) {
    yield put(doLoginError(error.message));
    yield fork(clearResult);
  }
}

export default function* login() {
  while (true) {
    const {
      payload: { username, password, savedRequest }
    } = yield take(authAction.LOGIN);
    yield fork(authorize, username, password, savedRequest);
  }
}
