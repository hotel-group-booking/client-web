import { take, call, fork, delay, put } from 'redux-saga/effects';
import { authAction } from '../../reducers/auth';
import { doAddData } from '../../reducers/user';
import { doReset } from '../../reducers/request';
import { auth } from '../../api';

function* logout() {
  try {
    yield put(doAddData(undefined));
    yield call(auth.logout(), {
      body: {
        token: localStorage.getItem('token')
      }
    });
    localStorage.clear();
    yield put(doReset())
  } catch (error) {
    yield delay(2000);
    yield call(auth.logout(), {
      body: {
        token: localStorage.getItem('token')
      }
    });
    localStorage.clear();
  }
};

function* logoutWatcher() {
  while(true) {
    yield take(authAction.LOGOUT);
    yield fork(logout);
  }
};

export default logoutWatcher;