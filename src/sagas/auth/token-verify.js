import { put, call, fork, take, delay } from 'redux-saga/effects';
import {
  authAction,
  doTokenVerifySuccess,
  doTokenVerifyError,
  doClearResult
} from '../../reducers/auth';
import { doFetchData } from '../../reducers/user';
import { auth } from '../../api';

function* tokenVerify(token) {
  try {
    const response = yield call(auth.tokenVerify());

    localStorage.setItem('token', response.token);
    localStorage.setItem('role', response.role);
    localStorage.setItem('active', response.active);
    localStorage.setItem('isAuth', true);

    yield put(doTokenVerifySuccess());
    yield put(doFetchData(response._id));
    yield delay(4000);
    yield put(doClearResult());
  } catch (error) {
    yield put(doTokenVerifyError(error.message));
  }
}

export default function* tokenVerifyWatcher() {
  while (true) {
    const { payload } = yield take(authAction.TOKEN_VERIFY);
    yield fork(tokenVerify, payload.token);

    yield take(authAction.TOKEN_VERIFY_ERROR);
    localStorage.clear();
  }
}
