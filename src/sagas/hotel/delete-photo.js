import { take, fork, call, put } from 'redux-saga/effects';
import {
  hotelAction,
  doDeletePhotoDone,
  doDeletePhotoFailed
} from '../../reducers/hotel';
import { doFetchData } from '../../reducers/user';
import { hotels } from '../../api';

function* deletePhoto(user, hotel, photo) {
  try {
    yield call(hotels.deletePhoto(hotel, photo));
    yield put(doDeletePhotoDone());
    yield put(doFetchData(user));
  } catch (error) {
    yield put(doDeletePhotoFailed(error.message));
  }
}

export default function* deletePhotoWatcher() {
  while (true) {
    const { payload } = yield take(hotelAction.DELETE_PHOTO);
    yield fork(deletePhoto, payload.userId, payload.hotelId, payload.photoId);
  }
}
