import { fork, call, put, take, delay } from 'redux-saga/effects';
import { hotels } from '../../api';
import {
  hotelAction,
  doFetchHotelSuccess,
  doFetchHotelError,
  doClearResult
} from '../../reducers/hotel';

function* clearResult() {
  yield delay(4000);
  yield put(doClearResult());
}

function* fetchHotel(id) {
  try {
    const response = yield call(hotels.get(id));
    yield put(doFetchHotelSuccess(response));
    yield fork(clearResult);
  } catch (err) {
    yield put(doFetchHotelError(err.message));
  }
}

export default function* updateWatcher() {
  while (true) {
    const { payload } = yield take(hotelAction.FETCH_HOTEL);
    yield fork(fetchHotel, payload.id);
  }
};
