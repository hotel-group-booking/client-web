import { all, fork } from 'redux-saga/effects';
import uploadPhotos from './upload-photos';
import update from './update';
import fetchHotel from './fetch-hotel';
import deletePhoto from './delete-photo';

export default function* hotelSaga() {
  yield all([
    fork(uploadPhotos),
    fork(update),
    fork(fetchHotel),
    fork(deletePhoto)
  ]);
};