import { fork, call, put, take, delay } from 'redux-saga/effects';
import { hotels } from '../../api';
import {
  hotelAction,
  doUpdateDone,
  doUpdateFailed,
  doClearResult
} from '../../reducers/hotel';
import { doFetchData } from '../../reducers/user';

function* clearResult() {
  yield delay(4000);
  yield put(doClearResult());
}

function* update(userId, hotelId, data) {
  try {
    yield call(hotels.put(hotelId), {
      body: data
    });
    yield put(doUpdateDone());
    yield fork(clearResult);
    yield put(doFetchData(userId));
  } catch (err) {
    yield put(doUpdateFailed(err.message));
  }
}

export default function* updateWatcher() {
  while (true) {
    const { payload } = yield take(hotelAction.UPDATE);
    yield fork(update, payload.userId, payload.hotelId, payload.data);
  }
};
