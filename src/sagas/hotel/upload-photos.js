import { take, fork, call, put } from 'redux-saga/effects';
import {
  hotelAction,
  doUploadPhotosDone,
  doUploadPhotosFailed
} from '../../reducers/hotel';
import { split, map } from 'ramda';
import { hotels } from '../../api';
import { doFetchData } from '../../reducers/user';

function* uploadPhotos(userId, hotelId, images) {
  try {
    yield call(hotels.uploadPhotos(hotelId), {
      body: {
        images: map(image => split(',', image)[1])(images)
      }
    });
    yield put(doUploadPhotosDone());
    yield put(doFetchData(userId));
  } catch (error) {
    yield put(doUploadPhotosFailed(error.message));
  }
}

export default function* uploadPhotosWatcher() {
  while (true) {
    const { payload } = yield take(hotelAction.UPLOAD_PHOTOS);
    yield fork(uploadPhotos, payload.userId, payload.hotelId, payload.images);
  }
};
