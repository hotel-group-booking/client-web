import { all, fork } from 'redux-saga/effects';
import authSaga from './auth';
import userSaga from './user';
import placeAutocompleteSaga from './place-autocomplete';
import requestSaga from './request';
import offerSaga from './offer';
import notificationSaga from './notification';
import hotelSaga from './hotel';
import mapSaga from './map';
import messageSaga from './messages';

function* rootSaga() {
  yield all([
    fork(authSaga),
    fork(userSaga),
    fork(placeAutocompleteSaga),
    fork(requestSaga),
    fork(offerSaga),
    fork(notificationSaga),
    fork(hotelSaga),
    fork(mapSaga),
    fork(messageSaga)
  ]);
};

export default rootSaga;
