import { fork, call, put, take } from 'redux-saga/effects';
import { getDragPlace } from '../../api';
import {
  mapAction,
  doDragPlaceDone,
  doDragPlaceFailed
} from '../../reducers/map';


function* dragPlace(lat, lng) {
  try {
    const result = yield call(getDragPlace, {
      lat,
      lng
    });
    yield put(doDragPlaceDone(result));
  } catch (err) {
    yield put(doDragPlaceFailed(err.message));
  }
}

export default function* updateWatcher() {
  while (true) {
    const { payload } = yield take(mapAction.DRAG_PLACE);
    yield fork(dragPlace, payload.lat, payload.lng);
  }
};
