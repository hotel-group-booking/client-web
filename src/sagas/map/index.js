import { all, fork } from 'redux-saga/effects';
import dragPlace from './drag-place';

export default function* mapSaga() {
  yield all([
    fork(dragPlace)
  ]);
};