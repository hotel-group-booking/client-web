import { eventChannel } from 'redux-saga';
import { connectMessage } from 'api';

const createMessageChannel = room => {
  const service = connectMessage(room);
  service.join();

  return eventChannel(emit => {
    service.onTyping(data => {
      emit(data);
    });

    service.onEndTyping(data => {
      emit(data);
    });

    service.onMessageHistory(data => {
      emit(data);
    });

    service.onMessage(data => {
      emit([data]);
    })

    return () => {
      service.disconnect();
    };
  });
};


export { 
  createMessageChannel
};