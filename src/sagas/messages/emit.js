import { call, take, fork } from 'redux-saga/effects';
import { messageAction } from 'reducers/message';
import { messages } from 'api';

function* emitWorker(room, event, data) {
  yield call(messages.emit(room, event), {
    body: data
  });
};

export default function* emitWatcher() {
  while (true) {
    const { payload } = yield take(messageAction.EMIT);
    yield fork(emitWorker, payload.room, payload.event, payload.data);
  }
}