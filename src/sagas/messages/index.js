import { all, fork } from 'redux-saga/effects';
import subcribe from './subcribe';
import send from './send';
import emit from './emit';

function* messageSaga() {
  yield all([
    fork(subcribe),
    fork(send),
    fork(emit)
  ]);
};

export default messageSaga;