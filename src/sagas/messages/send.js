import { call, take, fork } from 'redux-saga/effects';
import { messageAction } from 'reducers/message';
import { messages } from 'api';

function* sendWorker(message) {
  try {
    const response = yield call(messages.post(), {
      body: message
    });
    console.log(response);
  } catch (error) {
    console.log(error.message);
  }
}

export default function* sendWatcher() {
  while (true) {
    const { payload } = yield take(messageAction.SEND);
    yield fork(sendWorker, payload.message);
  }
}