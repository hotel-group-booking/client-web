import { call, take, fork, put } from 'redux-saga/effects';
import { createMessageChannel } from './channels';
import { messageAction, doAddMessages, doAddTyping } from 'reducers/message';

function* messageWorker(data) {
  if (Array.isArray(data)) {
    yield put(doAddMessages(data));
  } else {
    yield put(doAddTyping(data));
  }
};

export default function* subcribeWatcher() {
  const { payload } = yield take(messageAction.SUBCRIBE);
  const messageChannel = yield call(createMessageChannel, payload.room);

  while (true) {
    const data = yield take(messageChannel);
    yield fork(messageWorker, data);
  }
};

