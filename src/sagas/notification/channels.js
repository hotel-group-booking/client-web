import { eventChannel } from 'redux-saga';
import { connectNotification } from '../../api';

const createNotificationChannel = room => {
  const service = connectNotification(room);
  service.join();

  return eventChannel(emit => {
    service.onOfferUpdated(noti => {
      emit(noti);
    });

    service.onOfferAccepted(noti => {
      emit(noti);
    });

    service.onOfferBlocked(noti => {
      emit(noti);
    });

    service.onOfferNotification(noti => {
      emit(noti);
    });

    service.onRequestNotification(noti => {
      emit(noti);
    });
    
    return () => {
      service.disconnect();
    }
  });
};

export {
  createNotificationChannel
};