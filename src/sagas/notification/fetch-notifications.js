import { take, fork, put, call } from 'redux-saga/effects';
import {
  notificationAction,
  doFetchNotificationsDone,
  doFetchNotificationsFailed,
  doAddNotifications,
} from '../../reducers/notification';
import { notifications } from '../../api';

function* fetchNotifications(user) {
  try {
    const response = yield call(notifications.get(), {
      query: {
        user
      }
    });

    yield put(doFetchNotificationsDone());
    yield put(doAddNotifications(response.value));
  } catch (error) {
    doFetchNotificationsFailed(error.message);
  }
};

export default function* fetchNotificationWatcher() {
  while (true) {
    const { payload } = yield take(notificationAction.FETCH_NOTIFICATIONS);
    yield fork(fetchNotifications, payload.userId);
  }
};
