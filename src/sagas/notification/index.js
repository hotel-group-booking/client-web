import { all, fork } from 'redux-saga/effects';
import notiListener from './noti-listener';
import fetchNotifications from './fetch-notifications';
import update from './update'

function* notificationSaga() {
  yield all([
    fork(notiListener),
    fork(fetchNotifications),
    fork(update)
  ]);
};

export default notificationSaga;