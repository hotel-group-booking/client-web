import { call, take, fork, put } from 'redux-saga/effects';
import { createNotificationChannel } from './channels';
import {
  notificationAction,
  doAddNotification
} from '../../reducers/notification';

function* notificationWorker(noti) {
  yield put(doAddNotification(noti));
}

export default function* notiListener() {
  const { payload } = yield take(notificationAction.SUBCRIBE);
  const notificationChannel = yield call(
    createNotificationChannel,
    payload.room
  );

  while (true) {
    const noti = yield take(notificationChannel);
    yield fork(notificationWorker, noti);
  }
}
