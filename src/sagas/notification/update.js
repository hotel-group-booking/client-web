import { take, fork, call } from 'redux-saga/effects';
import { notificationAction } from '../../reducers/notification';
import { notifications } from '../../api';

function* update(id, body) {
  try {
    yield call(notifications.put(id), {
      body
    });
  } catch (err) {
    console.log(err.message);
  }
};

export default function* updateWatcher() {
  while(true) {
    const { payload } = yield take(notificationAction.UPDATE);
    yield fork(update, payload.id, payload.body);
  }
}