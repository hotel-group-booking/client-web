import { take, fork, call, put } from 'redux-saga/effects';
import { offers } from '../../api';
import {
  offerAction,
  doAcceptOfferSuccess,
  doAcceptOfferError,
  doAddDetail
} from '../../reducers/offer';

function* acceptOffer(id, accepted) {
  try {
    const response = yield call(offers.accept(id), {
      body: {
        accepted
      },
      query: {
        included: 'hotel,member'
      }
    });
    yield put(doAddDetail(response));
    yield put(doAcceptOfferSuccess());
  } catch (err) {
    yield put(doAcceptOfferError(err.message));
  }
};

export default function* acceptOfferWatcher() {
  while(true) {
    const { payload } = yield take(offerAction.ACCEPT_OFFER);
    yield fork(acceptOffer, payload.id, payload.accepted);
  }
};
