import { take, fork, call, put } from 'redux-saga/effects';
import { offers } from '../../api';
import {
  offerAction,
  doCreateOfferSuccess,
  doCreateOfferError,
  doAddCreatedOffer
} from '../../reducers/offer';

function* createOffer(offer) {
  try {
    const response = yield call(offers.post(), {
      body: offer,
      query: {
        included: 'member,hotel'
      }
    });
    yield put(doCreateOfferSuccess());
    yield put(doAddCreatedOffer(response));
  } catch (error) {
    yield put(doCreateOfferError(error.message));
  }
};

export default function* createOfferWatcher() {
  while (true) {
    const { payload } = yield take(offerAction.CREATE_OFFER);
    yield fork(createOffer, payload.offer);
  }
};