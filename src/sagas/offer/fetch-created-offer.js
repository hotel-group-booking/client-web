import { take, fork, call, put } from 'redux-saga/effects';
import {
  offerAction,
  doFetchCreatedOfferSuccess,
  doFetchCreatedOfferError,
  doAddCreatedOffer
} from '../../reducers/offer';
import { offers } from '../../api';

function* fetchCreatedOffer(partner, request) {
  try {
    const response = yield call(offers.get(), {
      query: {
        partner,
        request
      }
    });
    yield put(doFetchCreatedOfferSuccess());
    yield put(doAddCreatedOffer(response.value[0]));
  } catch (error) {
    yield put(doFetchCreatedOfferError(error.message));
  }
}

export default function* fetchCreatedOfferWatcher() {
  while (true) {
    const { payload } = yield take(offerAction.FETCH_CREATED_OFFER);
    yield fork(fetchCreatedOffer, payload.partner, payload.request);
  }
}