import { take, fork, call, put } from 'redux-saga/effects';
import {
  offerAction,
  doFetchDetailSuccess,
  doFetchDetailError,
  doAddDetail,
  doClearResult
} from '../../reducers/offer';
import { offers } from '../../api';

function* fetchDetail(id) {
  try {
    const response = yield call(offers.get(id), {
      query: {
        included: 'hotel,member'
      }
    });
    yield put(doAddDetail(response));
    yield put(doFetchDetailSuccess());
    yield put(doClearResult());
  } catch (error) {
    yield put(doFetchDetailError(error.message));
  }
}

export default function* fetchDetailWatcher() {
  while (true) {
    const { payload } = yield take(offerAction.FETCH_DETAIL);
    yield fork(fetchDetail, payload.id)
  }
}
