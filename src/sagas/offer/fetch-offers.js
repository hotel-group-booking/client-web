import { call, put, take, fork, delay } from 'redux-saga/effects';
import { offers } from '../../api';
import {
  offerAction,
  doFetchOffersErrors,
  doAddOffers,
  doFetchOffersSuccess,
  doClearResult
} from '../../reducers/offer';

function* fetchOffers(requestId, page) {
  try {
    const response = yield call(offers.get(), {
      query: {
        request: requestId,
        included: 'partner,hotel',
        page,
        active: true
      }
    });
    yield put(doAddOffers(response.value, page, response.size));
    yield put(doFetchOffersSuccess());
    yield delay(2000);
    yield put(doClearResult());
  } catch (error) {
    yield put(doFetchOffersErrors(error.message));
    yield delay(2000);
    yield put(doClearResult());
  }
};

export default function* fetchOfferWatcher() {
  while (true) {
    const { payload } = yield take(offerAction.FETCH_OFFERS);
    yield fork(fetchOffers, payload.requestId, payload.page);
  }
};
