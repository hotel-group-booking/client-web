import { call, put, take, fork, delay } from 'redux-saga/effects';
import { offers } from '../../api';
import {
  offerAction,
  doFetchPartnerOffersError,
  doAddOffers,
  doFetchPartnerOffersSuccess,
  doClearResult
} from '../../reducers/offer';

function* fetchPartnerOffers(partner, page) {
  try {
    const response = yield call(offers.get(), {
      query: {
        partner,
        included: 'partner,request',
        page
      }
    });
    yield put(doAddOffers(response.value, page, response.size));
    yield put(doFetchPartnerOffersSuccess());
    yield delay(2000);
    yield put(doClearResult());
  } catch (error) {
    yield put(doFetchPartnerOffersError(error.message));
    yield delay(2000);
    yield put(doClearResult());
  }
};

export default function* fetchPartnerOfferWatcher() {
  while (true) {
    const { payload } = yield take(offerAction.FETCH_PARTNER_OFFER);
    yield fork(fetchPartnerOffers, payload.partner, payload.page);
  }
};
