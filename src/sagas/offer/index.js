import { all, fork } from 'redux-saga/effects';
import fetchOffer from './fetch-offers';
import fetchDetail from './fetch-detail';
import fetchCreatedOffer from './fetch-created-offer';
import fetchPartnerOfferWatcher from './fetch-partner-offers';
import updateOffer from './update-offer';
import createOffer from './create-offer';
import acceptOffer from './accept-offer';

export default function* offerSaga() {
  yield all([
    fork(fetchOffer),
    fork(fetchDetail),
    fork(fetchCreatedOffer),
    fork(fetchPartnerOfferWatcher),
    fork(updateOffer),
    fork(createOffer),
    fork(acceptOffer)
  ]);
};