import { take, fork, call, put } from 'redux-saga/effects';
import { offers } from '../../api';
import {
  offerAction,
  doUpdateOfferSuccess,
  doUpdateOfferError,
  doAddDetail
} from '../../reducers/offer';

function* updateOffer(id, offer) {
  try {
    const response = yield call(offers.put(id), {
      body: offer,
      query: {
        included: 'partner,member'
      }
    });
    yield put(doAddDetail(response));
    yield put(doUpdateOfferSuccess());
  } catch (error) {
    yield put(doUpdateOfferError(error.message));
  }
};

export default function* updateOfferWatcher() {
  while (true) {
    const { payload } = yield take(offerAction.UPDATE_OFFER);
    yield fork(updateOffer, payload.id, payload.offer);
  }
};