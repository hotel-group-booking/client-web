import { call, put, takeLatest, all } from 'redux-saga/effects';
import { getPlacePredictions, getDetails } from '../../api';
import {
  doFetchPredictionsDone,
  doHasError,
  doSelectPlaceDone,
  autoCompleteAction
} from '../../reducers/auto-complete';

function* fetchPredictions(action) {
  try {
    const {
      payload: { input, sessionToken, typeAddress }
    } = action;
    const result = yield call(getPlacePredictions, {
      input,
      sessionToken,
      typeAddress
    });
    yield put(
      doFetchPredictionsDone({
        predictions: result,
        message: 'OK'
      })
    );
  } catch (error) {
    yield put(doHasError(error.message));
  }
}

function* selectPlace(action) {
  try {
    const {
      payload: { placeId, sessionToken }
    } = action;
    const result = yield call(getDetails, {
      placeId: placeId,
      sessionToken
    });
    yield put(doSelectPlaceDone(result, sessionToken));
  } catch (err) {
    yield put(doHasError(JSON.stringify(err)));
  }
};

export default function* placeAutocompleteSaga() {
  yield all([
    takeLatest(autoCompleteAction.FETCH_PREDICTIONS, fetchPredictions),
    takeLatest(autoCompleteAction.SELECT_PLACE, selectPlace)
  ]);
};
