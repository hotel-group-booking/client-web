import { fork, call, put, take } from 'redux-saga/effects';
import {
  requestAction,
  doCreateRequestSuccess,
  doCreateRequestError,
} from '../../reducers/request';
import { requests } from '../../api';

function* createRequest(request) {
  try {
    const response = yield call(requests.post(), {
      body: request
    });
    yield put(doCreateRequestSuccess(response._id));
  } catch (error) {
    yield put(doCreateRequestError(error.message));
  }
}

export default function* createRequestWatcher() {
  while (true) {
    const { payload } = yield take(requestAction.CREATE_REQUEST);
    yield fork(createRequest, payload.request);
  }
}
