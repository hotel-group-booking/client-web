import { take, fork, call, put } from 'redux-saga/effects';
import {
  requestAction,
  doFetchCountDone
} from 'reducers/request';
import { requests } from 'api';

function* fetchCount(id) {
  try {
    const response = yield call(requests.count(), {
      query: {
        member: id
      }
    });
    yield put(doFetchCountDone(response));
  } catch (error) {
    console.log(error.message);
  }
};


export default function* fetchCountWatcher() {
  while(true) {
    const { payload } = yield take(requestAction.FETCH_COUNT);
    yield fork(fetchCount, payload.id);
  }
}
