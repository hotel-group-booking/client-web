import { fork, call, take, put } from 'redux-saga/effects';
import {
  requestAction,
  doFetchDetailError,
  doAddDetail,
  doFetchDetailSuccess
} from '../../reducers/request';
import { requests } from '../../api';

function* fetchDetail(id) {
  try {
    const response = yield call(requests.get(id), {
      query: {
        included: 'member'
      }
    });
    yield put(doAddDetail(response));
    yield put(doFetchDetailSuccess());
  } catch (error) {
    yield put(doFetchDetailError(error.message));
  }
}

export default function* fetchDetailWatcher() {
  while (true) {
    const { payload } = yield take(requestAction.FETCH_DETAIL);
    yield fork(fetchDetail, payload.id);
  }
}
