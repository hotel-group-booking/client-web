import { take, fork, call, put } from 'redux-saga/effects';
import {
  requestAction,
  doFetchREquestError,
  doAddRequestToList,
  doFetchRequestSuccess,
  doFetchCount
} from '../../reducers/request';
import { requests } from '../../api';
import { filter } from 'ramda';

function* fetchRequest(page, member) {
  try {
    const query = filter(Boolean)({ page, member, included: 'member' });
    const response = yield call(requests.get(), {
      query
    });
    yield put(doAddRequestToList(response.value, page, response.size));
    yield put(doFetchRequestSuccess());
    yield put(doFetchCount(member));
  } catch (error) {
    yield put(doFetchREquestError(error.message));
  }
}

export default function* fetchRequestWatcher() {
  while (true) {
    const { payload } = yield take(requestAction.FETCH_REQUESTS);
    yield fork(fetchRequest, payload.page, payload.id);
  }
}
