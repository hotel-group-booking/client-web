import { fork, all } from 'redux-saga/effects';
import createRequest from './create-request';
import fetchRequest from './fetch-requests';
import fetchDetail from './fetch-detail';
import searchRequest from './search-request';
import update from './update';
import fetchCount from './fetch-count';

export default function* requestSaga() {
  yield all([
    fork(createRequest),
    fork(fetchRequest),
    fork(fetchDetail),
    fork(searchRequest),
    fork(update),
    fork(fetchCount)
  ]);
};