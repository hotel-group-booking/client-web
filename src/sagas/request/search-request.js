import { put, call, take, fork, delay } from 'redux-saga/effects';
import {
  requestAction,
  doSearchRequestSuccess,
  doSearchRequestError,
  doClearResult,
  doAddRequestToList
} from '../../reducers/request';
import { requests } from '../../api';

function* searchRequest(page, filter) {
  try {
    const response = yield call(requests.search(), {
      query: {
        page,
        rooms: filter.rooms,
        budget: [filter.budgetFrom, filter.budgetTo],
        room_type: filter.roomType,
        star_rating: filter.starRating,
        destination_ids: [filter.location.placeId],
        included: 'member'
      }
    });
    yield put(doAddRequestToList(response.value, page, response.size));
    yield put(doSearchRequestSuccess());
  } catch(error) {
    yield put(doSearchRequestError(error.message));
    yield delay(1000);
    yield put(doClearResult());
  }
}

export default function* searchRequestWatcher() {
  while(true) {
    const { payload } = yield take(requestAction.SEARCH_REQUEST);
    yield fork(searchRequest, payload.page, payload.filter);
  }
}
