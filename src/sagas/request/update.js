import { take, fork, call, put } from 'redux-saga/effects';
import {
  requestAction,
  doUpdateDone,
  doUpdateFailed,
  doSelect,
  doFetchCount
} from '../../reducers/request';
import { requests } from '../../api';

function* update(id, data, userId) {
  try {
    const response = yield call(requests.put(id), {
      body: data
    });
    yield put(doUpdateDone());
    yield put(doSelect(response));
    if (userId) {
      yield put(doFetchCount(userId));
    }
  } catch (error) {
    yield put(doUpdateFailed(error.message));
  }
}

export default function* updateWatcher() {
  while (true) {
    const { payload } = yield take(requestAction.UPDATE);
    yield fork(update, payload.id, payload.data, payload.userId);
  }
};
