import { take, call, put, fork, delay } from 'redux-saga/effects';
import { users } from '../../api';
import {
  userAction,
  doChangePasswordSuccess,
  doChangePasswordError,
  doClearResult
} from '../../reducers/user';

function* changePassword(id, data) {
  try {
    yield call(users.changePassword(id), {
      body: data
    });
    yield put(doChangePasswordSuccess());
    yield delay(2000);
    yield put(doClearResult());
  } catch (error) {
    yield put(doChangePasswordError(error.message));
  }
}



export default function* changePasswordWatcher() {
  while (true) {
    const { payload } = yield take(userAction.CHANGE_PASSWORD);
    yield fork(changePassword, payload.id, payload.data);
  }
}
