import { fork, call, put, take, delay } from 'redux-saga/effects';
import { users } from '../../api';
import {
  userAction,
  doFetchDataSuccess,
  doAddData,
  doFetchDataError,
  doClearResult
} from '../../reducers/user';

function* claerResult() {
  yield delay(4000);
  yield put(doClearResult());
};

function* fetchData(id) {
  try {
    const response = yield call(users.get(id), {
      query: {
        included: 'hotel'
      }
    });
    yield put(doFetchDataSuccess());
    yield fork(claerResult);
    yield put(doAddData(response));
  } catch (error) {
    yield put(doFetchDataError(error.message));
    yield fork(claerResult);
  }
};

function* fetchDataWatcher() {
  while (true) {
    const { payload } = yield take(userAction.FETCH_DATA);
    yield fork(fetchData, payload.id);
    
  }
};

export default fetchDataWatcher;
