import { all, fork } from 'redux-saga/effects';
import fetchDataWatcher from './fetch-data';
import registrationWatcher from './registration';
import updateWatcher from './update';
import changePassword from './change-password';
import uploadAvatar from './upload-avatar';

function* userSaga() {
  yield all([
    fork(fetchDataWatcher),
    fork(registrationWatcher),
    fork(updateWatcher),
    fork(changePassword),
    fork(uploadAvatar)
  ]);
};

export default userSaga;