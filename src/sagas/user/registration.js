import { fork, call, put, take} from 'redux-saga/effects';
import { users, hotels } from '../../api';
import {
  userAction,
  doRegistrationSuccess,
  doRegistrationError
} from '../../reducers/user';

function* registraion(user, hotel) {
  try {
    const response = yield call(users.post(), {
      body: {
        ...user,
        displayName: user.displayName || `${user.firstName} ${user.lastName}`
      }
    });
    
    if (hotel) {
      yield call(hotels.post(response.token), {
        body: hotel
      });
    }

    // if (user.savedRequest) {
    //   yield call(requests.post(response.token), {
    //     body: user.savedRequest
    //   });
    // }
    
    yield put(doRegistrationSuccess());
  } catch (error) {
    yield put(doRegistrationError(error.message));
  }
}

export default function* registrationWatcher() {
  while (true) {
    const { payload } = yield take(userAction.REGISTRATION);
    yield fork(registraion, payload.user, payload.hotel);
  }
}
