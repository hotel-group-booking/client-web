import { take, fork, put, call, delay } from 'redux-saga/effects';
import {
  userAction,
  doUpdateSuccess,
  doUpdateError,
  doAddData,
  doClearResult
} from '../../reducers/user';
import { users } from '../../api';

function* update(id, data) {
  try {
    const response = yield call(users.put(id), {
      body: data
    });
    yield put(doAddData(response));
    yield put(doUpdateSuccess());
    yield delay(2000);
    yield put(doClearResult());
  } catch(error) {
    yield put(doUpdateError(error.message));
  }
}

export default function* updateWatcher() {
  while (true) {
    const { payload } = yield take(userAction.UPDATE);
    yield fork(update, payload.id, payload.data);
  }
};

