import { take, fork, put, call } from 'redux-saga/effects';
import { split } from 'ramda';
import {
  userAction,
  doUploadAvatarDone,
  doUploadAvatarFailed,
  doAddData
} from '../../reducers/user';
import { users } from 'api';

function* uploadAvatar(id, image) {
  try {
    const response = yield call(users.uploadAvatar(id), {
      body: {
        image: split(',', image)[1]
      }
    });
    yield put(doAddData(response))
    yield put(doUploadAvatarDone());
  } catch (error) {
    yield put(doUploadAvatarFailed(error.message));
  }
  

}

export default function* uploadAvatarWatcher() {
  while (true) {
    const { payload } = yield take(userAction.UPLOAD_AVATAR);
    yield fork(uploadAvatar, payload.id, payload.image);
  }
}
